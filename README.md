# C-U Safety Forecasting Tool
More than 3,300 crashes happened every year in Champaign County in the past five years. Transportation planners and engineers often find it difficult to list and prioritize transportation projects based on safety measures as the required data and tools are not readily available to conduct regular and comprehensive safety evaluations for the region. This project aims to develop a safety forecasting tool and assist transportation professionals to understand where traffic crash risk is high in the Champaign-Urbana, IL region.

# Research Question
This project requires a careful selection of the time/space scales for analysis, including an improved set of explanatory variables and/or unobserved heterogeneity effects, in order to derive at the most sensible results. This section details the research scope by answering the following research questions:  

**What's predicted?**  
- [x] Crash risk: the likelihood of crash occurrence
- [ ] Crash frequency: total number of crashes in a year  
- [ ] Crash rate per vehicle mile: total crashes / (AADT X section length)

**What's the time horizon?**   
- [x] One year  
- [ ] One month
- [ ] One week
- [ ] One day

**What's the spatial dimension?**   
- [x] Segments
- [ ] Segments & intersections
- [ ] x mile by x mile grid

These questions will formulate the goal of the project:  
- [x] Roadway segments with the highest risk of crashes
- [ ] Probability of a crash to occur on Philo Road in February
- [ ] Location of the next most likely crash in the next 24 hours

# Report Structure
This project is implemented in a series of Jupyter Notebooks that can be executed in Jupyter Notebook or JupyterLab in the Anaconda Environment. The notebooks are organized into seven sections, and can be exported as HTML files for easy review.

* Notebook 1 provides project background, summarizes literature review findings, introduces research methodology, and presents project conclusions.
* Notebook 2 collects and organizes data needed for the project, including 10 years of traffic crash data (2009-2018), roadway geometry data, and demographic and land use data.
* Notebook 3 conducts exploratory analysis on the traffic crash data to understand temporal, spatial, and other patterns of traffic crashes in Champaign County.
* Notebook 4 conducts data preprocessing to prepare the datasets for the models. This includes assigning crashes to segments and creating training/testing datasets, etc for urban and rural Champaign County separately.
* Notebook 5 builds, tests, evaluates binary (crash/no-crash) machine learning models for roadways in Champaign County using rural Champaign County as an example. After identifying XGB model as the preferred model, notebooks in Rural(Urban) Binary Models folders implement the XGB model for the urban and rural Champaign County roadways, and apply the model to predict crash risks for LRTP 2045 preferred scenario.
* Notebook 6 builds, tests, evaluates multi-class (high/medium/low crash risks) machine learning models for roadways in urban and rural Champaign County.

## Author
This project is funded by the Illinois Department of Transportation and carried out by Shuake Wuzhati and Xiyue Li at the Champaign County Regional Planning Commission (CCRPC).

## License
This project is licensed under BSD 3-Clause License - see the LICENSE.md file for details.
