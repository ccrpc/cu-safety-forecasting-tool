
import pickle
import numpy as np
import pandas as pd
import geopandas as gpd
import requests
import json
from shapely import wkt
from sklearn.linear_model import LinearRegression

base_gis_dir  = "G:/CUUATS/Safety Forecasting Tool/"

""" Section 4.1 Prepare Static Features """

def pcd_street_static(streets):
    """
    This function select all essential features for describing PCD street network
    """
    streets_static=streets[['segment_id', 'geom', 'name', 'overlap','cross_name_start', 'cross_name_end',
       'start_intersection_id', 'end_intersection_id', 'st_length','curve_ratio','idot_inventory',
       'idot_begin_station', 'idot_end_station', 'one_way',
       'bicycle_facility_width', 'bicycle_path_category',
       'bicycle_buffer_width', 'bicycle_buffer_type', 'bicycle_path_type',
       'bicycle_approach_alignment', 'bus_trips_total', 'crossing_aadt',
       'crossing_functional_classification', 'crossing_speed',
       'functional_classification',
       'intersection_control_type', 'intersection_median_refuge',
       'lane_configuration', 'lanes_per_direction', 'max_lanes_crossed',
       'parking_lane_width', 'marked_center_line',
       'posted_speed', 'railroad_crossing_type', 'right_turn_length',
       'road_sign_type', 'sidewalk_buffer_width', 'sidewalk_condition_score',
       'sidewalk_width', 'sidewalk_buffer_type', 'total_lanes',
       'in_urbanized_area', 'volume_capacity', 'overall_landuse']]
    streets_static=streets_static[streets_static['idot_inventory'].notnull()]
    streets_static['idot_inventory']=streets_static['idot_inventory'].astype(str)
    streets_static['idot_begin_station']=pd.to_numeric(streets_static['idot_begin_station'])
    streets_static['idot_end_station']=pd.to_numeric(streets_static['idot_end_station'])
    print('{} street segments within IDOT inventory included'.format(streets_static.shape[0]))

    return streets_static

def rdwy_static_deal_miss(roadway_static):
    """
    This function populate the missing vlaues in roadway_static
    """
    roadway_static['cross_name_start']=roadway_static['cross_name_start'].fillna(0)
    roadway_static['cross_name_end']=roadway_static['cross_name_end'].fillna(0)
    roadway_static['name']=roadway_static['name'].fillna(0)
    roadway_static['one_way']=roadway_static['one_way'].fillna(0)
    roadway_static.loc[roadway_static['one_way']!=0,'one_way']=1
    roadway_static.loc[roadway_static['bicycle_facility_width']==0,
                       'bicycle_facility_width']=roadway_static['bicycle_facility_width'].mean()
    roadway_static['bicycle_facility_width']=roadway_static['bicycle_facility_width'].fillna(0)
    roadway_static.loc[roadway_static['bicycle_buffer_width']==0,'bicycle_buffer_width']=roadway_static['bicycle_buffer_width'].mean()
    roadway_static['bicycle_buffer_width']=roadway_static['bicycle_buffer_width'].fillna(0)
    roadway_static['bicycle_buffer_type']=roadway_static['bicycle_buffer_type'].fillna('None')
    roadway_static['bicycle_path_type']=roadway_static['bicycle_path_type'].fillna('None')
    roadway_static['bicycle_path_category']=roadway_static['bicycle_path_category'].fillna('None')
    roadway_static['bicycle_approach_alignment']=roadway_static['bicycle_approach_alignment'].fillna('None')
    roadway_static['bus_trips_total']=roadway_static['bus_trips_total'].fillna(0)
    roadway_static['crossing_aadt']=roadway_static['crossing_aadt'].fillna(0)
    roadway_static['crossing_functional_classification']=roadway_static['crossing_functional_classification'].fillna(
        roadway_static['functional_classification'])
    roadway_static['crossing_functional_classification']=roadway_static['crossing_functional_classification'].fillna(
        'Local Road or Street')
    roadway_static['functional_classification']=roadway_static['functional_classification'].fillna('Local Road or Street')
    roadway_static.loc[roadway_static['intersection_control_type']==' ','intersection_control_type']='None'
    roadway_static.loc[roadway_static['intersection_control_type']=='NA','intersection_control_type']='None'
    roadway_static['intersection_control_type']=roadway_static['intersection_control_type'].fillna('None')
    roadway_static['lane_configuration']=roadway_static['lane_configuration'].fillna('None')
    roadway_static.loc[roadway_static['lane_configuration']=='None','lane_configuration']='None_TT'
    roadway_static['max_lanes_crossed']=roadway_static['max_lanes_crossed'].fillna(0)
    roadway_static['parking_lane_width']=roadway_static['parking_lane_width'].fillna(0)
    roadway_static['marked_center_line']=roadway_static['marked_center_line'].fillna('No')
    roadway_static['posted_speed']=roadway_static['posted_speed'].fillna(30)
    roadway_static['crossing_speed']=roadway_static['crossing_speed'].fillna(roadway_static['posted_speed'])
    roadway_static['railroad_crossing_type']=roadway_static['railroad_crossing_type'].fillna('None')
    roadway_static['right_turn_length']=roadway_static['right_turn_length'].fillna(0)
    roadway_static['road_sign_type']=roadway_static['road_sign_type'].fillna('None')
    roadway_static['sidewalk_buffer_type']=roadway_static['sidewalk_buffer_type'].fillna('Unknown')
    roadway_static['sidewalk_buffer_width']=roadway_static['sidewalk_buffer_width'].fillna(0)
    roadway_static['sidewalk_condition_score']=roadway_static['sidewalk_condition_score'].fillna(0)
    roadway_static['sidewalk_width']=roadway_static['sidewalk_width'].fillna(0)
    roadway_static['in_urbanized_area']=roadway_static['in_urbanized_area'].fillna('No')
    roadway_static['overall_landuse']=roadway_static['overall_landuse'].fillna("Agricultural")
    roadway_static['volume_capacity']=roadway_static['volume_capacity'].fillna(0)
    roadway_static.loc[roadway_static['total_lanes'].isnull()==True,
                       'total_lanes']=roadway_static.loc[roadway_static['total_lanes'].isnull()==True,'LNS']
    roadway_static.loc[roadway_static['lanes_per_direction'].isnull()==True,
                       'lanes_per_direction']=roadway_static.loc[roadway_static['lanes_per_direction'].isnull()==True,'LNS']/2
    roadway_static.loc[roadway_static['total_lanes'].isnull()==True,'total_lanes']=2
    roadway_static=roadway_static.dropna(subset = ['INVENTORY'])
    roadway_static['ROAD_NAME']=roadway_static['ROAD_NAME'].fillna('None')
    roadway_static=roadway_static.drop(columns=['intersection_median_refuge','lanes_per_direction','SURF_YR','ROAD_NAME','SP_LIM'])

    return roadway_static

""" Section 4.2.1 Prepare AADT fields """

def streets_tmp(streets):
    """
    This function select PCD street network features that change over time
    """
    streets_temporal=streets[['segment_id', 'geom',
                          'idot_inventory','idot_begin_station', 'idot_end_station',
                          'aadt','functional_classification',
                          'heavy_vehicle_count','pavement_condition']]
    streets_temporal=streets_temporal[streets_temporal['idot_inventory'].notnull()]
    streets_temporal['idot_inventory']=streets_temporal['idot_inventory'].astype(str)
    streets_temporal['idot_begin_station']=pd.to_numeric(streets_temporal['idot_begin_station'])
    streets_temporal['idot_end_station']=pd.to_numeric(streets_temporal['idot_end_station'])

    return streets_temporal

def get_aadt (df):
    """
    This function pick the aadt data in year 20XX and place it in the corresponding column AADT_20XX
    """
    df_aadt=df[['INVENTORY','BEG_STA','END_STA','AADT','AADT_YR','FCNAME']]
    df_aadt.loc[(df_aadt.AADT_YR == '2014'), 'AADT_2014'] = df_aadt['AADT']
    df_aadt.loc[(df_aadt.AADT_YR == '2015'), 'AADT_2015'] = df_aadt['AADT']
    df_aadt.loc[(df_aadt.AADT_YR == '2016'), 'AADT_2016'] = df_aadt['AADT']
    df_aadt.loc[(df_aadt.AADT_YR == '2017'), 'AADT_2017'] = df_aadt['AADT']
    df_aadt.loc[(df_aadt.AADT_YR == '2018'), 'AADT_2018'] = df_aadt['AADT']
    df_aadt.loc[(df_aadt.AADT_YR == '2013'), 'AADT_2013'] = df_aadt['AADT']
    df_aadt.loc[(df_aadt.AADT_YR == '2012'), 'AADT_2012'] = df_aadt['AADT']
    df_aadt.loc[(df_aadt.AADT_YR == '2011'), 'AADT_2011'] = df_aadt['AADT']
    df_aadt.loc[(df_aadt.AADT_YR == '2010'), 'AADT_2010'] = df_aadt['AADT']
    df_aadt.loc[(df_aadt.AADT_YR == '2009'), 'AADT_2009'] = df_aadt['AADT']
    df_aadt=df_aadt[['INVENTORY','BEG_STA','END_STA',
                     'AADT_2018','AADT_2017','AADT_2016','AADT_2015','AADT_2014',
                     'AADT_2013','AADT_2012','AADT_2011','AADT_2010','AADT_2009','FCNAME']]

    return df_aadt

def HWY_aadt_merge (df1,df2):
    """
    This function combines IDOT HWY netowrk aadt
    """
    HWY_temporal= pd.merge(df1, df2,
                           how='left',
                           on=['INVENTORY','BEG_STA','END_STA'])

    return HWY_temporal

def HWY_aadt_replace(roadway_temporal):
    """
    This function is used for merge_all_temp_aadt() function.
    It helps merge two columns with AADT data of the same year.
    """
    roadway_temporal['AADT_2014_x']=roadway_temporal['AADT_2014_x'].fillna(roadway_temporal['AADT_2014_y'])
    roadway_temporal['AADT_2015_x']=roadway_temporal['AADT_2015_x'].fillna(roadway_temporal['AADT_2015_y'])
    roadway_temporal['AADT_2016_x']=roadway_temporal['AADT_2016_x'].fillna(roadway_temporal['AADT_2016_y'])
    roadway_temporal['AADT_2017_x']=roadway_temporal['AADT_2017_x'].fillna(roadway_temporal['AADT_2017_y'])
    roadway_temporal['AADT_2018_x']=roadway_temporal['AADT_2018_x'].fillna(roadway_temporal['AADT_2018_y'])
    roadway_temporal['AADT_2013_x']=roadway_temporal['AADT_2013_x'].fillna(roadway_temporal['AADT_2013_y'])
    roadway_temporal['AADT_2012_x']=roadway_temporal['AADT_2012_x'].fillna(roadway_temporal['AADT_2012_y'])
    roadway_temporal['AADT_2011_x']=roadway_temporal['AADT_2011_x'].fillna(roadway_temporal['AADT_2011_y'])
    roadway_temporal['AADT_2010_x']=roadway_temporal['AADT_2010_x'].fillna(roadway_temporal['AADT_2010_y'])
    roadway_temporal['AADT_2009_x']=roadway_temporal['AADT_2009_x'].fillna(roadway_temporal['AADT_2009_y'])
    roadway_temporal=roadway_temporal[['INVENTORY','BEG_STA','END_STA',
                                       'AADT_2018_x','AADT_2017_x','AADT_2016_x','AADT_2015_x','AADT_2014_x',
                                       'AADT_2013_x','AADT_2012_x','AADT_2011_x','AADT_2010_x','AADT_2009_x',
                                       'FCNAME_x']]

    return roadway_temporal

def HWY_aadt_replace_2(roadway_temporal):
    """
    This function is used for merge_all_temp_aadt() function.
    It helps merge two columns with AADT data of the same year.
    """
    roadway_temporal['AADT_2014_x']=roadway_temporal['AADT_2014_x'].fillna(roadway_temporal['AADT_2014'])
    roadway_temporal['AADT_2015_x']=roadway_temporal['AADT_2015_x'].fillna(roadway_temporal['AADT_2015'])
    roadway_temporal['AADT_2016_x']=roadway_temporal['AADT_2016_x'].fillna(roadway_temporal['AADT_2016'])
    roadway_temporal['AADT_2017_x']=roadway_temporal['AADT_2017_x'].fillna(roadway_temporal['AADT_2017'])
    roadway_temporal['AADT_2018_x']=roadway_temporal['AADT_2018_x'].fillna(roadway_temporal['AADT_2018'])
    roadway_temporal['AADT_2013_x']=roadway_temporal['AADT_2013_x'].fillna(roadway_temporal['AADT_2013'])
    roadway_temporal['AADT_2012_x']=roadway_temporal['AADT_2012_x'].fillna(roadway_temporal['AADT_2012'])
    roadway_temporal['AADT_2011_x']=roadway_temporal['AADT_2011_x'].fillna(roadway_temporal['AADT_2011'])
    roadway_temporal['AADT_2010_x']=roadway_temporal['AADT_2010_x'].fillna(roadway_temporal['AADT_2010'])
    roadway_temporal['AADT_2009_x']=roadway_temporal['AADT_2009_x'].fillna(roadway_temporal['AADT_2009'])
    roadway_temporal=roadway_temporal[['INVENTORY','BEG_STA','END_STA',
                                       'AADT_2018_x','AADT_2017_x','AADT_2016_x','AADT_2015_x','AADT_2014_x',
                                       'AADT_2013_x','AADT_2012_x','AADT_2011_x','AADT_2010_x','AADT_2009_x',
                                       'FCNAME_x']]

    return roadway_temporal

def merge_all_temp_aadt(HWY_temporal_2018_aadt,HWY_temporal_2017_aadt,HWY_temporal_2016_aadt,HWY_temporal_2015_aadt,HWY_temporal_2014_aadt,HWY_temporal_2013_aadt,HWY_temporal_2012_aadt,HWY_temporal_2011_aadt,HWY_temporal_2010_aadt,HWY_temporal_2009_aadt):
    """
    This function merges aadt from 2009 to 2018 year by year (descending order)
    """
    HWY_temporal_2017_2018=HWY_aadt_merge(HWY_temporal_2018_aadt, HWY_temporal_2017_aadt)
    HWY_temporal_2017_2018=HWY_aadt_replace(HWY_temporal_2017_2018)
    HWY_temporal_2016_2018=HWY_aadt_merge(HWY_temporal_2017_2018, HWY_temporal_2016_aadt)
    HWY_temporal_2016_2018=HWY_aadt_replace_2(HWY_temporal_2016_2018)
    HWY_temporal_2015_2018=HWY_aadt_merge(HWY_temporal_2016_2018, HWY_temporal_2015_aadt)
    HWY_temporal_2015_2018=HWY_aadt_replace_2(HWY_temporal_2015_2018)
    HWY_temporal_2014_2018=HWY_aadt_merge(HWY_temporal_2015_2018, HWY_temporal_2014_aadt)
    HWY_temporal_2014_2018=HWY_aadt_replace_2(HWY_temporal_2014_2018)
    HWY_temporal_2013_2018=HWY_aadt_merge(HWY_temporal_2014_2018, HWY_temporal_2013_aadt)
    HWY_temporal_2013_2018=HWY_aadt_replace_2(HWY_temporal_2013_2018)
    HWY_temporal_2012_2018=HWY_aadt_merge(HWY_temporal_2013_2018, HWY_temporal_2012_aadt)
    HWY_temporal_2012_2018=HWY_aadt_replace_2(HWY_temporal_2012_2018)
    HWY_temporal_2011_2018=HWY_aadt_merge(HWY_temporal_2012_2018, HWY_temporal_2011_aadt)
    HWY_temporal_2011_2018=HWY_aadt_replace_2(HWY_temporal_2011_2018)
    HWY_temporal_2010_2018=HWY_aadt_merge(HWY_temporal_2011_2018, HWY_temporal_2010_aadt)
    HWY_temporal_2010_2018=HWY_aadt_replace_2(HWY_temporal_2010_2018)
    HWY_temporal_2009_2018=HWY_aadt_merge(HWY_temporal_2010_2018, HWY_temporal_2009_aadt)
    HWY_temporal_2009_2018=HWY_aadt_replace_2(HWY_temporal_2009_2018)
    HWY_temporal_2009_2018=HWY_temporal_2009_2018.rename(columns={"AADT_2014_x": "AADT_2014",
                                                              "AADT_2015_x": "AADT_2015",
                                                              "AADT_2016_x": "AADT_2016",
                                                              "AADT_2017_x": "AADT_2017",
                                                              "AADT_2018_x": "AADT_2018",
                                                              "AADT_2013_x": "AADT_2013",
                                                              "AADT_2012_x": "AADT_2012",
                                                              "AADT_2011_x": "AADT_2011",
                                                              "AADT_2010_x": "AADT_2010",
                                                              "AADT_2009_x": "AADT_2009",
                                                              "FCNAME_x":"FCNAME"
                                                                     })
    HWY_temporal_2009_2018=HWY_temporal_2009_2018[['INVENTORY','BEG_STA','END_STA',
                                               'AADT_2018','AADT_2017','AADT_2016','AADT_2015','AADT_2014',
                                               'AADT_2013','AADT_2012','AADT_2011','AADT_2010','AADT_2009',
                                               'FCNAME']]

    return HWY_temporal_2009_2018

# For each road function class, interpolate the missing aadt data basing on the following years
## The dict keys represent roadway function class
## {0:Interstate, 1:Local Roads or Street, 2:Major Arterial, 3:Major Collector, 4:Minor Arterial, 5:Minor Collector}
## For each road function class (key), years that have aadt data are listed in the dict values
## The details can be examined by HWY_temporal_2009_2018.groupby(['FCNAME']).count()
aadt_train_years={0:np.array([[2017], [2015], [2013], [2011], [2009]]),
             1:np.array([[2016], [2011]]),
             2:np.array([[2017], [2015], [2013], [2011]]),
             3:np.array([[2016], [2011]]),
             4:np.array([[2016], [2015], [2011]]),
             5:np.array([[2016], [2011]])}
aadt_years=[2018,2017,2016,2015,2014,2013,2012,2011,2010,2009]

def aadt_get_train_index (aadt_train_years,aadt_years,n):
    """
    This function is used for aadt_train_index{} dict.
    It helps identify column index of years in the raw_aadt_fc dataframe
    """
    index=[]
    for i in range (0,len(aadt_train_years[n])):
        index.append(aadt_years.index(aadt_train_years[n][i].tolist()[0])+2)

    return index

# Years that have true aadt data by index, can be used for extrapolation later
## The keys represent roadway functon class
## {0:Interstate, 1:Local Roads or Street, 2:Major Arterial, 3:Major Collector, 4:Minor Arterial, 5:Minor Collector}
## The values represent year (index)
## {2019:1, 2018:2, 2017:3, 2016:5 ... 2009:11}
## aadt_train_index = {0: [3, 5, 7, 9, 11],1: [4, 9],2: [3, 5, 7, 9],3: [4, 9],4: [4, 5, 9],5: [4, 9]}
aadt_train_index={0:aadt_get_train_index (aadt_train_years,aadt_years,0),
             1:aadt_get_train_index (aadt_train_years,aadt_years,1),
             2:aadt_get_train_index (aadt_train_years,aadt_years,2),
             3:aadt_get_train_index (aadt_train_years,aadt_years,3),
             4:aadt_get_train_index (aadt_train_years,aadt_years,4),
             5:aadt_get_train_index (aadt_train_years,aadt_years,5)}

def aadt_get_predict_years (aadt_years,aadt_train_years,n):
    """
    This function is used for aadt_predict_years{} dict
    It helps identify years that need to extrapolate aadt
    """
    aadt_predict_years=np.setdiff1d(aadt_years,[item for sublist in aadt_train_years[n].tolist() for item in sublist])

    return (np.array([[el] for el in aadt_predict_years.tolist()]))

# Years that need extrapolate aadt data
## The keys represent roadway functon class
## {0:Interstate, 1:Local Roads or Street, 2:Major Arterial, 3:Major Collector, 4:Minor Arterial, 5:Minor Collector}
## {0: array([[2010],[2012],[2014],[2016],[2018]]),...,5: array([[2009],[2010],[2012],[2013],[2014],[2015],[2017],[2018]])}
aadt_predict_years={0:aadt_get_predict_years(aadt_years,aadt_train_years,0),
             1:aadt_get_predict_years(aadt_years,aadt_train_years,1),
             2:aadt_get_predict_years(aadt_years,aadt_train_years,2),
             3:aadt_get_predict_years(aadt_years,aadt_train_years,3),
             4:aadt_get_predict_years(aadt_years,aadt_train_years,4),
             5:aadt_get_predict_years(aadt_years,aadt_train_years,5)}

def aadt_get_predict_index (aadt_years,aadt_predict_years,n):

    """
    This function is used for aadt_predict_index{}
    It helps identify column index of these years in the aadt_fc dataframe.
    """
    index=[]
    for i in range (0,len(aadt_predict_years[n])):
        index.append(aadt_years.index(aadt_predict_years[n][i].tolist()[0])+2)

    return index

# Years that need to extrapolate aadt data by index
## The keys represent roadway functon class
## {0:Interstate, 1:Local Roads or Street, 2:Major Arterial, 3:Major Collector, 4:Minor Arterial, 5:Minor Collector}
aadt_predict_index={0:aadt_get_predict_index (aadt_years,aadt_predict_years,0),
             1:aadt_get_predict_index (aadt_years,aadt_predict_years,1),
             2:aadt_get_predict_index (aadt_years,aadt_predict_years,2),
             3:aadt_get_predict_index (aadt_years,aadt_predict_years,3),
             4:aadt_get_predict_index (aadt_years,aadt_predict_years,4),
             5:aadt_get_predict_index (aadt_years,aadt_predict_years,5)}

def update_aadt_fc (aadt_regsr,aadt_fc,aadt_train_years,aadt_train_index,aadt_predict_years,aadt_predict_index,n):
    """
    This function  ues LinearRegression method to update (interpolate and extrapolate) AADT functional class by function class
    The last input n represents road function class
    {0:Interstate, 1:Local Roads or Street, 2:Major Arterial, 3:Major Collector, 4:Minor Arterial, 5:Minor Collector}
    """
    X=aadt_train_years[n]
    y=aadt_fc.iloc[n,aadt_train_index[n]].values.reshape(-1,1)
    x_predict=aadt_predict_years[n]
    aadt_regsr.fit(X,y)
    y_predict=aadt_regsr.predict(x_predict)
    for i in range(0,len(aadt_predict_index[n])):
        aadt_fc.iloc[n,aadt_predict_index[n][i]]=y_predict[i]

    return aadt_fc

def aadt_fc_pct(aadt_fc):
    """
    This function calculate percentage change assuming different years as the base year
    It creates a df for each year within (2009,2019) and assembles the dfs in a dict
    """
    aadt_fc_pt_year={}
    for k in range (2009,2019):
        aadt_fc_pt=aadt_fc.copy()
        for i in range (0,aadt_fc_pt.shape[0]):
            for j in range (2,12):
                aadt_fc_pt.iloc[i,j]=(aadt_fc.iloc[i,j]-aadt_fc.iloc[i,(2020-k)])/aadt_fc.iloc[i,(2020-k)]
            aadt_fc_pt.iloc[i,(2020-k)]=0
        aadt_fc_pt_year.update( {k : aadt_fc_pt} )
    return aadt_fc_pt_year

def upd_miss_aadt_row(aadt_fc,aadt_fc_pt_year,df):
    """
    This function updates the aadt numbers for the segment database, based on years with AADT numbers available and functional classfication
    The detailed strategies see comments in aadt_avl_0() ~ aadt_avl_9()
    """
    def aadt_avl_0(i,aadt_fc,fc): # If AADT numbers are not available for all year
    ## Apply average AADT by functional classification
        for j in range(2,12):
            df.iloc[i,j]=aadt_fc.iloc[fc,j]
    def aadt_avl_1(i,aadt_fc,fc): ## If there is only one year with AADT number available
        ## Use this year as base year, and apply average percentage change by functiona class for each year
        base_aadt_index=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2]
        year_index=2020-base_aadt_index[0]
        for j in range (2,12):
            df.iloc[i,j]= (1+aadt_fc_pt_year.get(year_index).iloc[fc,j])*df.iloc[fc,base_aadt_index[0]]
    def aadt_avl_2(i,aadt_fc,fc): ## If there are two years with AADT numbers available
        base_aadt_index_1=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][0]
        base_aadt_index_2=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][1]
        year_index_1=2020-base_aadt_index_1
        year_index_2=2020-base_aadt_index_2
        for j in range (2,base_aadt_index_1+1):
        # Use the first year as the base year, and apply average percentage change by functiona class for each year before this year
            df.iloc[i,j]= (1+aadt_fc_pt_year.get(year_index_1).iloc[fc,j])*df.iloc[i,base_aadt_index_1]
        for j in range (base_aadt_index_2+1,12):
        # Use the second year as the base year, and apply average percentage change by functiona class for each year after this year
            df.iloc[i,j]= (1+aadt_fc_pt_year.get(year_index_2).iloc[fc,j])*df.iloc[i,base_aadt_index_2]
        for j in range (base_aadt_index_1+1,base_aadt_index_2):
        # Calculate average percentage change for years in-between and extrapolate AADT
            rate=(df.iloc[i,base_aadt_index_1]-df.iloc[i,base_aadt_index_2])/(base_aadt_index_2-base_aadt_index_1)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_1]+rate*(j-base_aadt_index_1)
    def aadt_avl_3(i,aadt_fc,fc): ## If there are three years with AADT numbers available
        base_aadt_index_1=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][0]
        base_aadt_index_2=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][1]
        base_aadt_index_3=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][2]
        year_index_1=2020-base_aadt_index_1
        year_index_2=2020-base_aadt_index_2
        year_index_3=2020-base_aadt_index_3
        for j in range (2,base_aadt_index_1+1):
            df.iloc[i,j]= (1+aadt_fc_pt_year.get(year_index_1).iloc[fc,j])*df.iloc[i,base_aadt_index_1]
        for j in range (base_aadt_index_3+1,12):
            df.iloc[i,j]= (1+aadt_fc_pt_year.get(year_index_3).iloc[fc,j])*df.iloc[i,base_aadt_index_3]
        for j in range (base_aadt_index_1+1,base_aadt_index_2):
            rate=(df.iloc[i,base_aadt_index_1]-df.iloc[i,base_aadt_index_2])/(base_aadt_index_2-base_aadt_index_1)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_1]+rate*(j-base_aadt_index_1)
        for j in range (base_aadt_index_2+1,base_aadt_index_3):
            rate=(df.iloc[i,base_aadt_index_2]-df.iloc[i,base_aadt_index_3])/(base_aadt_index_3-base_aadt_index_2)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_2]+rate*(j-base_aadt_index_2)
    def aadt_avl_4(i,aadt_fc,fc): ## If there are four years with AADT numbers available
        base_aadt_index_1=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][0]
        base_aadt_index_2=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][1]
        base_aadt_index_3=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][2]
        base_aadt_index_4=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][3]
        year_index_1=2020-base_aadt_index_1
        year_index_2=2020-base_aadt_index_2
        year_index_3=2020-base_aadt_index_3
        year_index_4=2020-base_aadt_index_4
        for j in range (2,base_aadt_index_1+1):
            df.iloc[i,j]= (1+aadt_fc_pt_year.get(year_index_1).iloc[fc,j])*df.iloc[i,base_aadt_index_1]
        for j in range (base_aadt_index_4+1,12):
            df.iloc[i,j]= (1+aadt_fc_pt_year.get(year_index_4).iloc[fc,j])*df.iloc[i,base_aadt_index_4]
        for j in range (base_aadt_index_1+1,base_aadt_index_2):
            rate=(df.iloc[i,base_aadt_index_1]-df.iloc[i,base_aadt_index_2])/(base_aadt_index_2-base_aadt_index_1)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_1]+rate*(j-base_aadt_index_1)
        for j in range (base_aadt_index_2+1,base_aadt_index_3):
            rate=(df.iloc[i,base_aadt_index_2]-df.iloc[i,base_aadt_index_3])/(base_aadt_index_3-base_aadt_index_2)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_2]+rate*(j-base_aadt_index_2)
        for j in range (base_aadt_index_3+1,base_aadt_index_4):
            rate=(df.iloc[i,base_aadt_index_3]-df.iloc[i,base_aadt_index_4])/(base_aadt_index_4-base_aadt_index_3)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_3]+rate*(j-base_aadt_index_3)
    def aadt_avl_5(i,aadt_fc,fc): ## If there are five years with AADT numbers available
        base_aadt_index_1=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][0]
        base_aadt_index_2=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][1]
        base_aadt_index_3=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][2]
        base_aadt_index_4=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][3]
        base_aadt_index_5=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][4]
        year_index_1=2020-base_aadt_index_1
        year_index_2=2020-base_aadt_index_2
        year_index_3=2020-base_aadt_index_3
        year_index_4=2020-base_aadt_index_4
        year_index_5=2020-base_aadt_index_5
        for j in range (2,base_aadt_index_1+1):
            df.iloc[i,j]= (1+aadt_fc_pt_year.get(year_index_1).iloc[fc,j])*df.iloc[i,base_aadt_index_1]
        for j in range (base_aadt_index_5+1,12):
            df.iloc[i,j]= (1+aadt_fc_pt_year.get(year_index_5).iloc[fc,j])*df.iloc[i,base_aadt_index_5]
        for j in range (base_aadt_index_1+1,base_aadt_index_2):
            rate=(df.iloc[i,base_aadt_index_1]-df.iloc[i,base_aadt_index_2])/(base_aadt_index_2-base_aadt_index_1)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_1]+rate*(j-base_aadt_index_1)
        for j in range (base_aadt_index_2+1,base_aadt_index_3):
            rate=(df.iloc[i,base_aadt_index_2]-df.iloc[i,base_aadt_index_3])/(base_aadt_index_3-base_aadt_index_2)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_2]+rate*(j-base_aadt_index_2)
        for j in range (base_aadt_index_3+1,base_aadt_index_4):
            rate=(df.iloc[i,base_aadt_index_3]-df.iloc[i,base_aadt_index_4])/(base_aadt_index_4-base_aadt_index_3)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_3]+rate*(j-base_aadt_index_3)
        for j in range (base_aadt_index_4+1,base_aadt_index_5):
            rate=(df.iloc[i,base_aadt_index_4]-df.iloc[i,base_aadt_index_5])/(base_aadt_index_5-base_aadt_index_4)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_4]+rate*(j-base_aadt_index_4)
    def aadt_avl_6(i,aadt_fc,fc): ## If there are six years with AADT numbers available
        base_aadt_index_1=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][0]
        base_aadt_index_2=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][1]
        base_aadt_index_3=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][2]
        base_aadt_index_4=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][3]
        base_aadt_index_5=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][4]
        base_aadt_index_6=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][5]
        year_index_1=2020-base_aadt_index_1
        year_index_2=2020-base_aadt_index_2
        year_index_3=2020-base_aadt_index_3
        year_index_4=2020-base_aadt_index_4
        year_index_5=2020-base_aadt_index_5
        year_index_6=2020-base_aadt_index_6
        for j in range (2,base_aadt_index_1+1):
            df.iloc[i,j]= (1+aadt_fc_pt_year.get(year_index_1).iloc[fc,j])*df.iloc[i,base_aadt_index_1]
        for j in range (base_aadt_index_6+1,12):
            df.iloc[i,j]= (1+aadt_fc_pt_year.get(year_index_6).iloc[fc,j])*df.iloc[i,base_aadt_index_6]
        for j in range (base_aadt_index_1+1,base_aadt_index_2):
            rate=(df.iloc[i,base_aadt_index_1]-df.iloc[i,base_aadt_index_2])/(base_aadt_index_2-base_aadt_index_1)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_1]+rate*(j-base_aadt_index_1)
        for j in range (base_aadt_index_2+1,base_aadt_index_3):
            rate=(df.iloc[i,base_aadt_index_2]-df.iloc[i,base_aadt_index_3])/(base_aadt_index_3-base_aadt_index_2)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_2]+rate*(j-base_aadt_index_2)
        for j in range (base_aadt_index_3+1,base_aadt_index_4):
            rate=(df.iloc[i,base_aadt_index_3]-df.iloc[i,base_aadt_index_4])/(base_aadt_index_4-base_aadt_index_3)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_3]+rate*(j-base_aadt_index_3)
        for j in range (base_aadt_index_4+1,base_aadt_index_5):
            rate=(df.iloc[i,base_aadt_index_4]-df.iloc[i,base_aadt_index_5])/(base_aadt_index_5-base_aadt_index_4)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_4]+rate*(j-base_aadt_index_4)
        for j in range (base_aadt_index_5+1,base_aadt_index_6):
            rate=(df.iloc[i,base_aadt_index_5]-df.iloc[i,base_aadt_index_6])/(base_aadt_index_6-base_aadt_index_5)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_5]+rate*(j-base_aadt_index_5)
    def aadt_avl_7(i,aadt_fc,fc): ## If there are seven years with AADT numbers available
        base_aadt_index_1=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][0]
        base_aadt_index_2=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][1]
        base_aadt_index_3=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][2]
        base_aadt_index_4=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][3]
        base_aadt_index_5=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][4]
        base_aadt_index_6=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][5]
        base_aadt_index_7=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][6]
        year_index_1=2020-base_aadt_index_1
        year_index_2=2020-base_aadt_index_2
        year_index_3=2020-base_aadt_index_3
        year_index_4=2020-base_aadt_index_4
        year_index_5=2020-base_aadt_index_5
        year_index_6=2020-base_aadt_index_6
        year_index_7=2020-base_aadt_index_7
        for j in range (2,base_aadt_index_1+1):
            df.iloc[i,j]= (1+aadt_fc_pt_year.get(year_index_1).iloc[fc,j])*df.iloc[i,base_aadt_index_1]
        for j in range (base_aadt_index_7+1,12):
            df.iloc[i,j]= (1+aadt_fc_pt_year.get(year_index_7).iloc[fc,j])*df.iloc[i,base_aadt_index_7]
        for j in range (base_aadt_index_1+1,base_aadt_index_2):
            rate=(df.iloc[i,base_aadt_index_1]-df.iloc[i,base_aadt_index_2])/(base_aadt_index_2-base_aadt_index_1)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_1]+rate*(j-base_aadt_index_1)
        for j in range (base_aadt_index_2+1,base_aadt_index_3):
            rate=(df.iloc[i,base_aadt_index_2]-df.iloc[i,base_aadt_index_3])/(base_aadt_index_3-base_aadt_index_2)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_2]+rate*(j-base_aadt_index_2)
        for j in range (base_aadt_index_3+1,base_aadt_index_4):
            rate=(df.iloc[i,base_aadt_index_3]-df.iloc[i,base_aadt_index_4])/(base_aadt_index_4-base_aadt_index_3)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_3]+rate*(j-base_aadt_index_3)
        for j in range (base_aadt_index_4+1,base_aadt_index_5):
            rate=(df.iloc[i,base_aadt_index_4]-df.iloc[i,base_aadt_index_5])/(base_aadt_index_5-base_aadt_index_4)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_4]+rate*(j-base_aadt_index_4)
        for j in range (base_aadt_index_5+1,base_aadt_index_6):
            rate=(df.iloc[i,base_aadt_index_5]-df.iloc[i,base_aadt_index_6])/(base_aadt_index_6-base_aadt_index_5)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_5]+rate*(j-base_aadt_index_5)
        for j in range (base_aadt_index_6+1,base_aadt_index_7):
            rate=(df.iloc[i,base_aadt_index_6]-df.iloc[i,base_aadt_index_7])/(base_aadt_index_7-base_aadt_index_6)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_6]+rate*(j-base_aadt_index_6)
    def aadt_avl_8(i,aadt_fc,fc): ## If there are eight years with AADT numbers available
        base_aadt_index_1=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][0]
        base_aadt_index_2=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][1]
        base_aadt_index_3=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][2]
        base_aadt_index_4=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][3]
        base_aadt_index_5=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][4]
        base_aadt_index_6=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][5]
        base_aadt_index_7=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][6]
        base_aadt_index_8=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][7]
        year_index_1=2020-base_aadt_index_1
        year_index_2=2020-base_aadt_index_2
        year_index_3=2020-base_aadt_index_3
        year_index_4=2020-base_aadt_index_4
        year_index_5=2020-base_aadt_index_5
        year_index_6=2020-base_aadt_index_6
        year_index_7=2020-base_aadt_index_7
        year_index_8=2020-base_aadt_index_8
        for j in range (2,base_aadt_index_1+1):
            df.iloc[i,j]= (1+aadt_fc_pt_year.get(year_index_1).iloc[fc,j])*df.iloc[i,base_aadt_index_1]
        for j in range (base_aadt_index_7+1,12):
            df.iloc[i,j]= (1+aadt_fc_pt_year.get(year_index_7).iloc[fc,j])*df.iloc[i,base_aadt_index_7]
        for j in range (base_aadt_index_1+1,base_aadt_index_2):
            rate=(df.iloc[i,base_aadt_index_1]-df.iloc[i,base_aadt_index_2])/(base_aadt_index_2-base_aadt_index_1)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_1]+rate*(j-base_aadt_index_1)
        for j in range (base_aadt_index_2+1,base_aadt_index_3):
            rate=(df.iloc[i,base_aadt_index_2]-df.iloc[i,base_aadt_index_3])/(base_aadt_index_3-base_aadt_index_2)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_2]+rate*(j-base_aadt_index_2)
        for j in range (base_aadt_index_3+1,base_aadt_index_4):
            rate=(df.iloc[i,base_aadt_index_3]-df.iloc[i,base_aadt_index_4])/(base_aadt_index_4-base_aadt_index_3)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_3]+rate*(j-base_aadt_index_3)
        for j in range (base_aadt_index_4+1,base_aadt_index_5):
            rate=(df.iloc[i,base_aadt_index_4]-df.iloc[i,base_aadt_index_5])/(base_aadt_index_5-base_aadt_index_4)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_4]+rate*(j-base_aadt_index_4)
        for j in range (base_aadt_index_5+1,base_aadt_index_6):
            rate=(df.iloc[i,base_aadt_index_5]-df.iloc[i,base_aadt_index_6])/(base_aadt_index_6-base_aadt_index_5)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_5]+rate*(j-base_aadt_index_5)
        for j in range (base_aadt_index_6+1,base_aadt_index_7):
            rate=(df.iloc[i,base_aadt_index_6]-df.iloc[i,base_aadt_index_7])/(base_aadt_index_7-base_aadt_index_6)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_6]+rate*(j-base_aadt_index_6)
        for j in range (base_aadt_index_7+1,base_aadt_index_8):
            rate=(df.iloc[i,base_aadt_index_7]-df.iloc[i,base_aadt_index_8])/(base_aadt_index_8-base_aadt_index_7)
            df.iloc[i,j]=df.iloc[i,base_aadt_index_7]+rate*(j-base_aadt_index_7)
    def aadt_avl_9(i,aadt_fc,fc): ## If there are nine years with AADT numbers available
        null_year_index=np.where(np.array(df.iloc[0].isnull())==True)[0][0]
        df.iloc[i,null_year]=(df.iloc[i,null_year-1]+df.iloc[i,null_year+1])/2

    for i in range (0,df.shape[0]):
        fc=aadt_fc.index.get_loc(df.iloc[i,13]) # road functional class
        row = df.iloc[i]
        if row.isnull().sum()==10: ## If AADT numbers are not available for all year
            aadt_avl_0(i,aadt_fc,fc)
        elif row.isnull().sum()==9: ## If there is only one year with AADT number available
            aadt_avl_1(i,aadt_fc,fc)
        elif row.isnull().sum()==8: ## If there are two years with AADT numbers available
            aadt_avl_2(i,aadt_fc,fc)
        elif row.isnull().sum()==7: ## If there are three years with AADT numbers available
            aadt_avl_3(i,aadt_fc,fc)
        elif row.isnull().sum()==6: ## If there are four years with AADT numbers available
            aadt_avl_4(i,aadt_fc,fc)
        elif row.isnull().sum()==5: ## If there are five years with AADT numbers available
            aadt_avl_5(i,aadt_fc,fc)
        elif row.isnull().sum()==4: ## If there are six years with AADT numbers available
            aadt_avl_6(i,aadt_fc,fc)
        elif row.isnull().sum()==3: ## If there are seven years with AADT numbers available
            aadt_avl_7(i,aadt_fc,fc)
        elif row.isnull().sum()==2: ## If there are eight years with AADT numbers available
            aadt_avl_8(i,aadt_fc,fc)
        elif row.isnull().sum()==1: ## If there are nine years with AADT numbers available
            aadt_avl_9(i,aadt_fc,fc)
        else: ## If all years with AADT numbers are available
            print("row i: "+str(i))
            break

    return df

def cmb_tmp_HWY_sts_aadt(HWY_temporal_2009_2018_copy_update,streets_temporal):
    """
    This function combines aadt of IDOT HWY and CCRPC PCD street by segment_id
    """
    roadway_temporal_2009_2018=pd.merge(streets_temporal,HWY_temporal_2009_2018_copy_update,
                           how='left',
                           left_on=['idot_inventory','idot_begin_station','idot_end_station'],
                           right_on = ['INVENTORY','BEG_STA','END_STA'])

    ## AADT_20XX is aadt from IDOT HWY network dataset, aadt is from PCD streets network dataset
    aadt_fields=['segment_id',
                 'AADT_2018','AADT_2017','AADT_2016','AADT_2015','AADT_2014',
                 'AADT_2013','AADT_2012','AADT_2011','AADT_2010','AADT_2009','aadt','functional_classification']

    roadway_temporal_2009_2018=roadway_temporal_2009_2018[aadt_fields]

    return roadway_temporal_2009_2018

""" Section 4.2.2 Prepare heavy vehicle count (HCV) fields """

def get_hcv (df):
    """
    This function pick the heavy vehicle count data in year 20XX and place it in the corresponding column AADT_20XX
    """
    df_hcv=df[['INVENTORY','BEG_STA','END_STA','HCV','HCV_MU_YR','FCNAME']]
    df_hcv.loc[(df_hcv.HCV_MU_YR == '2009'), 'HCV_2009'] = df_hcv['HCV']
    df_hcv.loc[(df_hcv.HCV_MU_YR == '2010'), 'HCV_2010'] = df_hcv['HCV']
    df_hcv.loc[(df_hcv.HCV_MU_YR == '2011'), 'HCV_2011'] = df_hcv['HCV']
    df_hcv.loc[(df_hcv.HCV_MU_YR == '2012'), 'HCV_2012'] = df_hcv['HCV']
    df_hcv.loc[(df_hcv.HCV_MU_YR == '2013'), 'HCV_2013'] = df_hcv['HCV']
    df_hcv.loc[(df_hcv.HCV_MU_YR == '2014'), 'HCV_2014'] = df_hcv['HCV']
    df_hcv.loc[(df_hcv.HCV_MU_YR == '2015'), 'HCV_2015'] = df_hcv['HCV']
    df_hcv.loc[(df_hcv.HCV_MU_YR == '2016'), 'HCV_2016'] = df_hcv['HCV']
    df_hcv.loc[(df_hcv.HCV_MU_YR == '2017'), 'HCV_2017'] = df_hcv['HCV']
    df_hcv.loc[(df_hcv.HCV_MU_YR == '2018'), 'HCV_2018'] = df_hcv['HCV']
    df_hcv=df_hcv[['INVENTORY','BEG_STA','END_STA',
                   'HCV_2018','HCV_2017','HCV_2016','HCV_2015','HCV_2014',
                   'HCV_2013','HCV_2012','HCV_2011','HCV_2010','HCV_2009','FCNAME']]

    return df_hcv

def HWY_hcv_merge (df1,df2):
    """
    This function combines IDOT HWY netowrk hcv
    """
    HWY_temporal= pd.merge(df1, df2,
                           how='left',
                           on=['INVENTORY','BEG_STA','END_STA'])

    return HWY_temporal

def HWY_hcv_replace(roadway_temporal):
    """
    This function is used for merge_all_temp_hcv() function.
    It helps merge two columns with hcv data of the same year.
    """
    roadway_temporal['HCV_2014_x']=roadway_temporal['HCV_2014_x'].fillna(roadway_temporal['HCV_2014_y'])
    roadway_temporal['HCV_2015_x']=roadway_temporal['HCV_2015_x'].fillna(roadway_temporal['HCV_2015_y'])
    roadway_temporal['HCV_2016_x']=roadway_temporal['HCV_2016_x'].fillna(roadway_temporal['HCV_2016_y'])
    roadway_temporal['HCV_2017_x']=roadway_temporal['HCV_2017_x'].fillna(roadway_temporal['HCV_2017_y'])
    roadway_temporal['HCV_2018_x']=roadway_temporal['HCV_2018_x'].fillna(roadway_temporal['HCV_2018_y'])
    roadway_temporal['HCV_2013_x']=roadway_temporal['HCV_2013_x'].fillna(roadway_temporal['HCV_2013_y'])
    roadway_temporal['HCV_2012_x']=roadway_temporal['HCV_2012_x'].fillna(roadway_temporal['HCV_2012_y'])
    roadway_temporal['HCV_2011_x']=roadway_temporal['HCV_2011_x'].fillna(roadway_temporal['HCV_2011_y'])
    roadway_temporal['HCV_2010_x']=roadway_temporal['HCV_2010_x'].fillna(roadway_temporal['HCV_2010_y'])
    roadway_temporal['HCV_2009_x']=roadway_temporal['HCV_2009_x'].fillna(roadway_temporal['HCV_2009_y'])
    roadway_temporal=roadway_temporal[['INVENTORY','BEG_STA','END_STA',
                                       'HCV_2018_x','HCV_2017_x','HCV_2016_x','HCV_2015_x','HCV_2014_x',
                                       'HCV_2013_x','HCV_2012_x','HCV_2011_x','HCV_2010_x','HCV_2009_x',
                                       'FCNAME_x']]

    return roadway_temporal

def HWY_hcv_replace_2(roadway_temporal):
    """
    This function is used for merge_all_temp_hcv() function.
    It helps merge two columns with hcv data of the same year.
    """
    roadway_temporal['HCV_2014_x']=roadway_temporal['HCV_2014_x'].fillna(roadway_temporal['HCV_2014'])
    roadway_temporal['HCV_2015_x']=roadway_temporal['HCV_2015_x'].fillna(roadway_temporal['HCV_2015'])
    roadway_temporal['HCV_2016_x']=roadway_temporal['HCV_2016_x'].fillna(roadway_temporal['HCV_2016'])
    roadway_temporal['HCV_2017_x']=roadway_temporal['HCV_2017_x'].fillna(roadway_temporal['HCV_2017'])
    roadway_temporal['HCV_2018_x']=roadway_temporal['HCV_2018_x'].fillna(roadway_temporal['HCV_2018'])
    roadway_temporal['HCV_2013_x']=roadway_temporal['HCV_2013_x'].fillna(roadway_temporal['HCV_2013'])
    roadway_temporal['HCV_2012_x']=roadway_temporal['HCV_2012_x'].fillna(roadway_temporal['HCV_2012'])
    roadway_temporal['HCV_2011_x']=roadway_temporal['HCV_2011_x'].fillna(roadway_temporal['HCV_2011'])
    roadway_temporal['HCV_2010_x']=roadway_temporal['HCV_2010_x'].fillna(roadway_temporal['HCV_2010'])
    roadway_temporal['HCV_2009_x']=roadway_temporal['HCV_2009_x'].fillna(roadway_temporal['HCV_2009'])
    roadway_temporal=roadway_temporal[['INVENTORY','BEG_STA','END_STA',
                                       'HCV_2018_x','HCV_2017_x','HCV_2016_x','HCV_2015_x','HCV_2014_x',
                                       'HCV_2013_x','HCV_2012_x','HCV_2011_x','HCV_2010_x','HCV_2009_x',
                                       'FCNAME_x']]

    return roadway_temporal

def merge_all_temp_hcv(HWY_temporal_2018_hcv,HWY_temporal_2017_hcv,HWY_temporal_2016_hcv,HWY_temporal_2015_hcv,HWY_temporal_2014_hcv,HWY_temporal_2013_hcv,HWY_temporal_2012_hcv,HWY_temporal_2011_hcv,HWY_temporal_2010_hcv,HWY_temporal_2009_hcv):
    """
    This function merges hcv from 2009 to 2018 year by year (descending order)
    """
    HCV_temporal_2017_2018=HWY_hcv_merge (HWY_temporal_2018_hcv, HWY_temporal_2017_hcv)
    HCV_temporal_2017_2018=HWY_hcv_replace(HCV_temporal_2017_2018)
    HCV_temporal_2016_2018=HWY_hcv_merge (HCV_temporal_2017_2018, HWY_temporal_2016_hcv)
    HCV_temporal_2016_2018=HWY_hcv_replace_2(HCV_temporal_2016_2018)
    HCV_temporal_2015_2018=HWY_hcv_merge (HCV_temporal_2016_2018, HWY_temporal_2015_hcv)
    HCV_temporal_2015_2018=HWY_hcv_replace_2(HCV_temporal_2015_2018)
    HCV_temporal_2014_2018=HWY_hcv_merge (HCV_temporal_2015_2018, HWY_temporal_2014_hcv)
    HCV_temporal_2014_2018=HWY_hcv_replace_2(HCV_temporal_2014_2018)
    HCV_temporal_2013_2018=HWY_hcv_merge (HCV_temporal_2014_2018, HWY_temporal_2013_hcv)
    HCV_temporal_2013_2018=HWY_hcv_replace_2(HCV_temporal_2013_2018)
    HCV_temporal_2012_2018=HWY_hcv_merge (HCV_temporal_2013_2018, HWY_temporal_2012_hcv)
    HCV_temporal_2012_2018=HWY_hcv_replace_2(HCV_temporal_2012_2018)
    HCV_temporal_2011_2018=HWY_hcv_merge (HCV_temporal_2012_2018, HWY_temporal_2011_hcv)
    HCV_temporal_2011_2018=HWY_hcv_replace_2(HCV_temporal_2011_2018)
    HCV_temporal_2010_2018=HWY_hcv_merge (HCV_temporal_2011_2018, HWY_temporal_2010_hcv)
    HCV_temporal_2010_2018=HWY_hcv_replace_2(HCV_temporal_2010_2018)
    HCV_temporal_2009_2018=HWY_hcv_merge (HCV_temporal_2010_2018, HWY_temporal_2009_hcv)
    HCV_temporal_2009_2018=HWY_hcv_replace_2(HCV_temporal_2009_2018)
    HCV_temporal_2009_2018=HCV_temporal_2009_2018.rename(columns={"HCV_2014_x": "HCV_2014",
                                                                  "HCV_2015_x": "HCV_2015",
                                                                  "HCV_2016_x": "HCV_2016",
                                                                  "HCV_2017_x": "HCV_2017",
                                                                  "HCV_2018_x": "HCV_2018",
                                                                  "HCV_2013_x": "HCV_2013",
                                                                  "HCV_2012_x": "HCV_2012",
                                                                  "HCV_2011_x": "HCV_2011",
                                                                  "HCV_2010_x": "HCV_2010",
                                                                  "HCV_2009_x": "HCV_2009",
                                                                  "FCNAME_x":"FCNAME"})
    HCV_temporal_2009_2018=HCV_temporal_2009_2018[['INVENTORY','BEG_STA','END_STA',
                                                   'HCV_2018','HCV_2017','HCV_2016','HCV_2015','HCV_2014',
                                                   'HCV_2013','HCV_2012','HCV_2011','HCV_2010','HCV_2009',
                                                   'FCNAME']]

    return HCV_temporal_2009_2018

# For each road function class, interpolate the missing hcv data basing on the following years
## The dict keys represent roadway function class
## {0:Interstate, 1:Local Roads or Street, 2:Major Arterial, 3:Major Collector, 4:Minor Arterial, 5:Minor Collector}
## For each road function class (key), years that have hcv data are listed in the dict values
## The details can be examined by HCV_temporal_2009_2018.groupby(['FCNAME']).count()
hcv_train_years={0:np.array([[2017], [2015], [2013], [2011], [2009]]),
             2:np.array([[2017], [2015], [2013]]),
             3:np.array([[2017], [2015], [2013]]),
             4:np.array([[2017], [2015], [2013]])
            }
hcv_years=[2018,2017,2016,2015,2014,2013,2012,2011,2010,2009]

def hcv_get_train_index (hcv_train_year,shcv_years,n):
    """
    This function is used for hcv_train_index{} dict.
    It helps identify column index of years in the hcv_fc dataframe
    """
    index=[]
    for i in range (0,len(hcv_train_years[n])):
        index.append(hcv_years.index(hcv_train_years[n][i].tolist()[0])+2)

    return index

# Years that have true hcv data by index, can be used for extrapolation later
## The keys represent roadway functon class
## {0:Interstate, 1:Local Roads or Street, 2:Major Arterial, 3:Major Collector, 4:Minor Arterial, 5:Minor Collector}
## The values represent year (index)
## {2019:1, 2018:2, 2017:3, 2016:5 ... 2009:11}
## hcv_train_index = {0: [3, 5, 7, 9, 11], 2: [3, 5, 7], 3: [3, 5, 7], 4: [3, 5, 7]}
hcv_train_index={0:hcv_get_train_index (hcv_train_years,hcv_years,0),
             2:hcv_get_train_index (hcv_train_years,hcv_years,2),
             3:hcv_get_train_index (hcv_train_years,hcv_years,3),
             4:hcv_get_train_index (hcv_train_years,hcv_years,4)}

def hcv_get_predict_years (hcv_years,hcv_train_years,n):
    """
    This function is used for hcv_predict_years{} dict
    It helps identify years that need to extrapolate hcv
    """
    hcv_predict_years=np.setdiff1d(hcv_years,[item for sublist in hcv_train_years[n].tolist() for item in sublist])

    return (np.array([[el] for el in hcv_predict_years.tolist()]))

# Years that need extrapolate hcv data
## The keys represent roadway functon class
## {0:Interstate, 1:Local Roads or Street, 2:Major Arterial, 3:Major Collector, 4:Minor Arterial, 5:Minor Collector}
## {0: array([[2010],[2012],[2014],[2016],[2018]]),...,5: array([[2009],[2010],[2011],[2012],[2014],[2016],[2018]])}
hcv_predict_years={0:hcv_get_predict_years(hcv_years,hcv_train_years,0),
             2:hcv_get_predict_years(hcv_years,hcv_train_years,2),
             3:hcv_get_predict_years(hcv_years,hcv_train_years,3),
             4:hcv_get_predict_years(hcv_years,hcv_train_years,4)}

def hcv_get_predict_index (hcv_years,hcv_predict_years,n):
    """
    This function is used for hcv_predict_index{}
    It helps identify column index of these years in the hcv_fc dataframe.
    """
    index=[]
    for i in range (0,len(hcv_predict_years[n])):
        index.append(hcv_years.index(hcv_predict_years[n][i].tolist()[0])+2)

    return index

# Years that need to extrapolate hcv data by index
## The keys represent roadway functon class
## {0:Interstate, 1:Local Roads or Street, 2:Major Arterial, 3:Major Collector, 4:Minor Arterial, 5:Minor Collector}
hcv_predict_index={0:hcv_get_predict_index (hcv_years,hcv_predict_years,0),
             2:hcv_get_predict_index (hcv_years,hcv_predict_years,2),
             3:hcv_get_predict_index (hcv_years,hcv_predict_years,3),
             4:hcv_get_predict_index (hcv_years,hcv_predict_years,4)}

def update_hcv_fc (hcv_regsr,hcv_fc,hcv_predict_years,hcv_predict_index,hcv_train_years,hcv_train_index,n):
    """
    This function  ues LinearRegression method to update (interpolate and extrapolate) hcv functional class by function class
    The last input n represents road function class
    {0:Interstate, 1:Local Roads or Street, 2:Major Arterial, 3:Major Collector, 4:Minor Arterial, 5:Minor Collector}
    """
    X=hcv_train_years[n]
    y=hcv_fc.iloc[n,hcv_train_index[n]].values.reshape(-1,1)
    x_predict=hcv_predict_years[n]
    hcv_regsr.fit(X,y)
    y_predict=hcv_regsr.predict(x_predict)
    for i in range(0,len(hcv_predict_index[n])):
        hcv_fc.iloc[n,hcv_predict_index[n][i]]=y_predict[i]
    return hcv_fc

def hcv_fc_pct(hcv_fc):
    """
    This function calculate hcv percentage change assuming different years as the base year
    It creates a df for each year within (2009,2019) and assembles the dfs in a dict
    """
    hcv_fc_pt_year={}
    for k in range (2009,2019):
        hcv_fc_pt=hcv_fc.copy()
        for i in [0,1,2,3,4,5]:
            for j in range (2,12):
                hcv_fc_pt.iloc[i,j]=(hcv_fc.iloc[i,j]-hcv_fc.iloc[i,(2020-k)])/hcv_fc.iloc[i,(2020-k)]
            hcv_fc_pt.iloc[i,(2020-k)]=0
        hcv_fc_pt_year.update( {k : hcv_fc_pt} )
    return hcv_fc_pt_year

def upd_miss_hcv_row(df,hcv_fc,hcv_fc_pt_year):
    """
    This function updates the hcv numbers for the segment database, based on years with hcv numbers available and functional classfication
    The detailed strategies see comments in hcv_avl_1() ~ hcv_avl_9()
    """
    def hcv_avl_1(fc,hcv_fc_pt_year,i):## If there is only one year with hcv number available
        base_hcv_index=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2]
        hcv_year_index=2020-base_hcv_index[0]
        ## Use this year as base year, and apply average percentage change by functiona class for each year
        for j in range (2,12):
            df.iloc[i,j]= (1+hcv_fc_pt_year.get(hcv_year_index).iloc[fc,j])*df.iloc[fc,base_hcv_index[0]]
    def hcv_avl_2(fc,hcv_fc_pt_year,i): ## If there are two years with hcv numbers available
        base_hcv_index_1=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][0]
        base_hcv_index_2=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][1]
        year_index_1=2020-base_hcv_index_1
        year_index_2=2020-base_hcv_index_2
        for j in range (2,base_hcv_index_1+1):
        ## Use the first year as base year, and apply average percentage change by functiona class for each year before this year
            df.iloc[i,j]= (1+hcv_fc_pt_year.get(year_index_1).iloc[fc,j])*df.iloc[i,base_hcv_index_1]
        for j in range (base_hcv_index_2+1,12):
        ## Use the second year as base year, and apply average percentage change by functiona class for each year after this year
            df.iloc[i,j]= (1+hcv_fc_pt_year.get(year_index_2).iloc[fc,j])*df.iloc[i,base_hcv_index_2]
        for j in range (base_hcv_index_1+1,base_hcv_index_2):
        ## Calculate average percentage change for years in-between and extrapolate hcv
            rate=(df.iloc[i,base_hcv_index_1]-df.iloc[i,base_hcv_index_2])/(base_hcv_index_2-base_hcv_index_1)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_1]+rate*(j-base_hcv_index_1)
    def hcv_avl_3(fc,hcv_fc_pt_year,i): ## If there are three years with hcv numbers available
        base_hcv_index_1=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][0]
        base_hcv_index_2=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][1]
        base_hcv_index_3=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][2]
        year_index_1=2020-base_hcv_index_1
        year_index_2=2020-base_hcv_index_2
        year_index_3=2020-base_hcv_index_3
        for j in range (2,base_hcv_index_1+1):
            df.iloc[i,j]= (1+hcv_fc_pt_year.get(year_index_1).iloc[fc,j])*df.iloc[i,base_hcv_index_1]
        for j in range (base_hcv_index_3+1,12):
            df.iloc[i,j]= (1+hcv_fc_pt_year.get(year_index_3).iloc[fc,j])*df.iloc[i,base_hcv_index_3]
        for j in range (base_hcv_index_1+1,base_hcv_index_2):
            rate=(df.iloc[i,base_hcv_index_1]-df.iloc[i,base_hcv_index_2])/(base_hcv_index_2-base_hcv_index_1)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_1]+rate*(j-base_hcv_index_1)
        for j in range (base_hcv_index_2+1,base_hcv_index_3):
            rate=(df.iloc[i,base_hcv_index_2]-df.iloc[i,base_hcv_index_3])/(base_hcv_index_3-base_hcv_index_2)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_2]+rate*(j-base_hcv_index_2)
    def hcv_avl_4(fc,hcv_fc_pt_year,i): ## If there are four years with hcv numbers available
        base_hcv_index_1=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][0]
        base_hcv_index_2=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][1]
        base_hcv_index_3=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][2]
        base_hcv_index_4=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][3]
        year_index_1=2020-base_hcv_index_1
        year_index_2=2020-base_hcv_index_2
        year_index_3=2020-base_hcv_index_3
        year_index_4=2020-base_hcv_index_4
        for j in range (2,base_hcv_index_1+1):
            df.iloc[i,j]= (1+hcv_fc_pt_year.get(year_index_1).iloc[fc,j])*df.iloc[i,base_hcv_index_1]
        for j in range (base_hcv_index_4+1,12):
            df.iloc[i,j]= (1+hcv_fc_pt_year.get(year_index_4).iloc[fc,j])*df.iloc[i,base_hcv_index_4]
        for j in range (base_hcv_index_1+1,base_hcv_index_2):
            rate=(df.iloc[i,base_hcv_index_1]-df.iloc[i,base_hcv_index_2])/(base_hcv_index_2-base_hcv_index_1)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_1]+rate*(j-base_hcv_index_1)
        for j in range (base_hcv_index_2+1,base_hcv_index_3):
            rate=(df.iloc[i,base_hcv_index_2]-df.iloc[i,base_hcv_index_3])/(base_hcv_index_3-base_hcv_index_2)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_2]+rate*(j-base_hcv_index_2)
        for j in range (base_hcv_index_3+1,base_hcv_index_4):
            rate=(df.iloc[i,base_hcv_index_3]-df.iloc[i,base_hcv_index_4])/(base_hcv_index_4-base_hcv_index_3)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_3]+rate*(j-base_hcv_index_3)
    def hcv_avl_5(fc,hcv_fc_pt_year,i): ## If there are five years with hcv numbers available
        base_hcv_index_1=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][0]
        base_hcv_index_2=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][1]
        base_hcv_index_3=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][2]
        base_hcv_index_4=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][3]
        base_hcv_index_5=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][4]
        year_index_1=2020-base_hcv_index_1
        year_index_2=2020-base_hcv_index_2
        year_index_3=2020-base_hcv_index_3
        year_index_4=2020-base_hcv_index_4
        year_index_5=2020-base_hcv_index_5
        for j in range (2,base_hcv_index_1+1):
            df.iloc[i,j]= (1+hcv_fc_pt_year.get(year_index_1).iloc[fc,j])*df.iloc[i,base_hcv_index_1]
        for j in range (base_hcv_index_5+1,12):
            df.iloc[i,j]= (1+hcv_fc_pt_year.get(year_index_5).iloc[fc,j])*df.iloc[i,base_hcv_index_5]
        for j in range (base_hcv_index_1+1,base_hcv_index_2):
            rate=(df.iloc[i,base_hcv_index_1]-df.iloc[i,base_hcv_index_2])/(base_hcv_index_2-base_hcv_index_1)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_1]+rate*(j-base_hcv_index_1)
        for j in range (base_hcv_index_2+1,base_hcv_index_3):
            rate=(df.iloc[i,base_hcv_index_2]-df.iloc[i,base_hcv_index_3])/(base_hcv_index_3-base_hcv_index_2)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_2]+rate*(j-base_hcv_index_2)
        for j in range (base_hcv_index_3+1,base_hcv_index_4):
            rate=(df.iloc[i,base_hcv_index_3]-df.iloc[i,base_hcv_index_4])/(base_hcv_index_4-base_hcv_index_3)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_3]+rate*(j-base_hcv_index_3)
        for j in range (base_hcv_index_4+1,base_hcv_index_5):
            rate=(df.iloc[i,base_hcv_index_4]-df.iloc[i,base_hcv_index_5])/(base_hcv_index_5-base_hcv_index_4)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_4]+rate*(j-base_hcv_index_4)
    def hcv_avl_6(fc,hcv_fc_pt_year,i): ## If there are six years with hcv numbers available
        base_hcv_index_1=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][0]
        base_hcv_index_2=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][1]
        base_hcv_index_3=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][2]
        base_hcv_index_4=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][3]
        base_hcv_index_5=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][4]
        base_hcv_index_6=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][5]
        year_index_1=2020-base_hcv_index_1
        year_index_2=2020-base_hcv_index_2
        year_index_3=2020-base_hcv_index_3
        year_index_4=2020-base_hcv_index_4
        year_index_5=2020-base_hcv_index_5
        year_index_6=2020-base_hcv_index_6
        for j in range (2,base_hcv_index_1+1):
            df.iloc[i,j]= (1+hcv_fc_pt_year.get(year_index_1).iloc[fc,j])*df.iloc[i,base_hcv_index_1]
        for j in range (base_hcv_index_6+1,12):
            df.iloc[i,j]= (1+hcv_fc_pt_year.get(year_index_6).iloc[fc,j])*df.iloc[i,base_hcv_index_6]
        for j in range (base_hcv_index_1+1,base_hcv_index_2):
            rate=(df.iloc[i,base_hcv_index_1]-df.iloc[i,base_hcv_index_2])/(base_hcv_index_2-base_hcv_index_1)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_1]+rate*(j-base_hcv_index_1)
        for j in range (base_hcv_index_2+1,base_hcv_index_3):
            rate=(df.iloc[i,base_hcv_index_2]-df.iloc[i,base_hcv_index_3])/(base_hcv_index_3-base_hcv_index_2)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_2]+rate*(j-base_hcv_index_2)
        for j in range (base_hcv_index_3+1,base_hcv_index_4):
            rate=(df.iloc[i,base_hcv_index_3]-df.iloc[i,base_hcv_index_4])/(base_hcv_index_4-base_hcv_index_3)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_3]+rate*(j-base_hcv_index_3)
        for j in range (base_hcv_index_4+1,base_hcv_index_5):
            rate=(df.iloc[i,base_hcv_index_4]-df.iloc[i,base_hcv_index_5])/(base_hcv_index_5-base_hcv_index_4)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_4]+rate*(j-base_hcv_index_4)
        for j in range (base_hcv_index_5+1,base_hcv_index_6):
            rate=(df.iloc[i,base_hcv_index_5]-df.iloc[i,base_hcv_index_6])/(base_hcv_index_6-base_hcv_index_5)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_5]+rate*(j-base_hcv_index_5)
    def hcv_avl_7(fc,hcv_fc_pt_year,i): ## If there are seven years with hcv numbers available
        base_hcv_index_1=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][0]
        base_hcv_index_2=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][1]
        base_hcv_index_3=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][2]
        base_hcv_index_4=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][3]
        base_hcv_index_5=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][4]
        base_hcv_index_6=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][5]
        base_hcv_index_7=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][6]
        year_index_1=2020-base_hcv_index_1
        year_index_2=2020-base_hcv_index_2
        year_index_3=2020-base_hcv_index_3
        year_index_4=2020-base_hcv_index_4
        year_index_5=2020-base_hcv_index_5
        year_index_6=2020-base_hcv_index_6
        year_index_7=2020-base_hcv_index_7
        for j in range (2,base_hcv_index_1+1):
            df.iloc[i,j]= (1+hcv_fc_pt_year.get(year_index_1).iloc[fc,j])*df.iloc[i,base_hcv_index_1]
        for j in range (base_hcv_index_7+1,12):
            df.iloc[i,j]= (1+hcv_fc_pt_year.get(year_index_7).iloc[fc,j])*df.iloc[i,base_hcv_index_7]
        for j in range (base_hcv_index_1+1,base_hcv_index_2):
            rate=(df.iloc[i,base_hcv_index_1]-df.iloc[i,base_hcv_index_2])/(base_hcv_index_2-base_hcv_index_1)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_1]+rate*(j-base_hcv_index_1)
        for j in range (base_hcv_index_2+1,base_hcv_index_3):
            rate=(df.iloc[i,base_hcv_index_2]-df.iloc[i,base_hcv_index_3])/(base_hcv_index_3-base_hcv_index_2)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_2]+rate*(j-base_hcv_index_2)
        for j in range (base_hcv_index_3+1,base_hcv_index_4):
            rate=(df.iloc[i,base_hcv_index_3]-df.iloc[i,base_hcv_index_4])/(base_hcv_index_4-base_hcv_index_3)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_3]+rate*(j-base_hcv_index_3)
        for j in range (base_hcv_index_4+1,base_hcv_index_5):
            rate=(df.iloc[i,base_hcv_index_4]-df.iloc[i,base_hcv_index_5])/(base_hcv_index_5-base_hcv_index_4)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_4]+rate*(j-base_hcv_index_4)
        for j in range (base_hcv_index_5+1,base_hcv_index_6):
            rate=(df.iloc[i,base_hcv_index_5]-df.iloc[i,base_hcv_index_6])/(base_hcv_index_6-base_hcv_index_5)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_5]+rate*(j-base_hcv_index_5)
        for j in range (base_hcv_index_6+1,base_hcv_index_7):
            rate=(df.iloc[i,base_hcv_index_6]-df.iloc[i,base_hcv_index_7])/(base_hcv_index_7-base_hcv_index_6)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_6]+rate*(j-base_hcv_index_6)
    def hcv_avl_8(fc,hcv_fc_pt_year,i): ## If there are eight years with hcv numbers available
        base_hcv_index_1=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][0]
        base_hcv_index_2=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][1]
        base_hcv_index_3=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][2]
        base_hcv_index_4=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][3]
        base_hcv_index_5=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][4]
        base_hcv_index_6=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][5]
        base_hcv_index_7=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][6]
        base_hcv_index_8=np.where(np.array(df.iloc[i].isnull())==False)[0][2:-2][7]
        year_index_1=2020-base_hcv_index_1
        year_index_2=2020-base_hcv_index_2
        year_index_3=2020-base_hcv_index_3
        year_index_4=2020-base_hcv_index_4
        year_index_5=2020-base_hcv_index_5
        year_index_6=2020-base_hcv_index_6
        year_index_7=2020-base_hcv_index_7
        year_index_8=2020-base_hcv_index_8
        for j in range (2,base_hcv_index_1+1):
            df.iloc[i,j]= (1+hcv_fc_pt_year.get(year_index_1).iloc[fc,j])*df.iloc[i,base_hcv_index_1]
        for j in range (base_hcv_index_7+1,12):
            df.iloc[i,j]= (1+hcv_fc_pt_year.get(year_index_7).iloc[fc,j])*df.iloc[i,base_hcv_index_7]
        for j in range (base_hcv_index_1+1,base_hcv_index_2):
            rate=(df.iloc[i,base_hcv_index_1]-df.iloc[i,base_hcv_index_2])/(base_hcv_index_2-base_hcv_index_1)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_1]+rate*(j-base_hcv_index_1)
        for j in range (base_hcv_index_2+1,base_hcv_index_3):
            rate=(df.iloc[i,base_hcv_index_2]-df.iloc[i,base_hcv_index_3])/(base_hcv_index_3-base_hcv_index_2)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_2]+rate*(j-base_hcv_index_2)
        for j in range (base_hcv_index_3+1,base_hcv_index_4):
            rate=(df.iloc[i,base_hcv_index_3]-df.iloc[i,base_hcv_index_4])/(base_hcv_index_4-base_hcv_index_3)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_3]+rate*(j-base_hcv_index_3)
        for j in range (base_hcv_index_4+1,base_hcv_index_5):
            rate=(df.iloc[i,base_hcv_index_4]-df.iloc[i,base_hcv_index_5])/(base_hcv_index_5-base_hcv_index_4)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_4]+rate*(j-base_hcv_index_4)
        for j in range (base_hcv_index_5+1,base_hcv_index_6):
            rate=(df.iloc[i,base_hcv_index_5]-df.iloc[i,base_hcv_index_6])/(base_hcv_index_6-base_hcv_index_5)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_5]+rate*(j-base_hcv_index_5)
        for j in range (base_hcv_index_6+1,base_hcv_index_7):
            rate=(df.iloc[i,base_hcv_index_6]-df.iloc[i,base_hcv_index_7])/(base_hcv_index_7-base_hcv_index_6)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_6]+rate*(j-base_hcv_index_6)
        for j in range (base_hcv_index_7+1,base_hcv_index_8):
            rate=(df.iloc[i,base_hcv_index_7]-df.iloc[i,base_hcv_index_8])/(base_hcv_index_8-base_hcv_index_7)
            df.iloc[i,j]=df.iloc[i,base_hcv_index_7]+rate*(j-base_hcv_index_7)
    def hcv_avl_9(fc,hcv_fc_pt_year,i): ## If there are nine years with hcv numbers available
        null_year_index=np.where(np.array(df.iloc[0].isnull())==True)[0][0]
        df.iloc[i,null_year]=(df.iloc[i,null_year-1]+df.iloc[i,null_year+1])/2

    for i in range (0,df.shape[0]):
        fc=hcv_fc.index.get_loc(df.iloc[i,13])
        if fc==1 or fc==5:
            pass
        else:
            if df.iloc[i].isnull().sum()==10: # if hcv numbers are not available for all years
                pass
            elif df.iloc[i].isnull().sum()==9: # if there is only one year with hcv number available
                hcv_avl_1(fc,hcv_fc_pt_year,i)
            elif df.iloc[i].isnull().sum()==8: # If there are two years with hcv numbers available
                hcv_avl_2(fc,hcv_fc_pt_year,i)
            elif df.iloc[i].isnull().sum()==7: # If there are three years with hcv numbers available
                hcv_avl_3(fc,hcv_fc_pt_year,i)
            elif df.iloc[i].isnull().sum()==6: # If there are four years with hcv numbers available
                hcv_avl_4(fc,hcv_fc_pt_year,i)
            elif df.iloc[i].isnull().sum()==5: # If there are five years with hcv numbers available
                hcv_avl_5(fc,hcv_fc_pt_year,i)
            elif df.iloc[i].isnull().sum()==4: # If there are six years with hcv numbers available
                hcv_avl_6(fc,hcv_fc_pt_year,i)
            elif df.iloc[i].isnull().sum()==3: # If there are seven years with hcv numbers available
                hcv_avl_7(fc,hcv_fc_pt_year,i)
            elif df.iloc[i].isnull().sum()==2: # If there are eight years with hcv numbers available
                hcv_avl_8(fc,hcv_fc_pt_year,i)
            elif df.iloc[i].isnull().sum()==1: # If there are nine years with hcv numbers available
                hcv_avl_9(fc,hcv_fc_pt_year,i)
            else: ## If all years with hcv numbers are available
                print ("row i: "+str(i))
                break
    return df

def cmb_tmp_HCV_sts_hcv(HCV_temporal_2009_2018_copy_update,streets_temporal):
    """
    This function combines hcv of IDOT HWY and CCRPC PCD street by segment_id
    """
    hcv_temporal_2009_2018= pd.merge(streets_temporal,HCV_temporal_2009_2018_copy_update,
                           how='left',
                           left_on=['idot_inventory','idot_begin_station','idot_end_station'],
                           right_on = ['INVENTORY','BEG_STA','END_STA'])
    hcv_fields=['segment_id',
                 'HCV_2018','HCV_2017','HCV_2016','HCV_2015','HCV_2014',
                 'HCV_2013','HCV_2012','HCV_2011','HCV_2010','HCV_2009','functional_classification']
    hcv_temporal_2009_2018=hcv_temporal_2009_2018[hcv_fields]

    return hcv_temporal_2009_2018

""" Section 4.2.3 Prepare pavement conditin fields """

def get_crs (df):
    """
    This function gets CRS for each year
    """
    df_crs=df[['INVENTORY','BEG_STA','END_STA','CRS_LOW','CRS_YR']]
    df_crs.loc[(df_crs.CRS_YR == '2014'), 'CRS_2014'] = df_crs['CRS_LOW']
    df_crs.loc[(df_crs.CRS_YR == '2015'), 'CRS_2015'] = df_crs['CRS_LOW']
    df_crs.loc[(df_crs.CRS_YR == '2016'), 'CRS_2016'] = df_crs['CRS_LOW']
    df_crs.loc[(df_crs.CRS_YR == '2017'), 'CRS_2017'] = df_crs['CRS_LOW']
    df_crs.loc[(df_crs.CRS_YR == '2018'), 'CRS_2018'] = df_crs['CRS_LOW']
    df_crs=df_crs[['INVENTORY','BEG_STA','END_STA','CRS_2014','CRS_2015','CRS_2016','CRS_2017','CRS_2018']]

    return df_crs

def crs_merge (df1,df2):
    """
    This function merge two crs df by road segment
    """
    roadway_temporal= pd.merge(df1, df2,
                               how='left',
                               left_on=['idot_inventory','idot_begin_station','idot_end_station'],
                               right_on = ['INVENTORY','BEG_STA','END_STA'])

    return roadway_temporal

def crs_replace(roadway_temporal):
    """
    This function is used for merging streets_temporal, HWY_temporal_2018_crs, HWY_temporal_2018_crs
    """
    roadway_temporal['CRS_2014_x']=roadway_temporal['CRS_2014_x'].fillna(roadway_temporal['CRS_2014_y'])
    roadway_temporal['CRS_2015_x']=roadway_temporal['CRS_2015_x'].fillna(roadway_temporal['CRS_2015_y'])
    roadway_temporal['CRS_2016_x']=roadway_temporal['CRS_2016_x'].fillna(roadway_temporal['CRS_2016_y'])
    roadway_temporal['CRS_2017_x']=roadway_temporal['CRS_2017_x'].fillna(roadway_temporal['CRS_2017_y'])
    roadway_temporal['CRS_2018_x']=roadway_temporal['CRS_2018_x'].fillna(roadway_temporal['CRS_2018_y'])
    roadway_temporal=roadway_temporal[['segment_id',
                                       'idot_inventory','idot_begin_station','idot_end_station',
                                       'CRS_2014_x','CRS_2015_x','CRS_2016_x','CRS_2017_x','CRS_2018_x',
                                       'pavement_condition']]

    return roadway_temporal

def crs_replace_2(roadway_temporal):
    """
    This Used for merging HWY_temporal_2016_crs~HWY_temporal_2014_crs
    """
    roadway_temporal['CRS_2014_x']=roadway_temporal['CRS_2014_x'].fillna(roadway_temporal['CRS_2014'])
    roadway_temporal['CRS_2015_x']=roadway_temporal['CRS_2015_x'].fillna(roadway_temporal['CRS_2015'])
    roadway_temporal['CRS_2016_x']=roadway_temporal['CRS_2016_x'].fillna(roadway_temporal['CRS_2016'])
    roadway_temporal['CRS_2017_x']=roadway_temporal['CRS_2017_x'].fillna(roadway_temporal['CRS_2017'])
    roadway_temporal['CRS_2018_x']=roadway_temporal['CRS_2018_x'].fillna(roadway_temporal['CRS_2018'])
    roadway_temporal=roadway_temporal[['segment_id',
                                       'idot_inventory','idot_begin_station','idot_end_station',
                                       'CRS_2014_x','CRS_2015_x','CRS_2016_x','CRS_2017_x','CRS_2018_x',
                                       'pavement_condition']]

    return roadway_temporal

def merge_all_temp_crs(streets_temporal,
                       HWY_temporal_2018_crs,HWY_temporal_2017_crs,HWY_temporal_2016_crs,HWY_temporal_2015_crs,HWY_temporal_2014_crs):
    """
    This function add pavement_condition from streets_temporal and merge all the HWY_temporal_20XX together
    """
    crs_fields=['segment_id','idot_inventory','idot_begin_station','idot_end_station',
             'CRS_2014','CRS_2015','CRS_2016','CRS_2017','CRS_2018','pavement_condition']
    crs_temporal_2018=crs_merge (streets_temporal, HWY_temporal_2018_crs)
    crs_temporal_2018=crs_temporal_2018[crs_fields]
    crs_temporal_2017_2018=crs_merge (crs_temporal_2018, HWY_temporal_2017_crs)
    crs_temporal_2017_2018=crs_replace(crs_temporal_2017_2018)
    crs_temporal_2016_2018=crs_merge (crs_temporal_2017_2018, HWY_temporal_2016_crs)
    crs_temporal_2016_2018=crs_replace_2(crs_temporal_2016_2018)
    crs_temporal_2015_2018=crs_merge (crs_temporal_2016_2018, HWY_temporal_2015_crs)
    crs_temporal_2015_2018=crs_replace_2(crs_temporal_2015_2018)
    crs_temporal_2014_2018=crs_merge (crs_temporal_2015_2018, HWY_temporal_2014_crs)
    crs_temporal_2014_2018=crs_replace_2(crs_temporal_2014_2018)
    crs_temporal_2014_2018=crs_temporal_2014_2018.rename(columns={"CRS_2014_x": "CRS_2014",
                                                                  "CRS_2015_x": "CRS_2015",
                                                                  "CRS_2016_x": "CRS_2016",
                                                                  "CRS_2017_x": "CRS_2017",
                                                                  "CRS_2018_x": "CRS_2018",
                                                                 })
    crs_temporal_2014_2018=crs_temporal_2014_2018[['segment_id',
                                                   'CRS_2014','CRS_2015','CRS_2016','CRS_2017','CRS_2018',
                                                   'pavement_condition']]

    return crs_temporal_2014_2018

def upd_miss_crs(crs_temporal_2014_2018):
    """
    This function updates missing values of crs_201X
    """

    def crs_2015 (crs_temporal_2014_2018): ## Interpolate crs_2015 basing on crs_2014 and crs_2016
        for i in range(0,crs_temporal_2014_2018.shape[0]):
            if  ((crs_temporal_2014_2018.at[i,'CRS_2014'] >0) & (crs_temporal_2014_2018.at[i,'CRS_2016'] >0)):
                crs_temporal_2014_2018.at[i,'CRS_2015'] = (crs_temporal_2014_2018.at[i,'CRS_2014']+
                                                              crs_temporal_2014_2018.at[i,'CRS_2016'])/2
            elif crs_temporal_2014_2018.at[i,'CRS_2014'] >0:
                crs_temporal_2014_2018.at[i,'CRS_2015'] = crs_temporal_2014_2018.at[i,'CRS_2014']
            elif crs_temporal_2014_2018.at[i,'CRS_2016'] >0:
                crs_temporal_2014_2018.at[i,'CRS_2015'] = crs_temporal_2014_2018.at[i,'CRS_2016']
            else:
                crs_temporal_2014_2018.at[i,'CRS_2015']=np.nan
        return crs_temporal_2014_2018


    def crs_2017 (crs_temporal_2014_2018): ## Interpolate crs_2017 basing on crs_2016 and crs_2018
        for i in range(0,crs_temporal_2014_2018.shape[0]):
            if  ((crs_temporal_2014_2018.at[i,'CRS_2016'] >0) & (crs_temporal_2014_2018.at[i,'CRS_2018'] >0)):
                crs_temporal_2014_2018.at[i,'CRS_2017'] = (crs_temporal_2014_2018.at[i,'CRS_2016']+
                                                              crs_temporal_2014_2018.at[i,'CRS_2018'])/2
            elif crs_temporal_2014_2018.at[i,'CRS_2016'] >0:
                crs_temporal_2014_2018.at[i,'CRS_2017'] = crs_temporal_2014_2018.at[i,'CRS_2016']
            elif crs_temporal_2014_2018.at[i,'CRS_2018'] >0:
                crs_temporal_2014_2018.at[i,'CRS_2017'] = crs_temporal_2014_2018.at[i,'CRS_2018']
            else:
                crs_temporal_2014_2018.at[i,'CRS_2017']=np.nan
        return crs_temporal_2014_2018

    def crs_nan (crs_temporal_2014_2018): ## Assign the nearest available value
        for i in range(0,crs_temporal_2014_2018.shape[0]):
            if np.isnan(crs_temporal_2014_2018.at[i,'CRS_2018']):
                if np.isnan(crs_temporal_2014_2018.at[i,'CRS_2017']):
                    crs_temporal_2014_2018.at[i,'CRS_2018']=crs_temporal_2014_2018.at[i,'CRS_2016']
                else:
                    crs_temporal_2014_2018.at[i,'CRS_2018']=crs_temporal_2014_2018.at[i,'CRS_2017']
            if np.isnan(crs_temporal_2014_2018.at[i,'CRS_2017']):
                if np.isnan(crs_temporal_2014_2018.at[i,'CRS_2016']):
                    crs_temporal_2014_2018.at[i,'CRS_2017']=crs_temporal_2014_2018.at[i,'CRS_2015']
                else:
                    crs_temporal_2014_2018.at[i,'CRS_2017']=crs_temporal_2014_2018.at[i,'CRS_2016']
            if np.isnan(crs_temporal_2014_2018.at[i,'CRS_2016']):
                if np.isnan(crs_temporal_2014_2018.at[i,'CRS_2017']):
                    crs_temporal_2014_2018.at[i,'CRS_2016']=crs_temporal_2014_2018.at[i,'CRS_2015']
                else:
                    crs_temporal_2014_2018.at[i,'CRS_2016']=crs_temporal_2014_2018.at[i,'CRS_2017']
            if np.isnan(crs_temporal_2014_2018.at[i,'CRS_2015']):
                crs_temporal_2014_2018.at[i,'CRS_2015']=crs_temporal_2014_2018.at[i,'CRS_2016']
            if np.isnan(crs_temporal_2014_2018.at[i,'CRS_2014']):
                crs_temporal_2014_2018.at[i,'CRS_2014']=crs_temporal_2014_2018.at[i,'CRS_2015']
        return crs_temporal_2014_2018

    crs_temporal_2014_2018_final=crs_2015 (crs_temporal_2014_2018)
    crs_temporal_2014_2018_final=crs_2017 (crs_temporal_2014_2018_final)
    crs_temporal_2014_2018_final=crs_nan (crs_temporal_2014_2018_final)

    return crs_temporal_2014_2018_final

def crs_pc_cond_eva(crs_temporal_2014_2018_final, 
                    evalbins = [0.0,2.0,4.5,6.0,7.5,9.0], 
                    eval_labels =['Failed','Poor','Fair','Good','Excellent'] ):
    """
    This function converts crs(numbers) to pavement_conditions (text)
    """
    crs_temporal_2014_2018_final['PC_2018'] = pd.cut(crs_temporal_2014_2018_final['CRS_2018'], bins=evalbins,labels=eval_labels)
    crs_temporal_2014_2018_final['PC_2017'] = pd.cut(crs_temporal_2014_2018_final['CRS_2017'], bins=evalbins,labels=eval_labels)
    crs_temporal_2014_2018_final['PC_2016'] = pd.cut(crs_temporal_2014_2018_final['CRS_2016'], bins=evalbins,labels=eval_labels)
    crs_temporal_2014_2018_final['PC_2015'] = pd.cut(crs_temporal_2014_2018_final['CRS_2015'], bins=evalbins,labels=eval_labels)
    crs_temporal_2014_2018_final['PC_2014'] = pd.cut(crs_temporal_2014_2018_final['CRS_2014'], bins=evalbins,labels=eval_labels)

    return crs_temporal_2014_2018_final

def pc_fill_sts(crs_temporal_2014_2018_final):
    """
    For each column in 'PC_2018~PC_2014', if it's null, then assign pavement_condition from streets_temporal
    """
    for i in range(0,crs_temporal_2014_2018_final.shape[0]):
        for j in list(range (7,12)):
            if pd.isnull(crs_temporal_2014_2018_final.iloc[i,j]):
                crs_temporal_2014_2018_final.iloc[i,j]=crs_temporal_2014_2018_final.iloc[i,6]

    return crs_temporal_2014_2018_final

def pc_fill_unk(crs_temporal_2014_2018_final):
    """
    This function fills unknown pavement condition "PC_20XX" with 'Unknown'
    """
    crs_temporal_2014_2018_final['PC_2018']=crs_temporal_2014_2018_final['PC_2018'].cat.add_categories('Unknown')
    crs_temporal_2014_2018_final['PC_2018'].fillna('Unknown', inplace =True)
    crs_temporal_2014_2018_final['PC_2017']=crs_temporal_2014_2018_final['PC_2017'].cat.add_categories('Unknown')
    crs_temporal_2014_2018_final['PC_2017'].fillna('Unknown', inplace =True)
    crs_temporal_2014_2018_final['PC_2016']=crs_temporal_2014_2018_final['PC_2016'].cat.add_categories('Unknown')
    crs_temporal_2014_2018_final['PC_2016'].fillna('Unknown', inplace =True)
    crs_temporal_2014_2018_final['PC_2015']=crs_temporal_2014_2018_final['PC_2015'].cat.add_categories('Unknown')
    crs_temporal_2014_2018_final['PC_2015'].fillna('Unknown', inplace =True)
    crs_temporal_2014_2018_final['PC_2014']=crs_temporal_2014_2018_final['PC_2014'].cat.add_categories('Unknown')
    crs_temporal_2014_2018_final['PC_2014'].fillna('Unknown', inplace =True)
    return crs_temporal_2014_2018_final

def crs_pc_deter_rate(crs_temporal_2014_2018_final):
    """
    This function helps examine pavement condition deterioration rate
    """
    crs_temporal_rate=crs_temporal_2014_2018_final.copy().drop_duplicates(subset=['segment_id'], keep='first')
    crs_temporal_rate['rate_2018']=crs_temporal_rate['CRS_2018']-crs_temporal_rate['CRS_2017']
    crs_temporal_rate['rate_2017']=crs_temporal_rate['CRS_2017']-crs_temporal_rate['CRS_2017']
    crs_temporal_rate['rate_2016']=crs_temporal_rate['CRS_2016']-crs_temporal_rate['CRS_2016']
    crs_temporal_rate['rate_2015']=crs_temporal_rate['CRS_2015']-crs_temporal_rate['CRS_2015']
    crs_temporal_rate['PV_2018'] = pd.cut(crs_temporal_rate['CRS_2018'], 
                                          bins=[0.0,2.0,4.5,6.0,7.5,9.0],labels=['Failed','Poor','Fair','Good','Excellent'])
    crs_temporal_rate['PV_2017'] = pd.cut(crs_temporal_rate['CRS_2017'], 
                                          bins=[0.0,2.0,4.5,6.0,7.5,9.0],labels=['Failed','Poor','Fair','Good','Excellent'])
    crs_temporal_rate['PV_2016'] = pd.cut(crs_temporal_rate['CRS_2016'], 
                                          bins=[0.0,2.0,4.5,6.0,7.5,9.0],labels=['Failed','Poor','Fair','Good','Excellent'])
    crs_temporal_rate['PV_2015'] = pd.cut(crs_temporal_rate['CRS_2015'], 
                                          bins=[0.0,2.0,4.5,6.0,7.5,9.0],labels=['Failed','Poor','Fair','Good','Excellent'])
    crs_temporal_rate['PV_2014'] = pd.cut(crs_temporal_rate['CRS_2014'], 
                                          bins=[0.0,2.0,4.5,6.0,7.5,9.0],labels=['Failed','Poor','Fair','Good','Excellent'])
    crs_temporal_rate['PV_rate']=crs_temporal_rate['PV_2018'].astype(
        str)+' - '+crs_temporal_rate['PV_2017'].astype(
        str)+' - '+crs_temporal_rate['PV_2016'].astype(
        str)+' - '+crs_temporal_rate['PV_2015'].astype(
        str)+' - '+crs_temporal_rate['PV_2014'].astype(str)

    return crs_temporal_rate

""" Section 4.2.4 Join demographic features """

def fill_null_strs_pop(streets_pop):
    """
    This function deals with null in 'Pop_den_2017'
    """
    streets_pop=streets_pop.sort_values(by=['segment_id'])
    streets_pop=streets_pop.drop_duplicates(subset=['segment_id'], keep='first')
    streets_pop=streets_pop.reset_index()
    for i in range (0,streets_pop.shape[0]):
        if pd.isnull(streets_pop.at[i,'Pop_den_2017']):
            streets_pop.at[i,'Pop_den_2017']=streets_pop.at[i,'Pop_den_2016']
            streets_pop.at[i,'Pop_den_2018']=streets_pop.at[i,'Pop_den_2016']

    return streets_pop

def merge_aadt_hcv(roadway_temporal_2014_2018, hcv_temporal_2014_2018,hcv_share):
    """
    This function:
    (1) Merges AADT and HCV
    (2) Populates missing heavy vehicle count numbers (HCV) with a percentage of the AADT (0.07)
    """
    roadway_temporal_volume =pd.merge(roadway_temporal_2014_2018, hcv_temporal_2014_2018,
                                      on=['segment_id'],how='outer')

    # Populate missing heavy vehicle count numbers (HCV) with a percentage of the AADT (hcv_share = 0.07).
    for i in range(0,roadway_temporal_volume.shape[0]):
        if roadway_temporal_volume.at[i,'HCV_2018'] >0:
            pass
        else: ## if HCV_2018 <=0
            roadway_temporal_volume.at[i,'HCV_2018']=roadway_temporal_volume.at[i,'AADT_2018']*hcv_share
            roadway_temporal_volume.at[i,'HCV_2017']=roadway_temporal_volume.at[i,'AADT_2017']*hcv_share
            roadway_temporal_volume.at[i,'HCV_2016']=roadway_temporal_volume.at[i,'AADT_2016']*hcv_share
            roadway_temporal_volume.at[i,'HCV_2015']=roadway_temporal_volume.at[i,'AADT_2015']*hcv_share
            roadway_temporal_volume.at[i,'HCV_2014']=roadway_temporal_volume.at[i,'AADT_2014']*hcv_share

    # Keep needed columns
    roadway_temporal_volume=roadway_temporal_volume.iloc[:,[0,1,2,3,4,5,8,9,10,11,12,7]]
    roadway_temporal_volume.iloc[:,1:11] = roadway_temporal_volume.iloc[:,1:11].fillna(0)
    roadway_temporal_volume=roadway_temporal_volume.rename(columns={'segment_id_x': 'segment_id', 
                                                                    "functional_classification_x": "functional_classification"})

    for i in range(0,roadway_temporal_volume.shape[0]):
        if roadway_temporal_volume.at[i,'HCV_2014'] <0:
            roadway_temporal_volume.at[i,'HCV_2014']=roadway_temporal_volume.at[i,'AADT_2014']*hcv_share

    roadway_temporal_volume.iloc[:,0:11]=roadway_temporal_volume.iloc[:,0:11].astype('int')

    print('Heavy Vehicle Count (HCV) data merged into AADT data')

    return roadway_temporal_volume

def merge_aadt_hcv_pc(roadway_temporal_volume, crs_temporal_2014_2018):
    """
    This function merges Pacement Condition (PC) data into AADT_HCV data
    """
    roadway_temporal =pd.merge(roadway_temporal_volume, crs_temporal_2014_2018, on=['segment_id'],how='left')
    #roadway_temporal=roadway_temporal.drop(columns=['segment_id_y'])
    #roadway_temporal=roadway_temporal.rename(columns={'segment_id_x': 'segment_id'})
    print('Pacement Condition (PC) data merged into AADT_HCV data')

    return roadway_temporal

def merge_aadt_hcv_pc_stspop(roadway_temporal,streets_pop):
    """
    This function merges Street Pop Density data into AADT_HCV_PC data
    """
    roadway_temporal_pop =pd.merge(roadway_temporal, streets_pop, on=['segment_id'], how='left')
    cols = ['segment_id','AADT_2014','AADT_2015','AADT_2016','AADT_2017','AADT_2018',
        'HCV_2014','HCV_2015','HCV_2016','HCV_2017','HCV_2018',
        'Pop_den_2014','Pop_den_2015','Pop_den_2016','Pop_den_2017','Pop_den_2018',
        'PC_2014','PC_2015','PC_2016','PC_2017','PC_2018','functional_classification','pavement_condition']
    roadway_temporal_pop=roadway_temporal_pop[cols]
    roadway_temporal_pop.dropna(subset = ["Pop_den_2018"], inplace=True)
    roadway_temporal_final=roadway_temporal_pop.copy()
    roadway_temporal_final.iloc[:,11:16]= roadway_temporal_final.iloc[:,11:16].astype('int')
    print('Street Pop Density data merged into AADT_HCV_PC data')

    return roadway_temporal_final

""" Section 4.2.5 Population density """

def rpl_dup_seg_rec(streets_pop,bg_pop,SegmentID):

    """
    This function
    (1) Finds segments that have duplicate Pop_den_20XX records in streets_pop
    Some road segments belongs two blocks, see Example_1, (segment_id)id1=1, id2=8248
    Some other road segments are on the boundary of two blocks, see Example_2, id1=8208, id2=8209
    Thus, these road segments have more than one row of Pop_den_20XX records, see Example_2,
    (2) Use the Pop_den_20XX of the block that has highest (length) % of the segment as the pop of this segments
    e.g. If Block_A has 99% of the segment, Block_B has 1% of the segment, use the Pop_den_20XX of Block_A
    """

    # Find all segments that have duplicate records
    # Some of them are on the block boundaries, some of them across two blocks
    dpseg_segid = set(streets_pop[streets_pop[['segment_id']].duplicated()].segment_id)
    dpseg = streets_pop.loc[streets_pop['segment_id'].isin(dpseg_segid)].sort_values(by=['segment_id'])
    dpseg_geoid = set(dpseg['GEOID'])
    dpseg = gpd.GeoDataFrame(dpseg, crs="EPSG:3435")
    dpseg = dpseg.reset_index(drop=True)

    # Create a gdf of GEOID according to segments that have duplicate records
    dpgpd = pd.DataFrame()
    for i in dpseg['GEOID']:
        dpgpd = dpgpd.append(bg_pop.loc[bg_pop['GEOID']==str(i)])
    dpgpd = dpgpd.rename(columns={'GEOID':'GEOID_polygon'})
    dpgpd = dpgpd[['GEOID_polygon','geometry']]
    dpgdf = gpd.GeoDataFrame(dpgpd, crs="EPSG:3435")
    dpgdf = dpgdf.reset_index(drop=True)

    # Calculate the length in each block
    dp_len = []
    for i in range(dpgdf.shape[0]):
        dp_len.append(gpd.clip(dpseg[['segment_id','geometry']].iloc[[i]],dpgdf.iloc[[i]]).length)
    for i in range(len(dp_len)):
        dp_len[i] = list(dp_len[i].values)[0]
    dpseg['clip_len'] = dp_len
    dpseg['clip_len_pct'] = dp_len/dpseg.length

    # Find the biggest (length) proportion
    max_len_blk = pd.DataFrame()
    for id in np.unique(dpseg['segment_id']):
        subdf = dpseg.loc[dpseg['segment_id']==id]
        max_len_blk = max_len_blk.append(subdf.loc[subdf['clip_len_pct']==np.max(subdf.clip_len_pct)])
    max_len_blk = max_len_blk[['segment_id', 'geometry', 'index_right', 'GEOID', 
                               'Pop_den_2014','Pop_den_2015', 'Pop_den_2016', 'Pop_den_2017', 'Pop_den_2018']]

    # Replace original Pop_den_20xx of duplicate segments
    ## Drop all duplicate rows
    streets_pop.drop_duplicates(subset ="segment_id", keep = False, inplace = True)
    ## Insert new Pop_den_20xx values
    streets_pop = streets_pop.append(max_len_blk,ignore_index=True)

    if streets_pop.shape[0] == len(set(streets_pop['segment_id'].values)):
        print('All street segments have unique record now')
    else:
        print('Some street segments still have more than one row of records')

    return streets_pop

""" Section 4.3.1 Project each crash to its road segment """

def geo_segmentid(streets):
    """
    This function project PCD roadway segments to epsg:3435
    """
    SegmentID=streets[['segment_id','st_astext']]
    SegmentID.loc[:,'geom'] = SegmentID.loc[:,'st_astext'].apply(wkt.loads)
    geo_SegmentID = gpd.GeoDataFrame(
        SegmentID,
        geometry=SegmentID.geom,
        crs = {'init': 'epsg:3435'})

    return geo_SegmentID

def proj_crash(crashes_2014_2018):

    """
    This function reproject crashes to epsg:3435
    """

    geo_crashes_2014_2018 = gpd.GeoDataFrame(
    crashes_2014_2018,
    geometry=gpd.points_from_xy(
        crashes_2014_2018.TSCrashCoordinateX,
        crashes_2014_2018.TSCrashCoordinateY),
    crs={'init': 'epsg:3436'})

    geo_crashes_2014_2018_repro  = geo_crashes_2014_2018.to_crs({'init': 'epsg:3435'})

    return geo_crashes_2014_2018_repro

def assign_segment_to_crashes (crashes,segments):
    """
    This function achieves the same thing as Join Attributes by Nearest Tool in the Vector Tool toolbox in QGIS.
    Inspired by https://medium.com/@brendan_ward/how-to-leverage-geopandas-for-faster-snapping-of-points-to-lines-6113c94e59aa
    """
    offset = 50
    bbox = crashes.bounds + [-offset, -offset, offset, offset]
    segments.sindex
    hits = bbox.apply(lambda row: list(segments.sindex.intersection(row)), axis=1)

    tmp = pd.DataFrame({
        ## index of points table
        "Crash_Index": np.repeat(hits.index, hits.apply(len)),
        ## ordinal position of line - access via iloc later
        "segment_index": np.concatenate(hits.values)
    })

    # Join back to the lines on line_i; we use reset_index() to
    # give us the ordinal position of each line
    tmp = tmp.join(segments.reset_index(drop=True), on="segment_index",how='left')
    tmp['Crash_Index'].value_counts()

    # Join back to the original points to get their geometry
    # Rename the point geometry as "point"
    tmp_point = tmp.merge(crashes, on="Crash_Index",how='left')
    tmp_point=tmp_point[['Crash_Index','segment_id','geom','ICN','geometry_y',
                         'TSCrashCoordinateX','TSCrashCoordinateY','CrashYear']]
    tmp_point=tmp_point.rename(columns={"geometry_y": "point"})

    # Convert back to a GeoDataFrame, so we can do spatial ops
    tmp_point= gpd.GeoDataFrame(tmp_point, geometry="geom", crs=crashes.crs)
    tmp_point["snap_dist"] = tmp_point.geometry.distance(gpd.GeoSeries(tmp_point.point))
    tmp_point = tmp_point.loc[tmp_point.snap_dist <= 100]
    tmp_point= tmp_point.sort_values(by=["snap_dist"])

    ## Group by the index of the points and take the first, which is the closest line
    closest = tmp_point.groupby("Crash_Index").first()
    ## Construct a GeoDataFrame of the closest lines
    closest = gpd.GeoDataFrame(closest, geometry="geom")

    return closest

""" Section 4.3.2 Summarize crashes per segment per year """

def crash_by_seg_yr(crash_segment,geo_SegmentID):
    """
    This function summarizes the number of crashes per segment for each year
    """
    segment_crash_14 = crash_segment[crash_segment['CrashYear']==14].groupby("segment_id").count()
    segment_crash_15 = crash_segment[crash_segment['CrashYear']==15].groupby("segment_id").count()
    segment_crash_16 = crash_segment[crash_segment['CrashYear']==16].groupby("segment_id").count()
    segment_crash_17 = crash_segment[crash_segment['CrashYear']==17].groupby("segment_id").count()
    segment_crash_18 = crash_segment[crash_segment['CrashYear']==18].groupby("segment_id").count()
    segment_crash_14=segment_crash_14[['ICN']]
    segment_crash_15=segment_crash_15[['ICN']]
    segment_crash_16=segment_crash_16[['ICN']]
    segment_crash_17=segment_crash_17[['ICN']]
    segment_crash_18=segment_crash_18[['ICN']]
    target_14=geo_SegmentID[['segment_id','geometry']]
    target_14.loc[:,'Year']=2014
    target_15=geo_SegmentID[['segment_id','geometry']]
    target_15.loc[:,'Year']=2015
    target_16=geo_SegmentID[['segment_id','geometry']]
    target_16.loc[:,'Year']=2016
    target_17=geo_SegmentID[['segment_id','geometry']]
    target_17.loc[:,'Year']=2017
    target_18=geo_SegmentID[['segment_id','geometry']]
    target_18.loc[:,'Year']=2018
    target_14=target_14.merge(segment_crash_14,on='segment_id',how='left')
    target_15=target_15.merge(segment_crash_15,on='segment_id',how='left')
    target_16=target_16.merge(segment_crash_16,on='segment_id',how='left')
    target_17=target_17.merge(segment_crash_17,on='segment_id',how='left')
    target_18=target_18.merge(segment_crash_18,on='segment_id',how='left')

    return target_14,target_15,target_16,target_17,target_18

def merge_all_target(target_14, target_15,target_16,target_17,target_18):
    """
    This function merge all crashed together in time (year) order
    """
    target=pd.concat([target_14, target_15,target_16,target_17,target_18])
    target=pd.DataFrame(target)
    target['ICN'].fillna(0,inplace = True)
    return target

""" Section 4.4 Combine all features """

def cmb_static_full_by_yr(roadway_static_full):
    """
    This function reshapes static feature table to a long table
    """
    temp_2014=roadway_static_full.copy()
    temp_2014.loc[:,'Year']=2014
    temp_2015=roadway_static_full.copy()
    temp_2015.loc[:,'Year']=2015
    temp_2016=roadway_static_full.copy()
    temp_2016.loc[:,'Year']=2016
    temp_2017=roadway_static_full.copy()
    temp_2017.loc[:,'Year']=2017
    temp_2018=roadway_static_full.copy()
    temp_2018.loc[:,'Year']=2018
    df_static=pd.concat([temp_2014,temp_2015,temp_2016,temp_2017,temp_2018],ignore_index=True)

    return df_static

def cmb_tmp_fnl_by_yr(roadway_temporal_final):
    """
    This function reshapes temporal feature table to a long table
    """
    roadway_temporal_final_2014=roadway_temporal_final[['segment_id',
                                                        'AADT_2014','HCV_2014','Pop_den_2014','PC_2014']].copy()
    roadway_temporal_final_2014['Year']=2014
    roadway_temporal_final_2014=roadway_temporal_final_2014.rename(columns={'AADT_2014': 'AADT',
                                                                           'HCV_2014':'HCV',
                                                                           'Pop_den_2014':'Pop_den',
                                                                            'PC_2014':'PC'})
    roadway_temporal_final_2015=roadway_temporal_final[['segment_id',
                                                        'AADT_2015','HCV_2015','Pop_den_2015','PC_2015']].copy()
    roadway_temporal_final_2015['Year']=2015
    roadway_temporal_final_2015=roadway_temporal_final_2015.rename(columns={'AADT_2015': 'AADT',
                                                                           'HCV_2015':'HCV',
                                                                           'Pop_den_2015':'Pop_den',
                                                                            'PC_2015':'PC'})
    roadway_temporal_final_2016=roadway_temporal_final[['segment_id',
                                                        'AADT_2016','HCV_2016','Pop_den_2016','PC_2016']].copy()
    roadway_temporal_final_2016['Year']=2016
    roadway_temporal_final_2016=roadway_temporal_final_2016.rename(columns={'AADT_2016': 'AADT',
                                                                           'HCV_2016':'HCV',
                                                                           'Pop_den_2016':'Pop_den',
                                                                            'PC_2016':'PC'})
    roadway_temporal_final_2017=roadway_temporal_final[['segment_id',
                                                        'AADT_2017','HCV_2017','Pop_den_2017','PC_2017']].copy()
    roadway_temporal_final_2017['Year']=2017
    roadway_temporal_final_2017=roadway_temporal_final_2017.rename(columns={'AADT_2017': 'AADT',
                                                                           'HCV_2017':'HCV',
                                                                           'Pop_den_2017':'Pop_den',
                                                                            'PC_2017':'PC'})
    roadway_temporal_final_2018=roadway_temporal_final[['segment_id',
                                                        'AADT_2018','HCV_2018','Pop_den_2018','PC_2018']].copy()
    roadway_temporal_final_2018['Year']=2018
    roadway_temporal_final_2018=roadway_temporal_final_2018.rename(columns={'AADT_2018': 'AADT',
                                                                           'HCV_2018':'HCV',
                                                                           'Pop_den_2018':'Pop_den',
                                                                            'PC_2018':'PC'})

    df_temporal=pd.concat([roadway_temporal_final_2014,roadway_temporal_final_2015,
                           roadway_temporal_final_2016,roadway_temporal_final_2017,roadway_temporal_final_2018],
                          ignore_index=True)

    return df_temporal

def merge_stat_tmp_tgt(df_static,df_temporal,target):
    """
    This function merges all dataset into one df
    The df is ready for modeling
    """
    print('Static feature dataframe segment_id is used as the base for the final complete dataframe')
    df_full=df_static.merge(
    df_temporal,on=['segment_id','Year'],how='left').merge(
    target,on=['segment_id','Year'],how='left')
    df=df_full[['segment_id', 'overlap','st_length', 'curve_ratio', 'one_way',
            'bicycle_facility_width','bicycle_path_category', 'bicycle_buffer_width',
            'bicycle_buffer_type','bicycle_path_type', 'bicycle_approach_alignment',
            'bus_trips_total','crossing_aadt', 'crossing_functional_classification', 'crossing_speed',
            'functional_classification', 'intersection_control_type',
            'lane_configuration', 'max_lanes_crossed', 'parking_lane_width','marked_center_line',
            'posted_speed', 'railroad_crossing_type','right_turn_length', 'road_sign_type',
            'sidewalk_buffer_width','sidewalk_condition_score', 'sidewalk_width', 'sidewalk_buffer_type',
            'total_lanes', 'in_urbanized_area', 'volume_capacity','overall_landuse',
            'ACC_CNTL','I_SHD1_TYP', 'I_SHD1_WTH', 'I_SHD2_TYP', 'I_SHD2_WTH', 'LN_WTH', 'LNS',
            'MED_TYP', 'MED_WTH', 'SURF_TYP', 'SURF_WTH',
            'O_SHD1_TYP','O_SHD1_WTH', 'O_SHD2_TYP', 'O_SHD2_WTH', 'OP_1_2_WAY',
            'PRK_LT','PRK_RT', 'JUR_TYPE', 'MPO', 'MUNI_NAME', 'NHS', 'TRK_RT',
            'AADT', 'HCV', 'Pop_den', 'PC', 'geometry','Year', 'Crashes']]

    return df

def retrieve_pop(API): # Used in 2_Data_Source Section 2.3

    print('This function retrieves total popualation by block group information from U.S. Census Bureau ACS from year 2014 to 2018'+'\n')

    response_2014 = requests.get('https://api.census.gov/data/2014/acs/acs5?get=NAME,B00001_001E&for=block%20group:*&in=state:17+county:019&key='+API)
    response_2015 = requests.get('https://api.census.gov/data/2015/acs/acs5?get=NAME,B00001_001E&for=block%20group:*&in=state:17+county:019&key='+API)
    response_2016 = requests.get('https://api.census.gov/data/2016/acs/acs5?get=NAME,B00001_001E&for=block%20group:*&in=state:17+county:019&key='+API)
    response_2017 = requests.get('https://api.census.gov/data/2017/acs/acs5?get=NAME,B00001_001E&for=block%20group:*&in=state:17+county:019&key='+API)
    response_2018 = requests.get('https://api.census.gov/data/2018/acs/acs5?get=NAME,B00001_001E&for=block%20group:*&in=state:17+county:019&key='+API)

    # Load the response into a JSON, ignoring the first element which is just field labels
    response_2014_js = json.loads(response_2014.text)[1:]
    response_2015_js = json.loads(response_2015.text)[1:]
    response_2016_js = json.loads(response_2016.text)[1:]
    response_2017_js = json.loads(response_2017.text)[1:]
    response_2018_js = json.loads(response_2018.text)[1:]

    # Store the response in a dataframe
    pop_2014 = pd.DataFrame(columns=["NAME","Pop_2014","state","county","tract","block group"],data=response_2014_js)
    pop_2015 = pd.DataFrame(columns=["NAME","Pop_2015","state","county","tract","block group"],data=response_2015_js)
    pop_2016 = pd.DataFrame(columns=["NAME","Pop_2016","state","county","tract","block group"],data=response_2016_js)
    pop_2017 = pd.DataFrame(columns=["NAME","Pop_2017","state","county","tract","block group"],data=response_2017_js)
    pop_2018 = pd.DataFrame(columns=["NAME","Pop_2018","state","county","tract","block group"],data=response_2018_js)

    pop_2014 = pop_2014.astype({'Pop_2014': 'int32'})
    pop_2015 = pop_2015.astype({'Pop_2015': 'int32'})
    pop_2016 = pop_2016.astype({'Pop_2016': 'int32'})
    pop_2017['Pop_2017'] = pd.to_numeric(pop_2017['Pop_2017'], errors='coerce')
    pop_2018['Pop_2018'] = pd.to_numeric(pop_2018['Pop_2018'], errors='coerce')

    pop = pd.merge(pop_2014, pop_2015[['NAME','Pop_2015']],
             how='left',
             on='NAME')
    pop = pd.merge(pop, pop_2016[['NAME','Pop_2016']],
             how='left',
             on='NAME')
    pop = pd.merge(pop, pop_2017[['NAME','Pop_2017']],
             how='left',
             on='NAME')
    pop = pd.merge(pop, pop_2018[['NAME','Pop_2018']],
             how='left',
             on='NAME')
    pop['GEOID'] = pop['state']+pop['county']+pop['tract']+pop['block group']

    return pop

def bgpop_density(pop): # Used in 2_Data_Source Section 2.3

    # Calculate pop density
    blockgroup_geopackage=base_gis_dir+"Data/Blockgroups.gpkg"
    blockgroups=gpd.read_file(blockgroup_geopackage, driver="GPKG", layer='Blockgroups')
    blockgroups = blockgroups.set_geometry('geometry')
    blockgroups.crs = "EPSG:3435"
    blockgroups["area_sqm"] = blockgroups['geometry'].area*3.587e-8
    bg_pop=pd.merge(blockgroups[['GEOID','geometry','area_sqm']],
                    pop[['GEOID','Pop_2014','Pop_2015','Pop_2016','Pop_2017','Pop_2018']],
                    how='left',
                    on='GEOID')
    bg_pop['Pop_den_2014']=bg_pop['Pop_2014']/bg_pop['area_sqm']
    bg_pop['Pop_den_2015']=bg_pop['Pop_2015']/bg_pop['area_sqm']
    bg_pop['Pop_den_2016']=bg_pop['Pop_2016']/bg_pop['area_sqm']
    bg_pop['Pop_den_2017']=bg_pop['Pop_2017']/bg_pop['area_sqm']
    bg_pop['Pop_den_2018']=bg_pop['Pop_2018']/bg_pop['area_sqm']

    # Save file
    with open('../datbase_file_dir+a/interim/demographic/bg_pop.pkl', 'wb') as f:
        pickle.dump(bg_pop, f)
    print('pop density by blockgroup saved in bg_pop.pkl')

    return bg_pop