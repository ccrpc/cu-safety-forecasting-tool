"""
This file includes the functions used for 3_Exploratory_Crash_Data_Analysis
"""

# Import libraries

# Python data analysis library
import pandas as pd
# Standard Python built-in libraries
from functools import partial
# Numerical libraries
import numpy as np
# Visualization library
import seaborn
# Datashader Big Data Visualization
import datashader as ds
from datashader import transfer_functions as tf
# Convert Longitude-Latitude coordinates into Web Mercator X-Y Values
from datashader.utils import lnglat_to_meters as webm
# Export a datashader plot to an image file (e.g. jpg/png)
from datashader.utils import export_image
# Color Maps
from datashader.colors import colormap_select, Greys9, Hot, viridis, inferno
from matplotlib.cm import hot
import matplotlib.image as mpimg
from colorcet import fire

import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.rc('axes', labelsize=14)
mpl.rc('xtick', labelsize=12)
mpl.rc('ytick', labelsize=12)

def plot_injury_level(plot_data): # used in Section 3.1.2

    print('Plotting crash injury level statistics for each year')
    fig, ax = plt.subplots(1, 3, figsize = (12, 3));
    seaborn.barplot(x="CrashYear", y="TotalFatals",  data=plot_data, ax=ax[0]);
    seaborn.barplot(x="CrashYear", y="Ainjuries",  data=plot_data, ax=ax[1]);
    seaborn.barplot(x="CrashYear", y="Binjuries",  data=plot_data, ax=ax[2]);
    [ax[i].set_xlabel('Crash Year') for i in range(3)]
    plt.tight_layout()
    plt.savefig("../reports/figures/exploratory_crash_data_analysis/injury_level.png",dpi=300)
    print("Injury level figures saved")
    plt.show()

def plot_rdwy_env_con(plot_data): # used in Section 3.1.3

    print('Plotting statistics of roadway features, environmental conditions and crash types')
    fig, ax = plt.subplots(2, 3, figsize = (20, 8));
    seaborn.countplot(y='RoadSurfaceCond',data=plot_data, ax=ax[0,0])
    seaborn.countplot(y="LightCondition",data=plot_data,  ax=ax[0,1])
    seaborn.countplot(y='WeatherCondition',data=plot_data, ax=ax[0,2])
    seaborn.countplot(y='RoadwayFunctionalClass',data=plot_data, ax=ax[1,0])
    seaborn.countplot(y='RoadAlignment',data=plot_data, ax=ax[1,1])
    seaborn.countplot(y='TypeofFirstCrash',data=plot_data,  ax=ax[1,2])
    plt.tight_layout()
    plt.savefig("../reports/figures/exploratory_crash_data_analysis/roadwway_env_conditions.png",dpi=300)
    print("Figures of roadway features, environmental conditions and crash types statistics saved")
    plt.show()

def plot_time_dist(plot_data): # used in Section 3.1.4

    print('Plotting Monthly, weekly, daily, hourly distribution bar chart')
    fig, axes = plt.subplots(nrows=2, ncols=2,figsize=(10, 6))
    fig.subplots_adjust(hspace=.6)
    # monthly breakdown
    plot_data['CrashMonth'].value_counts().sort_index().plot(ax=axes[0,0],kind='bar',title='Month-wise accident')
    # daily breakdown
    plot_data['CrashDay'].value_counts().sort_index().plot(ax=axes[0,1],title='Day-wise accident')
    # weekly breakdown
    plot_data['DayofWeek'].value_counts().sort_index().plot(ax=axes[1,0],kind='bar',title='Week_Day-wise accident')
    # hourly breakdown
    plot_data['CrashHour'].value_counts().sort_index().plot(ax=axes[1,1],kind='bar',title='Hour-wise accident')
    plt.savefig("../reports/figures/exploratory_crash_data_analysis/time_distribution.png",dpi=300)
    print('Time distribution figures saved')
    plt.show()

def plot_crash_mapping_XYcoord(plot_data): # used in Section 3.2

    print('Plotting crash mapping with X,Y coordinates')
    plot_data.plot(kind="scatter", x="TSCrashCoordinateX", y="TSCrashCoordinateY", alpha=0.4,
                       s=plot_data["TotalInjured"], label="Total injuries", figsize=(10,7),
                       #c="point_color_category",
                       cmap=plt.get_cmap("jet"), colorbar=True,sharex=False)
    plt.legend()
    plt.savefig("../reports/figures/exploratory_crash_data_analysis/traffic_crash_scatterplot_XY.png",dpi=300)
    print('Crash mapping with X,Y coordinates figure saved')
    plt.show()

def plot_crash_mapping_lnglat(plot_data): # used in Section 3.2

    print('Plotting crash mapping with longitude and latitude')
    plot_data.plot(kind="scatter", x="TSCrashLong", y="TSCrashLat", alpha=0.4,
                       s=plot_data["TotalInjured"], label="Total injuries", figsize=(10,7),
                       #c="point_color_category",
                       cmap=plt.get_cmap("jet"), colorbar=True,sharex=False)
    plt.legend()
    plt.savefig("../reports/figures/exploratory_crash_data_analysis/traffic_crash_scatterplot_LatLon.png",dpi=300)
    print('Crash mapping with longitude and latitude figure saved')
    plt.show()

def Champaign_County_bounding(plot_data): # used in Section 3.2

    # Bounding box for the Champaign County and the plot
    ## Compute Mercator x and y values from Longitude and Latitude
    x, y = [c for c in list(webm(plot_data.loc[:, 'TSCrashLong'], plot_data.loc[:, 'TSCrashLat']))]
    ## Add as new columns called `webm_x` and `webm_y `in our dataframe df
    plot_data = plot_data.assign(webm_x=x).copy()
    plot_data = plot_data.assign(webm_y=y).copy()
    # In lng/lats, manually obatained from Open Street Map
    # https://www.openstreetmap.org/export
    bboxes = {"ChampaignCounty":((-88.8418, -87.5001), (39.8727, 40.4123))}
    ## Default plot width
    plot_width= 1500
    ## Compute X and Y distances
    x_range, y_range = webm(*bboxes["ChampaignCounty"])
    x_range_dist = x_range[1] - x_range[0]
    y_range_dist = y_range[1] - y_range[0]
    plot_height = int(y_range_dist * plot_width /x_range_dist)

    print("Champaign County Bounding Box")
    print("  - in lng/lat ranges: {}".format(bboxes["ChampaignCounty"]))
    print("  - in Web Mercator X-Y ranges:")
    print("    - x_range: {}".format(x_range))
    print("    - y_range: {}".format(y_range))
    print("  - in Plot width: {}, Plot height: {}".format(plot_width, plot_height))

    return (plot_data,plot_width, plot_height, x_range, y_range)

def display_image_in_actual_size(im_path): # used in Section 3.2

    # learned from https://stackoverflow.com/questions/28816046/displaying-different-images-with-actual-size-in-matplotlib-subplot
    dpi = 80
    im_data = plt.imread(im_path)
    height, width, depth = im_data.shape
    figsize = width / float(dpi), height / float(dpi)
    ## Create a figure of the right size with one axes that takes up the full figure
    fig = plt.figure(figsize=figsize)
    ax = fig.add_axes([0, 0, 1, 1])
    ## Hide spines, ticks, etc.
    ax.axis('off')
    ## Display the image.
    ax.imshow(im_data, cmap='gray')
    plt.show()

def plot_crash_mapping_WebMercator_XYValues(plot_data,background = "black"): # used in Section 3.2

    print('Plotting crash mapping with WebMercator X,Y values')
    ## utility function: to enable us to export a plot to an image file, with default parameters (e.g. background)
    export = partial(export_image, background=background, export_path="export")
    ## utility function: color map
    cm = partial(colormap_select, reverse=(background!="black"))

    # Plot Crash density
    updated_plot_data,plot_width, plot_height, x_range, y_range = Champaign_County_bounding(plot_data)
    cvs = ds.Canvas(plot_width, plot_height, x_range, y_range)
    agg = cvs.points(updated_plot_data, 'webm_x', 'webm_y')
    export(tf.shade(agg, cmap=cm(Hot, 0.25), how='eq_hist'),"Hot_eq_hist_0.25")
    print('Crash mapping with WebMercator XY values figure saved')

    # Load and display the image
    display_image_in_actual_size('./export/Hot_eq_hist_0.25.png')
