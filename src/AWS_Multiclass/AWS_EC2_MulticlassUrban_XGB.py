# Import libraries
import pandas as pd
import numpy as np
import geopandas as gpd
import pickle
from numpy import argmax
import importlib
import collections
import time
import getpass
import datetime as datetime
from sklearn import metrics
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler, OneHotEncoder, FunctionTransformer,  LabelEncoder
from sklearn.linear_model import LinearRegression,  LogisticRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.pipeline import Pipeline
from sklearn.dummy import DummyClassifier
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score, recall_score, precision_recall_curve, brier_score_loss, auc
from sklearn.metrics import f1_score, fbeta_score, confusion_matrix, make_scorer, classification_report
from sklearn.utils.class_weight import compute_class_weight
from sklearn.model_selection import  StratifiedKFold, KFold, cross_val_score, StratifiedShuffleSplit
from sklearn.model_selection import  RepeatedStratifiedKFold, GridSearchCV, cross_validate
from xgboost import XGBClassifier

print('HELLO')

def func_OHtransform_var (df):
    df_variables=df.columns.tolist()
    num_attributes=df.describe().T.index.tolist()
    cat_attributes=[x for x in df_variables if x not in num_attributes]
    variables_pipeline = ColumnTransformer([
        ("num", StandardScaler(), num_attributes),
        ("cat", OneHotEncoder(), cat_attributes),
    ])
    df_transformed = variables_pipeline.fit_transform(df)
    return df_transformed

class_weight = {'No Crash':0.2,'1 Crash':0.3,'>=2 Crashes':0.5}

def tn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 0]
def fp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 1]
def fn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 0]
def tp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 1]
def precision(y_true, y_pred): return precision_score(y_true, y_pred)
def recall(y_true, y_pred): return recall_score(y_true, y_pred)
def fbeta_2(y_true,y_pred,beta=2.0): return fbeta_score(y_true, y_pred, average='weighted', beta=2.0)

def f2_by_wt(y_true, y_pred,class_weight):
    # Customize f2 by class
    target_names = list(class_weight.keys())
    report = classification_report(y_true, y_pred, digits=3, output_dict=True,target_names=target_names)
    df = pd.DataFrame(report).transpose()
    No_Crash_f2 = (5*df.loc['No Crash','recall']*df.loc['No Crash','precision'])/(4*df.loc['No Crash','precision']+df.loc['No Crash','recall'])
    OneCrash_f2 = (5*df.loc['1 Crash','recall']*df.loc['1 Crash','precision'])/(4*df.loc['1 Crash','precision']+df.loc['1 Crash','recall'])
    TwoPlusCrash_f2= (5*df.loc['>=2 Crashes','recall']*df.loc['>=2 Crashes','precision'])/(4*df.loc['>=2 Crashes','precision']+df.loc['>=2 Crashes','recall'])
    f2bywt = No_Crash_f2*class_weight['No Crash'] + OneCrash_f2*class_weight['1 Crash'] + TwoPlusCrash_f2*class_weight['>=2 Crashes']
    return f2bywt

def f1_by_wt(y_true, y_pred,class_weight):
    # Customize f1 by class
    target_names = list(class_weight.keys())
    report = classification_report(y_true, y_pred, digits=3, output_dict=True,target_names=target_names)
    df = pd.DataFrame(report).transpose()
    No_Crash_f1 = (2*df.loc['No Crash','recall']*df.loc['No Crash','precision'])/(df.loc['No Crash','precision']+df.loc['No Crash','recall'])
    OneCrash_f1 = (2*df.loc['1 Crash','recall']*df.loc['1 Crash','precision'])/(df.loc['1 Crash','precision']+df.loc['1 Crash','recall'])
    TwoPlusCrash_f1= (2*df.loc['>=2 Crashes','recall']*df.loc['>=2 Crashes','precision'])/(df.loc['>=2 Crashes','precision']+df.loc['>=2 Crashes','recall'])
    f1bywt = No_Crash_f1*class_weight['No Crash']+OneCrash_f1*class_weight['1 Crash']+TwoPlusCrash_f1*class_weight['>=2 Crashes']
    return f1bywt

scoring = {'tp': make_scorer(tp), 'tn': make_scorer(tn), 'fp': make_scorer(fp), 'fn': make_scorer(fn),
           'precision':make_scorer(precision),'recall':make_scorer(recall),
           'fbeta_2':make_scorer(fbeta_2),'f2_by_wt':make_scorer(f2_by_wt),'f1_by_wt':make_scorer(f1_by_wt)}

def multiclass_perf_fullreport(y_test, y_pred,class_weight):
    report = classification_report(y_test, y_pred, output_dict=True)
    df = pd.DataFrame(report).transpose()
    df = df.rename(index={'0.0':'No Crash','1.0':'1 Crash','2.0':'>=2 Crashes','accuracy': 'micro avg'})
    df.loc['weighted avg','precision'] = sum([df.loc[i,'precision']*class_weight[i] for i in class_weight.keys()])
    df.loc['weighted avg','recall'] = sum([df.loc[i,'recall']*class_weight[i] for i in class_weight.keys()])
    No_Crash_f2 = (5*df.loc['No Crash','recall']*df.loc['No Crash','precision'])/(4*df.loc['No Crash','precision']+df.loc['No Crash','recall'])
    OneCrash_f2 = (5*df.loc['1 Crash','recall']*df.loc['1 Crash','precision'])/(4*df.loc['1 Crash','precision']+df.loc['1 Crash','recall'])
    TwoPlusCrash_f2= (5*df.loc['>=2 Crashes','recall']*df.loc['>=2 Crashes','precision'])/(4*df.loc['>=2 Crashes','precision']+df.loc['>=2 Crashes','recall'])
    df.loc['No Crash','f2-score'] = No_Crash_f2
    df.loc['1 Crash','f2-score'] = OneCrash_f2
    df.loc['>=2 Crashes','f2-score'] = TwoPlusCrash_f2
    df.loc['macro avg','f2-score'] = fbeta_score(y_test, y_pred, average='macro', beta=2)
    df.loc['micro avg','f2-score'] = fbeta_score(y_test, y_pred, average='micro', beta=2)
    df.loc['weighted avg','f2-score'] = f2_by_wt(y_test, y_pred,class_weight)
    df.loc['weighted avg','f1-score'] = f1_by_wt(y_test, y_pred,class_weight)
    df = df.round(3)
    return df

with open('./df_urban_train.pkl', 'rb') as f:
    df_urban_train = pickle.load(f)
with open('./df_urban_train_label.pkl', 'rb') as f:
    df_urban_train_label = pickle.load(f)
with open('./df_urban_test.pkl', 'rb') as f:
    df_urban_test = pickle.load(f)
with open('./df_urban_test_label.pkl', 'rb') as f:
    df_urban_test_label = pickle.load(f)


# Remove features that should not be fed into the model
remove_list=['segment_id','overlap','geometry','Year']
targetlist = [i for i in list(df_urban_train.columns) if i not in remove_list]
df_urban_train_sub = df_urban_train[targetlist]
df_urban_test_sub = df_urban_test[targetlist]

# Convert full training & testing data to binary class categories
df_urban_train_cat_label=df_urban_train_label.mask(df_urban_train_label >= 3, 2)
df_urban_test_cat_label=df_urban_test_label.mask(df_urban_test_label >= 3, 2)

X_train=df_urban_train_sub
y_train=df_urban_train_cat_label
X_test=df_urban_test_sub
y_test=df_urban_test_cat_label

X_train_OHtransformed=func_OHtransform_var(X_train)
X_test_OHtransformed=func_OHtransform_var(X_test)

sampling=None #sampling=SMOTE()
search_n_estimators = []
search_max_depth = []
search_eta = []
search_gamma = []
search_subsample = []
search_min_child_weight = []
search_scale_pos_weight = []
search_wgt_precision = []
search_wgt_recall = []
search_wgt_f2score = []
search_NoCrash_precision =  []
search_NoCrash_recall =  []
search_NoCrash_f2score =  []
search_OneCrash_precision = []
search_OneCrash_recall = []
search_OneCrash_f2score = []
search_TwoPlusCrash_precision = []
search_TwoPlusCrash_recall = []
search_TwoPlusCrash_f2score = []
search_score = []
search_time = []

for n_estimators in np.arange(50,250,50):
    for max_depth in [3,4,5,6,7,8]:
        for eta in [0.1]:
            for gamma in [0,1,3,5,7]:
                for subsample in [0.8,1]:
                    for min_child_weight in [3,5,7,9,11]:

                        print(n_estimators,max_depth,eta,gamma,subsample,min_child_weight)

                        search_n_estimators.append(n_estimators)
                        search_max_depth.append(max_depth)
                        search_eta.append(eta)
                        search_gamma.append(gamma)
                        search_subsample.append(subsample)
                        search_min_child_weight.append(min_child_weight)

                        t0 = time.time()
                        XGBmodel = XGBClassifier(n_jobs=-1,random_state=42,n_estimators=n_estimators,max_depth=max_depth,
                                                 eta=eta,gamma=gamma,subsample=subsample,min_child_weight=min_child_weight)
                        XGBmodel_fitted=XGBmodel.fit(X_train_OHtransformed, y_train)
                        t1 = time.time()
                        y_pred = XGBmodel_fitted.predict(X_test_OHtransformed)

                        target_names = ['No Crash', '1 Crash', '>=2 Crashes']
                        report = multiclass_perf_fullreport(y_test, y_pred,class_weight)

                        search_wgt_precision.append(report.loc['weighted avg','precision'])
                        search_wgt_recall.append(report.loc['weighted avg','recall'])
                        search_wgt_f2score.append(report.loc['weighted avg','f2-score'])
                        search_NoCrash_precision.append(report.loc['No Crash','precision'])
                        search_NoCrash_recall.append(report.loc['No Crash','recall'])
                        search_NoCrash_f2score.append(report.loc['No Crash','f2-score'])
                        search_OneCrash_precision.append(report.loc['1 Crash','precision'])
                        search_OneCrash_recall.append(report.loc['1 Crash','recall'])
                        search_OneCrash_f2score.append(report.loc['1 Crash','f2-score'])
                        search_TwoPlusCrash_precision.append(report.loc['>=2 Crashes','precision'])
                        search_TwoPlusCrash_recall.append(report.loc['>=2 Crashes','recall'])
                        search_TwoPlusCrash_f2score.append(report.loc['>=2 Crashes','f2-score'])

                        search_score.append(np.sum(np.multiply(metrics.confusion_matrix(y_test, y_pred),np.array([[1,-1,-2],[-3,2,-4],[-6,-5,3]])))/np.sum(np.multiply(y_test.value_counts().values,[1,2,3])))

                        search_time.append(t1-t0)


searchresults = pd.DataFrame()
searchresults['n_estimators']  = search_n_estimators
searchresults['max_depth'] = search_max_depth
searchresults['eta'] = search_eta
searchresults['gamma'] = gamma
searchresults['subsample'] = search_subsample
searchresults['min_child_weight'] = search_min_child_weight

searchresults['wgt_precision'] = search_wgt_precision
searchresults['wgt_recall'] = search_wgt_recall
searchresults['wgt_f2score'] = search_wgt_f2score
searchresults['NoCrash_precision'] = search_NoCrash_precision
searchresults['NoCrash_recall'] = search_NoCrash_recall
searchresults['NoCrash_f2score'] = search_NoCrash_f2score
searchresults['OneCrash_precision'] = search_OneCrash_precision
searchresults['OneCrash_recall'] = search_OneCrash_recall
searchresults['OneCrash_f2score'] = search_OneCrash_f2score
searchresults['TwoPlusCrash_precision'] = search_TwoPlusCrash_precision
searchresults['TwoPlusCrash_recall'] = search_TwoPlusCrash_recall
searchresults['TwoPlusCrash_f2score'] = search_TwoPlusCrash_f2score
searchresults['wgt_cfm_score']= search_score
searchresults['time'] = search_time

f = open("Multiclass_Urban_XGB_search_results"+datetime.datetime.now().strftime("%Y%m%d-%H%M%S")+".pkl","wb")
pickle.dump(searchresults,f)
f.close()
