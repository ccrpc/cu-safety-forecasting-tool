import numpy as np
from numpy import argmax
from sklearn.model_selection import cross_val_score, StratifiedKFold
from matplotlib import pyplot as plt
from shapely import wkt
import warnings
import pandas as pd
import geopandas as gpd
import datetime
import time
from csv import DictWriter
import categorical_embedder as ce
from sklearn.decomposition import PCA
from sklearn import svm
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler, OneHotEncoder, FunctionTransformer,  LabelEncoder
from sklearn.model_selection import StratifiedShuffleSplit
from imblearn.over_sampling import SMOTE
from imblearn.combine import SMOTEENN
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score, recall_score, precision_recall_curve, brier_score_loss, auc
from sklearn.metrics import f1_score, fbeta_score, confusion_matrix, make_scorer, classification_report
from sklearn.metrics import brier_score_loss
from sklearn.metrics import mean_squared_error
from sklearn.dummy import DummyClassifier
#from bayes_opt import BayesianOptimization
from xgboost import XGBClassifier
from varname import nameof
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# =============================================
# Customize score for imbalanced classification
# =============================================

class_weight = {'No Crash':0.2,'1 Crash':0.3,'>=2 Crashes':0.5}

def tn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 0]
def fp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 1]
def fn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 0]
def tp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 1]
def precision(y_true, y_pred): return precision_score(y_true, y_pred)
def recall(y_true, y_pred): return recall_score(y_true, y_pred)
def fbeta_2(y_true,y_pred,beta=2.0): return fbeta_score(y_true, y_pred, average='weighted', beta=2.0)

def f2_by_wt(y_true, y_pred,class_weight):
    # Customize f2 by class
    target_names = list(class_weight.keys())
    report = classification_report(y_true, y_pred, digits=3, output_dict=True,target_names=target_names)
    df = pd.DataFrame(report).transpose()
    No_Crash_f2 = (5*df.loc['No Crash','recall']*df.loc['No Crash','precision'])/(4*df.loc['No Crash','precision']+df.loc['No Crash','recall'])
    OneCrash_f2 = (5*df.loc['1 Crash','recall']*df.loc['1 Crash','precision'])/(4*df.loc['1 Crash','precision']+df.loc['1 Crash','recall'])
    TwoPlusCrash_f2= (5*df.loc['>=2 Crashes','recall']*df.loc['>=2 Crashes','precision'])/(4*df.loc['>=2 Crashes','precision']+df.loc['>=2 Crashes','recall'])
    f2bywt = No_Crash_f2*class_weight['No Crash'] + OneCrash_f2*class_weight['1 Crash'] + TwoPlusCrash_f2*class_weight['>=2 Crashes']
    return f2bywt

def f1_by_wt(y_true, y_pred,class_weight):
    # Customize f1 by class
    target_names = list(class_weight.keys())
    report = classification_report(y_true, y_pred, digits=3, output_dict=True,target_names=target_names)
    df = pd.DataFrame(report).transpose()
    No_Crash_f1 = (2*df.loc['No Crash','recall']*df.loc['No Crash','precision'])/(df.loc['No Crash','precision']+df.loc['No Crash','recall'])
    OneCrash_f1 = (2*df.loc['1 Crash','recall']*df.loc['1 Crash','precision'])/(df.loc['1 Crash','precision']+df.loc['1 Crash','recall'])
    TwoPlusCrash_f1= (2*df.loc['>=2 Crashes','recall']*df.loc['>=2 Crashes','precision'])/(df.loc['>=2 Crashes','precision']+df.loc['>=2 Crashes','recall'])
    f1bywt = No_Crash_f1*class_weight['No Crash']+OneCrash_f1*class_weight['1 Crash']+TwoPlusCrash_f1*class_weight['>=2 Crashes']
    return f1bywt

scoring = {'tp': make_scorer(tp), 'tn': make_scorer(tn), 'fp': make_scorer(fp), 'fn': make_scorer(fn),
           'precision':make_scorer(precision),'recall':make_scorer(recall),
           'fbeta_2':make_scorer(fbeta_2),'f2_by_wt':make_scorer(f2_by_wt),'f1_by_wt':make_scorer(f1_by_wt)}

def multiclass_perf_fullreport(y_test, y_pred,class_weight):
    report = classification_report(y_test, y_pred, output_dict=True)
    df = pd.DataFrame(report).transpose()
    df = df.rename(index={'0.0':'No Crash','1.0':'1 Crash','2.0':'>=2 Crashes','accuracy': 'micro avg'})
    df.loc['weighted avg','precision'] = sum([df.loc[i,'precision']*class_weight[i] for i in class_weight.keys()])
    df.loc['weighted avg','recall'] = sum([df.loc[i,'recall']*class_weight[i] for i in class_weight.keys()])
    No_Crash_f2 = (5*df.loc['No Crash','recall']*df.loc['No Crash','precision'])/(4*df.loc['No Crash','precision']+df.loc['No Crash','recall'])
    OneCrash_f2 = (5*df.loc['1 Crash','recall']*df.loc['1 Crash','precision'])/(4*df.loc['1 Crash','precision']+df.loc['1 Crash','recall'])
    TwoPlusCrash_f2= (5*df.loc['>=2 Crashes','recall']*df.loc['>=2 Crashes','precision'])/(4*df.loc['>=2 Crashes','precision']+df.loc['>=2 Crashes','recall'])
    df.loc['No Crash','f2-score'] = No_Crash_f2
    df.loc['1 Crash','f2-score'] = OneCrash_f2
    df.loc['>=2 Crashes','f2-score'] = TwoPlusCrash_f2
    df.loc['macro avg','f2-score'] = fbeta_score(y_test, y_pred, average='macro', beta=2)
    df.loc['micro avg','f2-score'] = fbeta_score(y_test, y_pred, average='micro', beta=2)
    df.loc['weighted avg','f2-score'] = f2_by_wt(y_test, y_pred,class_weight)
    df.loc['weighted avg','f1-score'] = f1_by_wt(y_test, y_pred,class_weight)
    df = df.drop(columns=['support'])
    df = df.round(3)
    return df
# =============================================
# Data processing
# =============================================

def find_line_centroid(df):
    "Add centorid_x and centroid_y to the dataset"
    # eg: df_rural_train = find_line_centroid(df_rural_train)
    geo_df = gpd.GeoDataFrame(
            df,
            geometry=df.geometry,
            crs = {'init': 'epsg:3435'})
    df['centroid_x'] = geo_df['geometry'].centroid.x
    df['centroid_y'] = geo_df['geometry'].centroid.y
    return df

# convert categorical variables with string values to numeric
def func_PC_numeric(X):
    ordered_PC=['Unknown','Failed','Poor','Fair','Good','Excellent']
    X.loc[:,'PC'] = pd.Categorical(X['PC'],ordered=True,categories=ordered_PC)
    X.loc[:,'PC'] = X['PC'].cat.codes.astype(str)
    return X

# Explanatory variable subsets -- Remove list of variables
def func_remove_var (df,remove_list):
    variables=np.array(df.columns.tolist())
    remove_index=[]
    variable_index=list(range(len(variables)))
    for i in range(len(remove_list)):
        remove_index.append(np.where(variables == remove_list[i])[0][0])
    df_sub=df.drop(df.columns[remove_index],axis=1,inplace=False)
    return df_sub

# Development Feature Preprocessing --categorical variable embedding transformation parameters
def func_NNembed_cat_transf_para (X_cat,y, epochs=10, batch_size=256):
    X_encoded,encoders = ce.get_label_encoded_data(X_cat)
    embedding_info = ce.get_embedding_info(X_cat)
    embeddings = ce.get_embeddings(X_encoded, pd.Series(y),
                               categorical_embedding_info=embedding_info,
                               is_classification=True,
                               epochs=epochs,
                               batch_size=batch_size)
    return embeddings,encoders, embedding_info

# Development Feature Preprocessing -- get the variable names of the transformed training data
def func_NNembed_transformed_var (embedding_info,df):
    df_variables=df.columns.tolist()
    num_attributes=df.describe().T.index.tolist()
    embed_size=pd.DataFrame(embedding_info)
    cat_attributes=[]
    for i in range(0,embed_size.shape[1]):
        s=embed_size.columns[i]
        for j in range(0,embed_size.iloc[1,i]):
            code=str(j)
            cat_attributes.append(s+'_embed_'+code)
    df_feature_names = num_attributes+cat_attributes
    feature_sel = range(len(df_feature_names))
    fnames = np.array(df_feature_names)[feature_sel]
    return fnames

# Development Feature Preprocessing --categorical variable embedding transformation
def func_NNembed_cat_trans (X_cat):
    X = ce.fit_transform(X_cat, embeddings=embeddings, encoders=encoders, drop_categorical_vars=True)
    return X

# Development Feature Preprocessing -- transform numerical and categorical (embedded) variables
def func_NNembed_transform_var (df):
    df_variables=df.columns.tolist()
    num_attributes=df.describe().T.index.tolist()
    cat_attributes=[x for x in df_variables if x not in num_attributes]
    variables_pipeline = ColumnTransformer([
        ("num", StandardScaler(), num_attributes),
        ("cat", FunctionTransformer(func_NNembed_cat_trans), cat_attributes),
    ])
    df_transformed = variables_pipeline.fit_transform(df)
    return df_transformed

# NN Embeded categories classification model pipeline
def func_NNembed_classfication_pipeline (func_NNembed_transform_var,model, sampling):
    return (Pipeline(
    steps=[
        ('variable_transformer',FunctionTransformer(func_NNembed_transform_var)),
        ('sampling',sampling),
        ('model', model)
    ], verbose=True))

def func_OHtransform_var (df):
    df_variables=df.columns.tolist()
    num_attributes=df.describe().T.index.tolist()
    cat_attributes=[x for x in df_variables if x not in num_attributes]
    variables_pipeline = ColumnTransformer([
        ("num", StandardScaler(), num_attributes),
        ("cat", OneHotEncoder(), cat_attributes),
    ])
    df_transformed = variables_pipeline.fit_transform(df)
    return df_transformed

def func_OHtransformed_var (df):
    df_variables=df.columns.tolist()
    num_attributes=df.describe().T.index.tolist()
    cat_attributes=[x for x in df_variables if x not in num_attributes]
    num_feature_names = num_attributes
    cat_feature_names = pd.get_dummies(df[cat_attributes],columns=cat_attributes).columns.tolist()
    df_feature_names = num_feature_names+cat_feature_names
    feature_sel = range(len(df_feature_names))
    fnames = np.array(df_feature_names)[feature_sel]
    return fnames

# Classification model pipeline
def func_OH_classfication_pipeline (func_OHtransform_var, model, sampling):
    return (Pipeline(
    steps=[
        ('variable_transformer', FunctionTransformer(func_OHtransform_var, validate=False)),
        ('sampling',sampling),
        ('model', model)
    ], verbose=True))

# =============================================
# SMOTE Sampling for imbalanced data
# =============================================

# Find the best sampling strategy by using default XGB model
def SMOTE_ratio_test(X_train_NNtransformed, y_train,class_weight,strategy_pct):
    # class_weight = {'No Crash':0.2,'1 Crash':0.3,'>=2 Crashes':0.5}
    # strategy_pct = [
    #   [1,0.2,0.2],
    #   [1,0.4,0.2],[1,0.4,0.4],
    #   [1,0.6,0.2],[1,0.6,0.4],[1,0.6,0.6],
    #   [1,0.8,0.2],[1,0.8,0.4],[1,0.8,0.6],[1,0.8,0.8],
    #   [1,1,0.2],[1,1,0.4],[1,1,0.6],[1,1,0.8],[1,1,1]]

    print("This function select the best sampling strategy by using default XGB model")
    Classes = [0,1,2]
    target_names = ['No Crash', '1 Crash', '>=2 Crashes']
    print(class_weight)

    SMOTE_process = pd.DataFrame(columns=['ratio_0','ratio_1','ratio_2','time',
                                          'f2_micro','f2_macro','f2_by_wt','f1_micro','f1_macro','f1_by_wt','precision_macro','recall_macro',
                                         'Class0','Class1','Class2'])
    SMOTE_process['Class0'] = SMOTE_process['Class0'].astype('object')
    SMOTE_process['Class1'] = SMOTE_process['Class1'].astype('object')
    SMOTE_process['Class2'] = SMOTE_process['Class2'].astype('object')
    SMOTE_process = SMOTE_process.astype('object')

    base_number = y_train.value_counts()[y_train.value_counts().index==0].values[0]
    base = [base_number,base_number,base_number]
    print('base: ', base)
    print('class_weight: ',class_weight)
    print('Testing strategies: ', strategy_pct)

    XGBmodel = XGBClassifier(n_jobs=-1,random_state=42,objective='multi:softmax')

    for idx,s in enumerate(strategy_pct):

        strategy = dict(zip(Classes, [round(i) for i in np.multiply(base,s)]))
        SMOTE_process.loc[idx,'ratio_0'] = s[0]
        SMOTE_process.loc[idx,'ratio_1'] = s[1]
        SMOTE_process.loc[idx,'ratio_2'] = s[2]
        sampling = SMOTE(sampling_strategy=strategy,random_state=42)
        X_train_NNtransformed_sampled, y_train_sampled = sampling.fit_resample(X_train_NNtransformed, y_train)
        t0 = time.time()
        XGBmodel_fitted=XGBmodel.fit(X_train_NNtransformed_sampled, y_train_sampled)
        t1 = time.time()
        SMOTE_process.loc[idx,'time'] = t1-t0

    # If use SMOTENC instead
    #     cols_needs_tobe_coded_idx = np.where(np.isin(df_rural_train_sub.columns.to_list(),cols_needs_tobe_coded.to_list()))[0].tolist()
    #     sampling = SMOTENC(categorical_features = cols_needs_tobe_coded_idx,sampling_strategy=strategy,random_state=42)
    #     X_train_sampled, y_train_sampled = sampling.fit_resample(df_rural_train_sub_catcode, y_train_cat_label)

        # True values
        y_true = y_test
        # Predicted values
        y_pred = XGBmodel_fitted.predict(X_test_NNtransformed)
        SMOTE_process.loc[idx,'f2_micro'] = fbeta_score(y_true, y_pred, average='micro', beta=2)
        SMOTE_process.loc[idx,'f2_macro'] = fbeta_score(y_true, y_pred, average='macro', beta=2)
        SMOTE_process.loc[idx,'f2_by_wt'] = f2_by_wt(y_true, y_pred)
        SMOTE_process.loc[idx,'f1_micro'] = fbeta_score(y_true, y_pred, average='micro', beta=1)
        SMOTE_process.loc[idx,'f1_macro'] = fbeta_score(y_true, y_pred, average='macro', beta=1)
        SMOTE_process.loc[idx,'f1_by_wt'] = f1_by_wt(y_true, y_pred)
        SMOTE_process.loc[idx,'precision_macro'] = precision_score(y_true, y_pred, average='macro')
        SMOTE_process.loc[idx,'recall_macro'] = recall_score(y_true, y_pred, average='macro')
        SMOTE_process = SMOTE_process.astype('object')
        SMOTE_process.loc[idx,'Class0'] = metrics.confusion_matrix(y_true, y_pred)[0].tolist()
        SMOTE_process.loc[idx,'Class1'] = metrics.confusion_matrix(y_true, y_pred)[1].tolist()
        SMOTE_process.loc[idx,'Class2'] = metrics.confusion_matrix(y_true, y_pred)[2].tolist()

        return SMOTE_process

# Visualize SMOTE sampling process
def plt_SMOTE_Multiclass(X_train_NNtransformed,y_train,strategy):

    sampling = SMOTE(sampling_strategy=strategy,random_state=42)
    X_train_NNtransformed_Oversampled, y_train_Oversampled = sampling.fit_resample(X_train_NNtransformed, y_train)

    pca = PCA()
    pca_fitted = pca.fit(X_train_NNtransformed)
    X_original = pca_fitted.transform(X_train_NNtransformed)
    y_original = y_train
    X_aftersampling = pca_fitted.transform(X_train_NNtransformed_Oversampled)
    y_aftersampling = y_train_Oversampled
    # print(pca.explained_variance_ratio_)

    colors = {2:'green', 1:'blue', 0:'yellow'}

    fig,axs = plt.subplots(3, 2,figsize=(10,12))
    axs[0,0].scatter(X_original[y_original == 0][:,0],X_original[y_original == 0][:,1],c='grey',alpha=0.2,s=3)
    axs[0,1].scatter(X_aftersampling[y_aftersampling == 0][:,0],X_aftersampling[y_aftersampling == 0][:,1],c='grey',alpha=0.2,s=3)
    axs[1,0].scatter(X_original[y_original == 1][:,0],X_original[y_original == 1][:,1],c='blue',alpha=0.1,s=3)
    axs[1,1].scatter(X_aftersampling[y_aftersampling == 1][:,0],X_aftersampling[y_aftersampling == 1][:,1],c='blue',alpha=0.2,s=3)
    axs[2,0].scatter(X_original[y_original == 2][:,0],X_original[y_original == 2][:,1],c='green',alpha=0.2,s=3)
    axs[2,1].scatter(X_aftersampling[y_aftersampling == 2][:,0],X_aftersampling[y_aftersampling == 2][:,1],c='green',alpha=0.2,s=3)

    axs[0,0].set_title('Original Class_0 (0 Crash)')
    axs[0,1].set_title('Sampled Class_0 (0 Crash)')
    axs[1,0].set_title('Original Class_1 (1 Crash)')
    axs[1,1].set_title('Sampled Class_1 (1 Crash)')
    axs[2,0].set_title('Original Class_2 (>=2 Crashes)')
    axs[2,1].set_title('Sampled Class_2 (>=2 Crashes)')

    axs[0,0].set_yticks(axs[0,0].get_yticks().tolist())
    axs[0,0].set_xticks(axs[0,0].get_xticks().tolist())
    axs[1,0].set_yticks(axs[0,0].get_yticks().tolist())
    axs[1,0].set_xticks(axs[0,0].get_xticks().tolist())
    axs[2,0].set_yticks(axs[0,0].get_yticks().tolist())
    axs[2,0].set_xticks(axs[0,0].get_xticks().tolist())

    axs[0,1].set_yticks(axs[0,0].get_yticks().tolist())
    axs[0,1].set_xticks(axs[0,0].get_xticks().tolist())
    axs[1,1].set_yticks(axs[0,0].get_yticks().tolist())
    axs[1,1].set_xticks(axs[0,0].get_xticks().tolist())
    axs[2,1].set_yticks(axs[0,0].get_yticks().tolist())
    axs[2,1].set_xticks(axs[0,0].get_xticks().tolist())

    plt.tight_layout()

# =============================================
# Bayesian Tuning
# =============================================

## Naive K-Fold Bayesian Tuning
# Adapted from 'https://github.com/fmfn/BayesianOptimization'
def xgb_cv(max_depth,min_child_weight,eta,gamma,n_estimators,subsample,data,targets):
    """XGB cross validation.
    You may redefine your scoring strategy
    """
    estimator = XGBClassifier(
        max_depth=max_depth,
        min_child_weight=min_child_weight,
        eta=eta,
        gamma=gamma,
        n_estimators=n_estimators,
        subsample=subsample,
        random_state=42,
        n_jobs=-1
    )
    cval = cross_val_score(estimator, data, targets,
                           scoring=scoring['f2_by_wt'], cv=5)
    return cval.mean()

def optimize_xgb(data, targets,searching_range):
    """Apply Bayesian Optimization to XGB parameters."""
    def xgb_crossval(max_depth,min_child_weight,eta,gamma,n_estimators,subsample,searching_range,n_inits,n_iters):
        """Wrapper of XGB cross validation.
        """
        return xgb_cv(
            max_depth=int(max_depth),
            min_child_weight=int(min_child_weight),
            eta=eta,
            gamma=gamma,
            n_estimators=int(n_estimators),
            data=data,
            subsample=subsample,
            targets=targets,
        )

    optimizer = BayesianOptimization(
        f=xgb_crossval,
        pbounds=searching_range,
        random_state=42,
        verbose=2
    )

    print('\n')
    print("Hyperparameter tuning with scoring stretegy: f2_by_wt")
    print('\n')
    optimizer.maximize(init_points=n_inits,n_iter=n_iters)

    print("Final result:", optimizer.max)

    return optimizer

## SMOTE-Nested K-Fold Bayesian Tuning
# Adapted from 'https://github.com/fmfn/BayesianOptimization'
def xgb_smote_cv(max_depth,min_child_weight,eta,gamma,n_estimators,subsample,sampling_ratio,X_train,y_train):
    """XGB cross validation.
    You may redefine your scoring strategy
    """
    estimator = XGBClassifier(
        max_depth=max_depth,
        min_child_weight=round(min_child_weight,2),
        eta=round(eta,2),
        gamma=round(gamma,2),
        n_estimators=n_estimators,
        subsample=round(subsample,1),
        random_state=42,
        n_jobs=-1
    )

    cv = StratifiedKFold(n_splits=5, random_state=42)
    scores = []
    # Split into K Fold
    for train_fold_index, val_fold_index in cv.split(X_train, y_train):
        K_X_train_fold, K_y_train_fold = X_train[train_fold_index], y_train.values[train_fold_index]
        K_X_val_fold, K_y_val_fold = X_train[val_fold_index], y_train.values[val_fold_index]
        # Oversample the training data
        unique_elements, counts_elements = np.unique(K_y_train_fold, return_counts=True)
        base_number = np.max(counts_elements)
        base = [base_number,base_number,base_number]
        Classes = [0,1,2]
        selected_strategy = dict(zip(Classes, [round(i) for i in np.multiply(base,sampling_ratio)]))
        sampling = SMOTE(sampling_strategy=selected_strategy,random_state=42)
        # Model fitting
        K_X_train_Oversampled, K_y_train_Oversampled = sampling.fit_resample(K_X_train_fold, K_y_train_fold)
        estimator_fitted=estimator.fit(K_X_train_Oversampled, K_y_train_Oversampled)
        # Validation Score
        validation_score = f2_by_wt(K_y_val_fold, estimator_fitted.predict(K_X_val_fold))
        scores.append(validation_score)

    # The average of K Fold
    return np.mean(scores)

def optimize_smote_xgb(sampling_ratio,search_range,X_train,y_train,n_inits,n_iters):
    """Apply Bayesian Optimization to XGB parameters."""
    def xgb_val(max_depth,min_child_weight,eta,gamma,n_estimators,subsample):
        """Wrapper of XGB cross validation.
        """
        return xgb_smote_cv(
            max_depth=int(max_depth),
            min_child_weight=round(min_child_weight,2),
            eta=round(eta,2),
            gamma=round(gamma,2),
            n_estimators=int(n_estimators),
            sampling_ratio=sampling_ratio,
            X_train=X_train,
            subsample=round(subsample,2),
            y_train=y_train
        )

    optimizer = BayesianOptimization(
        f=xgb_val,
        pbounds=search_range,
        random_state=42,
        verbose=2
    )

    print('\n')
    print("Hyperparameter tuning with scoring stretegy: f2_by_wt")
    print('\n')
    optimizer.maximize(init_points=n_inits,n_iter=n_iters)

    print("Final result:", optimizer.max)

    return optimizer

# =============================================
# Examine by plots
# =============================================

def plt_multiclass_observed_crashes_map (axes,df_geom,y_true,year):
    # Example: plt_observed_crashes_map (axes[0],df_rural_train, y_true, 2014)
    warnings.simplefilter(action='ignore', category=UserWarning)
    gdf_geom  = gpd.GeoDataFrame(df_geom, geometry=df_geom['geometry'])
    gdf_geom['Crashes_cat']=y_true.values
    gdf_geom.loc[gdf_geom['Crashes_cat']>=3,'Crashes_cat']='3+'
    gdf_geom['Crashes_cat']=gdf_geom['Crashes_cat'].astype('category')
    gdf_geom['Crashes_cat']=gdf_geom['Crashes_cat'].apply(str)
    gdf_geom[
        (gdf_geom['Year']==year)&
        (gdf_geom['Crashes_cat']=='0.0')].plot(column='Crashes_cat',ax=axes, linewidth=0.5, legend=True,color='grey')
    gdf_geom[
        (gdf_geom['Year']==year)&
        (gdf_geom['Crashes_cat']=='1.0')].plot(column='Crashes_cat',ax=axes, linewidth=2,legend=True,color='orange')
    gdf_geom[
        (gdf_geom['Year']==year)&
        (gdf_geom['Crashes_cat']=='2.0')].plot(column='Crashes_cat',ax=axes, linewidth=2,legend=True,color='red')

def plt_multiclass_prediction_maps (axes, df_geom, prediction_result, year):
    # example: prediction_maps (axes[0], df_rural_test, y_pred, 2018, 'segment', 'rural', 'XGB-MultiClass', 'testmap')

    prediction_geom = pd.concat([df_geom, pd.DataFrame(prediction_result,index=df_geom.index,columns=['prediction'])], axis=1, sort=False)
    prediction_geom  = gpd.GeoDataFrame(prediction_geom , geometry=prediction_geom['geometry'])
    warnings.simplefilter(action='ignore', category=UserWarning)
    prediction_geom[
        (prediction_geom['Year']==year)&
        (prediction_geom['prediction']==0)].plot(column='prediction',ax=axes,linewidth=0.5,legend=True,color='grey')
    prediction_geom[
        (prediction_geom['Year']==year)&
        (prediction_geom['prediction']==1)].plot(column='prediction',ax=axes, linewidth=2,legend=True,color='orange')
    prediction_geom[
        (prediction_geom['Year']==year)&
        (prediction_geom['prediction']==2)].plot(column='prediction',ax=axes, linewidth=2,legend=True,color='red')
    plt.legend(['Class_0','Class_1','Class_2'])


def pred_future_multiclass_maps (axplace,geo_SubsegmentID,df_geom, prediction, year, segment_intersection, model_geography, model_name, map_name):
    prediction_geom = df_geom.copy(deep=True)
    prediction_geom['prediction'] = prediction
    prediction_geom  = gpd.GeoDataFrame(prediction_geom , geometry=prediction_geom['geometry'])
    warnings.simplefilter(action='ignore', category=UserWarning)
    geo_SubsegmentID.plot(ax=axplace, color='grey')
#     prediction_geom[(prediction_geom['Year']==year)&(prediction_geom['prediction']==0)].plot(column='Crashes_cat',ax=axplace, linewidth=1,legend=True,color='grey')
    prediction_geom[(prediction_geom['Year']==year)&(prediction_geom['prediction']==1)].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='orange')
    prediction_geom[(prediction_geom['Year']==year)&(prediction_geom['prediction']==2)].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='red')
    axplace.title.set_text(str(year))
