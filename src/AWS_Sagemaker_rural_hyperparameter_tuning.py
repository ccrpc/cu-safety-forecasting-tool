"""
Adapted from https://github.com/aws/amazon-sagemaker-examples/blob/master/hyperparameter_tuning/xgboost_direct_marketing/hpo_xgboost_direct_marketing_sagemaker_python_sdk.ipynb
Hyperparameter guidance: https://docs.aws.amazon.com/sagemaker/latest/dg/xgboost-tuning.html
"""

import os
import time
import boto3
import sagemaker
from sagemaker import image_uris
from sagemaker.amazon.amazon_estimator import get_image_uri
from sagemaker.inputs import TrainingInput
from sagemaker.tuner import IntegerParameter, CategoricalParameter, ContinuousParameter, HyperparameterTuner
import config

def main(bucket,prefix):

    bucket_path = "s3://{}".format(bucket)
    training_data_path = bucket_path + "/" + prefix + "/" + config.training_data_name
    validation_data_path = bucket_path + "/" + prefix + "/" + config.validation_data_name

    model_output_path = bucket_path +"/"+ prefix + "/models"

    print(f"bucket path: {bucket_path}")
    print(f"training data  path: {training_data_path}")
    print(f"validation data path: {validation_data_path}")
    print(f"model output path: {model_output_path}")

    boto_session = boto3.Session(
    region_name = config.region,
    aws_access_key_id = config.my_access_key_id,
    aws_secret_access_key = config.my_secret_key
    )

    boto_client = boto_session.client('sagemaker')
    sm_session = sagemaker.Session(boto_session)
    xgb_image = image_uris.retrieve('xgboost', config.region, version='latest')

    # role = sagemaker.get_execution_role()
    # print(f"role: {role}")

    xgb = sagemaker.estimator.Estimator(xgb_image,
                                    'arn:aws:iam::105197606076:role/service-role/AmazonSageMaker-ExecutionRole-20200821T113939',
                                    instance_count=1,
                                    instance_type='ml.m5.4xlarge',
                                    output_path=model_output_path,
                                    sagemaker_session=sm_session)


    xgb.set_hyperparameters(eval_metric='merror',
                        objective='multi:softmax',
                        num_class=3)


    hyperparameter_ranges = {
    'max_depth': IntegerParameter(3, 10),
    'min_child_weight': IntegerParameter(1, 10),
    'max_depth': IntegerParameter(3, 10),
    'num_round': IntegerParameter(50, 200),
    'eta': ContinuousParameter(0.1,0.5),
    'gamma': ContinuousParameter(0.1,5),
    'subsample': ContinuousParameter(0.5,0.9)}


    objective_metric_name = 'validation:merror'

    tuner = HyperparameterTuner(xgb,
    objective_metric_name,
    hyperparameter_ranges,
    objective_type='Minimize',
    max_jobs = 6,
    max_parallel_jobs=2)

    # s3_input_train = sagemaker.s3_input(s3_data=training_data_path, content_type='csv')
    # s3_input_validation = sagemaker.s3_input(s3_data=validation_data_path, content_type='csv')
    s3_input_train = TrainingInput(training_data_path, content_type='csv')
    s3_input_validation = TrainingInput(validation_data_path, content_type='csv')

    tuner.fit({"train":s3_input_train,"validation":s3_input_validation}, include_cls_metadata=False)

    boto3.client('sagemaker', region_name=config.region).describe_hyper_parameter_tuning_job(HyperParameterTuningJobName=tuner.latest_tuning_job.job_name)['HyperParameterTuningJobStatus']

if __name__ == "__main__":
    bucket = "multiclass-012plus"
    prefix = "Rural"
    main(bucket,prefix)
