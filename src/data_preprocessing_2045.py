# import libraries
import pickle
import json
import requests
import numpy as np
import pandas as pd
import geopandas as gpd
from shapely import wkt
import matplotlib.pyplot as plt
import collections

pd.options.mode.chained_assignment = None  # default='warn'

# Section 4.3.3 Update all the features in 2045 except Pop_den
def upd_rdfeatures(street_2045_sub, df_test):

    df_2045_dupinfo = pd.merge(street_2045_sub,df_test,how='right',on = 'segment_id')
    # sort columns to see which columns are duplicated
    df_2045_dupinfo = df_2045_dupinfo[sorted(df_2045_dupinfo.columns)]

    df_2045 = df_test.copy()
    # drop segments that have duplicate records
    duplicate_segs = [item for item, count in collections.Counter(df_2045['segment_id'].values).items() if count > 1]
    for seg in duplicate_segs:
        df_2045 = df_2045.drop(df_2045[df_2045['segment_id'] == seg].index.values[1:]) # keep the first record
    # use segment_idas df index
    df_2045.index = df_2045['segment_id'].values
    coldict = dict(zip(np.arange(len(df_2045.columns)), df_2045.columns))

    colneedupd_1 = ['bicycle_facility_width','bicycle_path_category','bicycle_path_type','bicycle_approach_alignment','bus_trips_total',
              'crossing_aadt','crossing_functional_classification','crossing_speed','functional_classification','intersection_control_type',
             'lane_configuration','max_lanes_crossed','parking_lane_width','posted_speed','railroad_crossing_type','right_turn_length',
             'road_sign_type','sidewalk_buffer_width','sidewalk_condition_score','sidewalk_width','sidewalk_buffer_type','total_lanes',
             'volume_capacity','overall_landuse']
    for updcol in colneedupd_1:
        updnewcol = updcol+'_x'
        segsneedupd = df_2045_dupinfo[['segment_id',updnewcol]][df_2045_dupinfo[updnewcol].notnull()] # find row (index) that needs update
        rowsneedupd = segsneedupd.segment_id.values
        df_2045.loc[rowsneedupd,updcol] = segsneedupd[updnewcol].values


    colneedupd_2 = ['AADT','HCV','PC']
    updnewcol_2 = ['aadt','heavy_vehicle_count','pavement_condition']
    for i in range(len(colneedupd_2)):
        segsneedupd = df_2045_dupinfo[['segment_id',updnewcol_2[i]]][df_2045_dupinfo[updnewcol_2[i]].notnull()]
        rowsneedupd = segsneedupd.segment_id.values
        df_2045.loc[rowsneedupd,colneedupd_2[i]] = segsneedupd[updnewcol_2[i]].values

    df_2045 = df_2045.drop(['Pop_den'],axis=1)

    print('Columns updated: ',colneedupd_1+colneedupd_2)

    return df_2045

# Section 4.3.4 Update Pop_den for roadsegment
def upd_popden(df_2045,Pref_TAZPop_2045):

    # Assign blockgroup to roadway network
    geo_df_2045 = gpd.GeoDataFrame(
            df_2045,
            geometry=df_2045.geometry,
            crs = {'init': 'epsg:3435'})
    streets_pop_2045 = gpd.sjoin(geo_df_2045,
                            Pref_TAZPop_2045[['geometry', 'pop','area']],
                            how="inner",
                            op='intersects')
    # Calculate pop density
    streets_pop_2045['Pop_den_sqmiles'] = round(streets_pop_2045['pop']/(streets_pop_2045['area']))
    streets_pop_2045 = streets_pop_2045.rename(columns={'Pop_den_sqmiles':'Pop_den'})

    print('Population density for each TAZ calculated. Unit: square miles')

    return streets_pop_2045

# Section 4.3.4 Update Pop_den for roadsegment
def drop_dup_seg(streets_pop_2045,Pref_TAZPop_2045,df_test):

    # find segments that have duplicate values
    duplicate_segs = [item for item, count in collections.Counter(streets_pop_2045['segment_id'].values).items() if count > 1]
    dpseg = streets_pop_2045.loc[streets_pop_2045['segment_id'].isin(duplicate_segs)].sort_values(by=['segment_id'])
    notdpseg = streets_pop_2045.loc[streets_pop_2045['segment_id'].isin(duplicate_segs)==False].sort_values(by=['segment_id'])

    # For dpseg, calculate the length in each TAZ
    seg_tobe_clip = dpseg[['segment_id','geometry']]
    polygon_rowid = dpseg.index_right.values
    polygon_for_clip = Pref_TAZPop_2045.iloc[polygon_rowid,:]
    dp_len = []
    for i in range(seg_tobe_clip.shape[0]):
        dp_len.append(gpd.clip(seg_tobe_clip.iloc[[i]],polygon_for_clip.iloc[[i]]).length.values[0])
    dpseg['dp_len'] = dp_len
    # For each segment, keep the first record and drop the other duplicate ones
    dpseg = dpseg.sort_values(by=['segment_id','dp_len'],ascending=False)
    dpseg = dpseg.drop_duplicates(subset=['segment_id','st_length','curve_ratio'],keep='first')

    # Merge the cleaned dpseg & notdpseg together
    streets_pop_2045 = pd.concat([dpseg,notdpseg])
    streets_pop_2045 = streets_pop_2045.sort_values(by='segment_id')
    #streets_pop_2045.index = df_test.sort_values(by='segment_id').index.values
    if streets_pop_2045.shape[0] == len(np.unique(streets_pop_2045.segment_id.values)):
        print('All the segments have only one record')
        streets_pop_2045 = streets_pop_2045.drop(columns=['dp_len'])
    else:
        print('Failed. Please check the input data')

    return streets_pop_2045
