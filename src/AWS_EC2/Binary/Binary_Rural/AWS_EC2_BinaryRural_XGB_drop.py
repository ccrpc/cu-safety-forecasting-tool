# Import libraries
import pandas as pd
import numpy as np
import pickle
from numpy import argmax
import importlib
import collections
import time
import getpass
import datetime as datetime

from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler, OneHotEncoder, FunctionTransformer,  LabelEncoder
from sklearn.linear_model import LinearRegression,  LogisticRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.pipeline import Pipeline
from sklearn.dummy import DummyClassifier
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score, recall_score, precision_recall_curve, brier_score_loss, auc
from sklearn.metrics import f1_score, fbeta_score, confusion_matrix, make_scorer, classification_report
from sklearn.utils.class_weight import compute_class_weight
from sklearn.model_selection import  StratifiedKFold, KFold, cross_val_score, StratifiedShuffleSplit
from sklearn.model_selection import  RepeatedStratifiedKFold, GridSearchCV, cross_validate
from xgboost import XGBClassifier

print('HELLO')

def func_OHtransform_var (df):
    df_variables=df.columns.tolist()
    num_attributes=df.describe().T.index.tolist()
    cat_attributes=[x for x in df_variables if x not in num_attributes]
    variables_pipeline = ColumnTransformer([
        ("num", StandardScaler(), num_attributes),
        ("cat", OneHotEncoder(), cat_attributes),
    ])
    df_transformed = variables_pipeline.fit_transform(df)
    return df_transformed

def tn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 0]
def fp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 1]
def fn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 0]
def tp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 1]
def precision(y_true, y_pred): return precision_score(y_true, y_pred)
def recall(y_true, y_pred): return recall_score(y_true, y_pred)
def fbeta_2(y_true,y_pred,beta=2.0): return fbeta_score(y_true, y_pred, beta=2.0)

scoring = {'tp': make_scorer(tp), 'tn': make_scorer(tn), 'fp': make_scorer(fp), 'fn': make_scorer(fn),
           'precision':make_scorer(precision),'recall':make_scorer(recall),
           'fbeta_2':make_scorer(fbeta_2)}

def cfm(y, y_pred_adj):
    all_confusionmatrix = pd.DataFrame(confusion_matrix(y, y_pred_adj),
                           columns=['pred_neg', 'pred_pos'],
                           index=['true_neg', 'true_pos'])
    true_negative = all_confusionmatrix.iloc[0,0]
    false_positive = all_confusionmatrix.iloc[0,1]
    false_negative = all_confusionmatrix.iloc[1,0]
    true_positive = all_confusionmatrix.iloc[1,1]
    print(all_confusionmatrix)
    print('\n')

    return true_negative,false_positive,false_negative,true_positive

with open('./df_rural_train.pkl', 'rb') as f:
    df_rural_train = pickle.load(f)
with open('./df_rural_train_label.pkl', 'rb') as f:
    df_rural_train_label = pickle.load(f)
with open('./df_rural_test.pkl', 'rb') as f:
    df_rural_test = pickle.load(f)
with open('./df_rural_test_label.pkl', 'rb') as f:
    df_rural_test_label = pickle.load(f)

# Remove features that should not be fed into the model
remove_list=['segment_id','overlap','geometry','Year']
targetlist = [i for i in list(df_rural_train.columns) if i not in remove_list]
df_rural_train_sub = df_rural_train[targetlist]
df_rural_test_sub = df_rural_test[targetlist]

# Convert full training & testing data to binary class categories
df_rural_train_bi_label = df_rural_train_label.mask(df_rural_train_label >=2, 1)
df_rural_test_bi_label=df_rural_test_label.mask(df_rural_test_label >= 2, 1)

X_train=df_rural_train_sub
y_train=df_rural_train_bi_label
X_test=df_rural_test_sub
y_test=df_rural_test_bi_label

X_train_OHtransformed=func_OHtransform_var(X_train)
X_test_OHtransformed=func_OHtransform_var(X_test)

sampling=None #sampling=SMOTE()

search_localdrop = []
search_majcolldrop = []
search_mincolldrop = []
search_max_depth = []
search_eta = []
search_subsample = []
search_min_child_weight = []
search_scale_pos_weight = []
search_threshold = []
search_precision = []
search_recall = []
search_f1score = []
search_f2score = []
search_fpr = []
search_tn = []
search_tp = []
search_fn = []
search_fp = []
search_time = []

df_rural_train_drop = df_rural_train_sub.copy()
df_rural_train_drop['y'] = y_train
localrd_pos_neg_counts = df_rural_train_drop[df_rural_train_drop['functional_classification']=='Local Road or Street'].y.value_counts()
localrd_neg_pos_ratio = int(localrd_pos_neg_counts[0]/localrd_pos_neg_counts[1])
majcoll_pos_neg_counts = df_rural_train_drop[df_rural_train_drop['functional_classification']=='Major Collector'].y.value_counts()
majcoll_neg_pos_ratio = int(majcoll_pos_neg_counts[0]/majcoll_pos_neg_counts[1])
mincoll_pos_neg_counts = df_rural_train_drop[df_rural_train_drop['functional_classification']=='Minor Collector'].y.value_counts()
mincoll_neg_pos_ratio = int(mincoll_pos_neg_counts[0]/mincoll_pos_neg_counts[1])

for localrd_dropratio in [0,0.05,0.1,0.25]:
    for majcoll_dropratio in [0,0.05,0.1,0.25]:
        for mincoll_dropratio in [0,0.05,0.1,0.25]:
            for max_depth in [3,4,5,6,7,8]:
                for eta in [0.1,0.3,0.5]:
                    for subsample in [0.6,0.8,1]:
                        for min_child_weight in [1,2,3,4,5]:
                            for scale_pos_weight in [1,3,5,7,9]:

                                print(localrd_dropratio,majcoll_dropratio,mincoll_dropratio)
                                print(max_depth,eta,subsample,min_child_weight,scale_pos_weight)

                                search_localdrop.append(localrd_dropratio)
                                search_majcolldrop.append(majcoll_dropratio)
                                search_mincolldrop.append(mincoll_dropratio)
                                search_max_depth.append(max_depth)
                                search_eta.append(eta)
                                search_subsample.append(subsample)
                                search_min_child_weight.append(min_child_weight)
                                search_scale_pos_weight.append(scale_pos_weight)

                                # Drop some negative samples of 'Local Road or Street'
                                localrd_removesize = int(localrd_pos_neg_counts[0]*localrd_dropratio)
                                localrd_index_to_remove = sorted(np.random.choice(df_rural_train_drop[(df_rural_train_drop['functional_classification']=='Local Road or Street')&(df_rural_train_drop['y']==0)].index,
                                                    size=localrd_removesize, replace=False))
                                localrd_id_to_remove = np.where(df_rural_train.index.isin(localrd_index_to_remove))[0]
                                # Drop some negative samples of 'Major Collector'
                                majcoll_removesize = int(majcoll_pos_neg_counts[0]*majcoll_dropratio)
                                majcoll_index_to_remove = sorted(np.random.choice(df_rural_train_drop[(df_rural_train_drop['functional_classification']=='Major Collector')&(df_rural_train_drop['y']==0)].index,
                                                    size=majcoll_removesize, replace=False))
                                majcoll_id_to_remove = np.where(df_rural_train.index.isin(majcoll_index_to_remove))[0]
                                # Drop some negative samples of 'Minor Collector'
                                mincoll_removesize = int(mincoll_pos_neg_counts[0]*mincoll_dropratio)
                                mincoll_index_to_remove = sorted(np.random.choice(df_rural_train_drop[(df_rural_train_drop['functional_classification']=='Local Road or Street')&(df_rural_train_drop['y']==0)].index,
                                                    size=mincoll_removesize, replace=False))
                                mincoll_id_to_remove = np.where(df_rural_train.index.isin(mincoll_index_to_remove))[0]

                                # Drop selected rows
                                if len(localrd_index_to_remove)>0 or len(majcoll_index_to_remove)>0 or len(mincoll_index_to_remove)>0:
                                    index_to_remove = localrd_index_to_remove + majcoll_index_to_remove + mincoll_index_to_remove
                                    X_train_drop = df_rural_train_sub.drop(index_to_remove,axis=0)
                                    y_train_drop = y_train.drop(index_to_remove,axis=0)
                                else:
                                    X_train_drop = df_rural_train_sub.copy()
                                    y_train_drop = y_train.copy()
                                if len(localrd_id_to_remove)>0 or len(majcoll_id_to_remove)>0 or len(mincoll_id_to_remove)>0:
                                    id_to_remove = np.concatenate((localrd_id_to_remove,majcoll_id_to_remove,mincoll_id_to_remove))
                                    X_train_OHtransformed_drop = np.delete(X_train_OHtransformed, id_to_remove, axis=0)
                                else:
                                    X_train_OHtransformed_drop = X_train_OHtransformed.copy()


                                t0 = time.time()
                                XGBmodel = XGBClassifier(n_jobs=-1,random_state=42,
                                           max_depth=max_depth, eta=eta,subsample=subsample,min_child_weight=min_child_weight,scale_pos_weight=scale_pos_weight)
                                XGBmodel_fitted=XGBmodel.fit(X_train_OHtransformed_drop, y_train_drop)
                                t1 = time.time()

                                pred_probs = XGBmodel_fitted.predict_proba(X_test_OHtransformed)
                                # keep probabilities for the positive outcome only
                                pred_probs = pred_probs[:, 1]
                                precision, recall, thresholds = precision_recall_curve(y_test, pred_probs)
                                f1score = ((1+1**2) * precision * recall) / (1**2*precision + recall)
                                f2score = ((1+2**2) * precision * recall) / (2**2*precision + recall)
                                ix = argmax(f2score)
                                search_threshold.append(thresholds[ix])
                                search_precision.append(precision[ix])
                                search_recall.append(recall[ix])
                                search_f1score.append(f1score[ix])
                                search_f2score.append(f2score[ix])

                                y_test_pred = XGBmodel_fitted.predict_proba(X_test_OHtransformed)[:,1]
                                y_test_pred_adj = [1 if y>=thresholds[ix] else 0 for y in y_test_pred]
                                testing_true_negative,testing_false_positive,testing_false_negative,testing_true_positive = cfm(y_test, y_test_pred_adj)
                                fpr = testing_false_positive/(testing_false_positive+testing_true_negative)
                                search_fpr.append(fpr)
                                search_tn.append(testing_true_negative)
                                search_fp.append(testing_false_positive)
                                search_fn.append(testing_false_negative)
                                search_tp.append(testing_true_positive)

                                search_time.append(t1-t0)


searchresults = pd.DataFrame()
searchresults['droplocal'] = search_localdrop
searchresults['dropmajcoll'] = search_majcolldrop
searchresults['dropmincoll'] = search_mincolldrop
searchresults['max_depth'] = search_max_depth
searchresults['eta'] = search_eta
searchresults['subample'] = search_subsample
searchresults['min_child_weight'] = search_min_child_weight
searchresults['scale_pos_weight'] = search_scale_pos_weight
searchresults['threshold'] = search_threshold
searchresults['precision'] = search_precision
searchresults['recall'] = search_recall
searchresults['f1score'] = search_f1score
searchresults['f2score'] = search_f2score
searchresults['fpr'] = search_fpr
searchresults['tn'] = search_tn
searchresults['fp'] = search_fp
searchresults['fn'] = search_fn
searchresults['tp'] = search_tp
searchresults['time'] = search_time

f = open("BinaryRural_XGB_search_results"+datetime.datetime.now().strftime("%Y%m%d-%H%M%S")+".pkl","wb")
pickle.dump(searchresults,f)
f.close()
