# Import libraries
import pandas as pd
import numpy as np
import pickle
from numpy import argmax
import importlib
import collections
import time
import getpass
import datetime as datetime

from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler, OneHotEncoder, FunctionTransformer,  LabelEncoder
from sklearn.linear_model import LinearRegression,  LogisticRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.pipeline import Pipeline
from sklearn.dummy import DummyClassifier
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score, recall_score, precision_recall_curve, brier_score_loss, auc
from sklearn.metrics import f1_score, fbeta_score, confusion_matrix, make_scorer, classification_report
from sklearn.utils.class_weight import compute_class_weight
from sklearn.model_selection import  StratifiedKFold, KFold, cross_val_score, StratifiedShuffleSplit
from sklearn.model_selection import  RepeatedStratifiedKFold, GridSearchCV, cross_validate
from xgboost import XGBClassifier

from imblearn.over_sampling import SMOTE

print('HELLO')

def func_OHtransform_var (df):
    df_variables=df.columns.tolist()
    num_attributes=df.describe().T.index.tolist()
    cat_attributes=[x for x in df_variables if x not in num_attributes]
    variables_pipeline = ColumnTransformer([
        ("num", StandardScaler(), num_attributes),
        ("cat", OneHotEncoder(), cat_attributes),
    ])
    df_transformed = variables_pipeline.fit_transform(df)
    return df_transformed

def tn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 0]
def fp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 1]
def fn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 0]
def tp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 1]
def precision(y_true, y_pred): return precision_score(y_true, y_pred)
def recall(y_true, y_pred): return recall_score(y_true, y_pred)
def fbeta_2(y_true,y_pred,beta=2.0): return fbeta_score(y_true, y_pred, beta=2.0)

scoring = {'tp': make_scorer(tp), 'tn': make_scorer(tn), 'fp': make_scorer(fp), 'fn': make_scorer(fn),
           'precision':make_scorer(precision),'recall':make_scorer(recall),
           'fbeta_2':make_scorer(fbeta_2)}

def cfm(y, y_pred_adj):
    all_confusionmatrix = pd.DataFrame(confusion_matrix(y, y_pred_adj),
                           columns=['pred_neg', 'pred_pos'],
                           index=['true_neg', 'true_pos'])
    true_negative = all_confusionmatrix.iloc[0,0]
    false_positive = all_confusionmatrix.iloc[0,1]
    false_negative = all_confusionmatrix.iloc[1,0]
    true_positive = all_confusionmatrix.iloc[1,1]
    print(all_confusionmatrix)
    print('\n')

    return true_negative,false_positive,false_negative,true_positive

with open('./df_rural_train.pkl', 'rb') as f:
    df_rural_train = pickle.load(f)
with open('./df_rural_train_label.pkl', 'rb') as f:
    df_rural_train_label = pickle.load(f)
with open('./df_rural_test.pkl', 'rb') as f:
    df_rural_test = pickle.load(f)
with open('./df_rural_test_label.pkl', 'rb') as f:
    df_rural_test_label = pickle.load(f)

# Remove features that should not be fed into the model
remove_list=['segment_id','overlap','geometry','Year']
targetlist = [i for i in list(df_rural_train.columns) if i not in remove_list]
df_rural_train_sub = df_rural_train[targetlist]
df_rural_test_sub = df_rural_test[targetlist]

# Convert full training & testing data to binary class categories
df_rural_train_bi_label = df_rural_train_label.mask(df_rural_train_label >=2, 1)
df_rural_test_bi_label=df_rural_test_label.mask(df_rural_test_label >= 2, 1)

X_train=df_rural_train_sub
y_train=df_rural_train_bi_label
X_test=df_rural_test_sub
y_test=df_rural_test_bi_label

X_train_OHtransformed=func_OHtransform_var(X_train)
X_test_OHtransformed=func_OHtransform_var(X_test)

search_sampratio_Int = []
search_sampratio_LocRd = []
search_sampratio_MajArt = []
search_sampratio_MajCol = []
search_sampratio_MinArt = []
search_sampratio_MinCol = []
search_max_depth = []
search_eta = []
search_subsample = []
search_min_child_weight = []
search_scale_pos_weight = []
search_threshold = []
search_precision = []
search_recall = []
search_f1score = []
search_f2score = []
search_fpr = []
search_tn = []
search_tp = []
search_fn = []
search_fp = []
search_time = []

X_train_Int = df_rural_train_sub[df_rural_train_sub['functional_classification']=='Interstate']
X_train_OHtransformed_Int = X_train_OHtransformed[df_rural_train_sub['functional_classification']=='Interstate',:]
y_train_Int = df_rural_train_bi_label[df_rural_train_sub['functional_classification']=='Interstate']

X_train_LocRd = df_rural_train_sub[df_rural_train_sub['functional_classification']=='Local Road or Street']
X_train_OHtransformed_LocRd = X_train_OHtransformed[df_rural_train_sub['functional_classification']=='Local Road or Street']
y_train_LocRd = df_rural_train_bi_label[df_rural_train_sub['functional_classification']=='Local Road or Street']

X_train_MajArt = df_rural_train_sub[df_rural_train_sub['functional_classification']=='Major Arterial']
X_train_OHtransformed_MajArt = X_train_OHtransformed[df_rural_train_sub['functional_classification']=='Major Arterial']
y_train_MajArt = df_rural_train_bi_label[df_rural_train_sub['functional_classification']=='Major Arterial']

X_train_MajCol = df_rural_train_sub[df_rural_train_sub['functional_classification']=='Major Collector']
X_train_OHtransformed_MajCol = X_train_OHtransformed[df_rural_train_sub['functional_classification']=='Major Collector']
y_train_MajCol = df_rural_train_bi_label[df_rural_train_sub['functional_classification']=='Major Collector']

X_train_MinArt = df_rural_train_sub[df_rural_train_sub['functional_classification']=='Minor Arterial']
X_train_OHtransformed_MinArt = X_train_OHtransformed[df_rural_train_sub['functional_classification']=='Minor Arterial']
y_train_MinArt = df_rural_train_bi_label[df_rural_train_sub['functional_classification']=='Minor Arterial']

X_train_MinCol = df_rural_train_sub[df_rural_train_sub['functional_classification']=='Minor Collector']
X_train_OHtransformed_MinCol = X_train_OHtransformed[df_rural_train_sub['functional_classification']=='Minor Collector']
y_train_MinCol = df_rural_train_bi_label[df_rural_train_sub['functional_classification']=='Minor Collector']

pos_counts_Int = y_train_Int.value_counts()[1.0]
neg_counts_Int = y_train_Int.value_counts()[0.0]
pos_counts_LocRd = y_train_LocRd.value_counts()[1.0]
neg_counts_LocRd = y_train_LocRd.value_counts()[0.0]
pos_counts_MajArt = y_train_MajArt.value_counts()[1.0]
neg_counts_MajArt = y_train_MajArt.value_counts()[0.0]
pos_counts_MajCol = y_train_MajCol.value_counts()[1.0]
neg_counts_MajCol = y_train_MajCol.value_counts()[0.0]
pos_counts_MinArt = y_train_MinArt.value_counts()[1.0]
neg_counts_MinArt = y_train_MinArt.value_counts()[0.0]
pos_counts_MinCol = y_train_MinCol.value_counts()[1.0]
neg_counts_MinCol = y_train_MinCol.value_counts()[0.0]

sampratio = [0,0.125,0.25,0.375,0.5]
for sampratio_Int in sampratio:
    addpos_counts_Int = round(sampratio_Int*pos_counts_Int)
    for sampratio_LocRd in sampratio:
        addpos_counts_LocRd = round(sampratio_LocRd*pos_counts_LocRd)
        for sampratio_MajArt in sampratio:
            addpos_counts_MajArt = round(sampratio_MajArt*pos_counts_MajArt)
            for sampratio_MajCol in sampratio:
                addpos_counts_MajCol = round(sampratio_MajCol*pos_counts_MajCol)
                for sampratio_MinArt in sampratio:
                    addpos_counts_MinArt = round(sampratio_MinArt*pos_counts_MinArt)
                    for sampratio_MinCol in sampratio:
                        addpos_counts_MinCol = round(sampratio_MinCol*pos_counts_MinCol)

                        index_to_dup_Int = sorted(np.random.choice(df_rural_train_sub[(df_rural_train_sub['functional_classification']=='Interstate')&(y_train==1)].index,
                                            size=addpos_counts_Int, replace=False))
                        id_to_dup_Int = np.where(df_rural_train_sub.index.isin(index_to_dup_Int))[0]
                        index_to_dup_LocRd = sorted(np.random.choice(df_rural_train_sub[(df_rural_train_sub['functional_classification']=='Local Road or Street')&(y_train==1)].index,
                                            size=addpos_counts_LocRd, replace=False))
                        id_to_dup_LocRd = np.where(df_rural_train_sub.index.isin(index_to_dup_LocRd))[0]
                        index_to_dup_MajArt = sorted(np.random.choice(df_rural_train_sub[(df_rural_train_sub['functional_classification']=='Major Arterial')&(y_train==1)].index,
                                            size=addpos_counts_MajArt, replace=False))
                        id_to_dup_MajArt = np.where(df_rural_train_sub.index.isin(index_to_dup_MajArt))[0]
                        index_to_dup_MajCol = sorted(np.random.choice(df_rural_train_sub[(df_rural_train_sub['functional_classification']=='Major Collector')&(y_train==1)].index,
                                            size=addpos_counts_MajCol, replace=False))
                        id_to_dup_MajCol = np.where(df_rural_train_sub.index.isin(index_to_dup_MajCol))[0]
                        index_to_dup_MinArt = sorted(np.random.choice(df_rural_train_sub[(df_rural_train_sub['functional_classification']=='Minor Arterial')&(y_train==1)].index,
                                            size=addpos_counts_MinArt, replace=False))
                        id_to_dup_MinArt = np.where(df_rural_train_sub.index.isin(index_to_dup_MinArt))[0]
                        index_to_dup_MinCol = sorted(np.random.choice(df_rural_train_sub[(df_rural_train_sub['functional_classification']=='Minor Collector')&(y_train==1)].index,
                                            size=addpos_counts_MinCol, replace=False))
                        id_to_dup_MinCol = np.where(df_rural_train_sub.index.isin(index_to_dup_MinCol))[0]

                        index_to_dup = index_to_dup_Int + index_to_dup_LocRd + index_to_dup_MajArt + index_to_dup_MajCol + index_to_dup_MinArt + index_to_dup_MinCol
                        id_to_dup = np.concatenate((id_to_dup_Int,id_to_dup_LocRd,id_to_dup_MajArt,id_to_dup_MajCol,id_to_dup_MinArt,id_to_dup_MinCol))

                        X_train_OHtransformed_Oversampled = np.concatenate((X_train_OHtransformed,X_train_OHtransformed[id_to_dup,:]),axis=0)
                        y_train_Oversampled = np.concatenate((y_train.values,y_train.values[id_to_dup]),axis=0)

                        search_sampratio_Int.append(sampratio_Int)
                        search_sampratio_LocRd.append(sampratio_LocRd)
                        search_sampratio_MajArt.append(sampratio_MajArt)
                        search_sampratio_MajCol.append(sampratio_MajCol)
                        search_sampratio_MinArt.append(sampratio_MinArt)
                        search_sampratio_MinCol.append(sampratio_MinCol)

                        for max_depth in [3,4,5,6]:
                            for eta in [0.1,0.3,0.5]:
                                for subsample in [0.6,0.8,1]:
                                    for min_child_weight in [1,2,3,4,5]:
                                        for scale_pos_weight in [1,3,5,7,9]:

                                            print(sampratio_Int,sampratio_LocRd,sampratio_MajArt,sampratio_MajCol,sampratio_MinArt,sampratio_MinCol)
                                            print(max_depth,eta,subsample,min_child_weight,scale_pos_weight)

                                            search_max_depth.append(max_depth)
                                            search_eta.append(eta)
                                            search_subsample.append(subsample)
                                            search_min_child_weight.append(min_child_weight)
                                            search_scale_pos_weight.append(scale_pos_weight)

                                            t0 = time.time()
                                            XGBmodel = XGBClassifier(n_jobs=-1,random_state=42,
                                                                    max_depth=max_depth, eta=eta,subsample=subsample,min_child_weight=min_child_weight,scale_pos_weight=scale_pos_weight)
                                            XGBmodel_fitted=XGBmodel.fit(X_train_OHtransformed_Oversampled, y_train_Oversampled)
                                            t1 = time.time()

                                            train_pred_probs = XGBmodel_fitted.predict_proba(X_train_OHtransformed_Oversampled)
                                            train_pred_probs = train_pred_probs[:, 1]
                                            precision, recall, thresholds = precision_recall_curve(y_train_Oversampled, train_pred_probs)
                                            f1score = ((1+1**2) * precision * recall) / (1**2*precision + recall)
                                            f2score = ((1+2**2) * precision * recall) / (2**2*precision + recall)
                                            ix = argmax(f2score)
                                            best_thres = round(thresholds[ix],2)
                                            print(best_thres)

                                            # Check the performance on test data
                                            test_pred_probs = XGBmodel_fitted.predict_proba(X_test_OHtransformed)
                                            test_pred_probs = test_pred_probs[:,1]
                                            test_pred_adj = [1 if y>=best_thres else 0 for y in test_pred_probs]
                                            # Confusion matrix
                                            testing_true_negative,testing_false_positive,testing_false_negative,testing_true_positive = cfm(y_test, test_pred_adj)
                                            precision = testing_true_positive/(testing_true_positive+testing_false_positive)
                                            fpr = testing_false_positive/(testing_false_positive+testing_true_negative)
                                            recall = testing_true_positive/(testing_true_positive+testing_false_negative)
                                            f1score = ((1+1**2) * precision * recall) / (1**2*precision + recall)
                                            f2score = ((1+2**2) * precision * recall) / (2**2*precision + recall)

                                            search_threshold.append(best_thres)
                                            search_precision.append(precision)
                                            search_recall.append(recall)
                                            search_f1score.append(f1score)
                                            search_f2score.append(f2score)
                                            search_fpr.append(fpr)

                                            search_tn.append(testing_true_negative)
                                            search_fp.append(testing_false_positive)
                                            search_fn.append(testing_false_negative)
                                            search_tp.append(testing_true_positive)

                                            search_time.append(t1-t0)


searchresults = pd.DataFrame()
searchresults['search_sampratio_Int'] = search_sampratio_Int
searchresults['search_sampratio_LocRd'] = search_sampratio_LocRd
searchresults['search_sampratio_MajArt'] = search_sampratio_MajArt
searchresults['search_sampratio_MajCol'] = search_sampratio_MajCol
searchresults['search_sampratio_MinArt'] = search_sampratio_MinArt
searchresults['search_sampratio_MinCol'] = search_sampratio_MinCol

searchresults['max_depth'] = search_max_depth
searchresults['eta'] = search_eta
searchresults['subample'] = search_subsample
searchresults['min_child_weight'] = search_min_child_weight
searchresults['scale_pos_weight'] = search_scale_pos_weight

searchresults['threshold'] = search_threshold
searchresults['precision'] = search_precision
searchresults['recall'] = search_recall
searchresults['f1score'] = search_f1score
searchresults['f2score'] = search_f2score
searchresults['fpr'] = search_fpr
searchresults['tn'] = search_tn
searchresults['fp'] = search_fp
searchresults['fn'] = search_fn
searchresults['tp'] = search_tp
searchresults['time'] = search_time

f = open("BinaryRural_XGB_search_results"+datetime.datetime.now().strftime("%Y%m%d-%H%M%S")+".pkl","wb")
pickle.dump(searchresults,f)
f.close()
