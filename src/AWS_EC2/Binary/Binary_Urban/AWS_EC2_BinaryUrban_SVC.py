# Import libraries
import pandas as pd
import numpy as np
import pickle
from numpy import argmax
import importlib
import collections
import time
import getpass
import datetime as datetime

from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler, OneHotEncoder, FunctionTransformer,  LabelEncoder
from sklearn.linear_model import LinearRegression,  LogisticRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.pipeline import Pipeline
from sklearn.dummy import DummyClassifier
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score, recall_score, precision_recall_curve, brier_score_loss, auc
from sklearn.metrics import f1_score, fbeta_score, confusion_matrix, make_scorer, classification_report
from sklearn.utils.class_weight import compute_class_weight
from sklearn.model_selection import  StratifiedKFold, KFold, cross_val_score, StratifiedShuffleSplit
from sklearn.model_selection import  RepeatedStratifiedKFold, GridSearchCV, cross_validate
from sklearn.svm import SVC

print('HELLO')

def func_OHtransform_var (df):
    df_variables=df.columns.tolist()
    num_attributes=df.describe().T.index.tolist()
    cat_attributes=[x for x in df_variables if x not in num_attributes]
    variables_pipeline = ColumnTransformer([
        ("num", StandardScaler(), num_attributes),
        ("cat", OneHotEncoder(), cat_attributes),
    ])
    df_transformed = variables_pipeline.fit_transform(df)
    return df_transformed

def tn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 0]
def fp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 1]
def fn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 0]
def tp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 1]
def precision(y_true, y_pred): return precision_score(y_true, y_pred)
def recall(y_true, y_pred): return recall_score(y_true, y_pred)
def fbeta_2(y_true,y_pred,beta=2.0): return fbeta_score(y_true, y_pred, beta=2.0)

scoring = {'tp': make_scorer(tp), 'tn': make_scorer(tn), 'fp': make_scorer(fp), 'fn': make_scorer(fn),
           'precision':make_scorer(precision),'recall':make_scorer(recall),
           'fbeta_2':make_scorer(fbeta_2)}

def cfm(y, y_pred_adj):
    all_confusionmatrix = pd.DataFrame(confusion_matrix(y, y_pred_adj),
                           columns=['pred_neg', 'pred_pos'],
                           index=['true_neg', 'true_pos'])
    true_negative = all_confusionmatrix.iloc[0,0]
    false_positive = all_confusionmatrix.iloc[0,1]
    false_negative = all_confusionmatrix.iloc[1,0]
    true_positive = all_confusionmatrix.iloc[1,1]
    print(all_confusionmatrix)
    print('\n')

    return true_negative,false_positive,false_negative,true_positive

with open('./df_urban_train.pkl', 'rb') as f:
    df_urban_train = pickle.load(f)
with open('./df_urban_train_label.pkl', 'rb') as f:
    df_urban_train_label = pickle.load(f)
with open('./df_urban_test.pkl', 'rb') as f:
    df_urban_test = pickle.load(f)
with open('./df_urban_test_label.pkl', 'rb') as f:
    df_urban_test_label = pickle.load(f)

# Remove features that should not be fed into the model
remove_list=['segment_id','overlap','geometry','Year']
targetlist = [i for i in list(df_urban_train.columns) if i not in remove_list]
df_urban_train_sub = df_urban_train[targetlist]
df_urban_test_sub = df_urban_test[targetlist]

# Convert full training & testing data to binary class categories
df_urban_train_bi_label = df_urban_train_label.mask(df_urban_train_label >=2, 1)
df_urban_test_bi_label=df_urban_test_label.mask(df_urban_test_label >= 2, 1)

X_train=df_urban_train_sub
y_train=df_urban_train_bi_label
X_test=df_urban_test_sub
y_test=df_urban_test_bi_label

X_train_OHtransformed=func_OHtransform_var(X_train)
X_test_OHtransformed=func_OHtransform_var(X_test)

sampling=None #sampling=SMOTE()

search_kernel = []
search_class_weight = []
search_threshold = []
search_precision = []
search_recall = []
search_f1score = []
search_f2score = []
search_fpr = []
search_tn = []
search_tp = []
search_fn = []
search_fp = []
search_time = []

for kernel in ['linear', 'poly', 'rbf', 'sigmoid']:
    for class_weight in [{1:1},{1:3},{1:5}]:

        print(kernel,class_weight)

        search_kernel.append(kernel)
        search_class_weight.append(class_weight)

        t0 = time.time()
        model = SVC(random_state=42,probability=True,
                    kernel=kernel,class_weight=class_weight)
        model_fitted=model.fit(X_train_OHtransformed, y_train)
        t1 = time.time()

        train_pred_probs = model_fitted.predict_proba(X_train_OHtransformed)
        train_pred_probs = train_pred_probs[:, 1]
        precision, recall, thresholds = precision_recall_curve(y_train, train_pred_probs)
        f1score = ((1+1**2) * precision * recall) / (1**2*precision + recall)
        f2score = ((1+2**2) * precision * recall) / (2**2*precision + recall)
        ix = argmax(f2score)
        best_thres = round(thresholds[ix],2)
        print(best_thres)

        # Check the performance on test data
        test_pred_probs = model_fitted.predict_proba(X_test_OHtransformed)
        test_pred_probs = test_pred_probs[:,1]
        test_pred_adj = [1 if y>=best_thres else 0 for y in test_pred_probs]
        # Confusion matrix
        testing_true_negative,testing_false_positive,testing_false_negative,testing_true_positive = cfm(y_test, test_pred_adj)
        precision = testing_true_positive/(testing_true_positive+testing_false_positive)
        fpr = testing_false_positive/(testing_false_positive+testing_true_negative)
        recall = testing_true_positive/(testing_true_positive+testing_false_negative)
        f1score = ((1+1**2) * precision * recall) / (1**2*precision + recall)
        f2score = ((1+2**2) * precision * recall) / (2**2*precision + recall)

        search_threshold.append(best_thres)
        search_precision.append(precision)
        search_recall.append(recall)
        search_f1score.append(f1score)
        search_f2score.append(f2score)
        search_fpr.append(fpr)
        search_tn.append(testing_true_negative)
        search_fp.append(testing_false_positive)
        search_fn.append(testing_false_negative)
        search_tp.append(testing_true_positive)

        search_time.append(t1-t0)

searchresults = pd.DataFrame()
searchresults['kernel'] = kernel
searchresults['class_weight'] = class_weight

searchresults['threshold'] = search_threshold
searchresults['precision'] = search_precision
searchresults['recall'] = search_recall
searchresults['f1score'] = search_f1score
searchresults['f2score'] = search_f2score
searchresults['fpr'] = search_fpr
searchresults['tn'] = search_tn
searchresults['fp'] = search_fp
searchresults['fn'] = search_fn
searchresults['tp'] = search_tp
searchresults['time'] = search_time

f = open("BinaryUrban_SVC_search_results"+datetime.datetime.now().strftime("%Y%m%d-%H%M%S")+".pkl","wb")
pickle.dump(searchresults,f)
f.close()
