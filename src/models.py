import numpy as np
from numpy import argmax
from sklearn.model_selection import cross_val_score
from matplotlib import pyplot as plt
from shapely import wkt
import warnings
import pandas as pd
import geopandas as gpd
import datetime
from csv import DictWriter
import categorical_embedder as ce
from sklearn import svm
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler, OneHotEncoder, FunctionTransformer,  LabelEncoder
from sklearn.model_selection import StratifiedShuffleSplit
from imblearn.over_sampling import SMOTE
from imblearn.combine import SMOTEENN
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_recall_curve, auc, fbeta_score, f1_score,precision_score, recall_score, brier_score_loss
from sklearn.metrics import mean_squared_error
from sklearn.metrics import roc_curve,roc_auc_score
from sklearn.dummy import DummyClassifier
from varname import nameof
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


#######Feature Pre-processing Functions#############
# Make sure each road segments has only one record
def clean_seg(df_train,df_train_label):
    # Make sure there is only one record for each segment
    df_train_full = df_train.copy()
    df_train_full['df_train_label'] = df_train_label.values
    df_train_full = df_train_full.drop_duplicates(subset=['segment_id', 'Year'], keep='first')
    df_train_label = df_train_full['df_train_label']
    df_train = df_train_full.iloc[:,:-1]
    return df_train,df_train_label

# convert categorical variables with string values to numeric
def func_PC_numeric(X):
    ordered_PC=['Unknown','Failed','Poor','Fair','Good','Excellent']
    X.loc[:,'PC'] = pd.Categorical(X['PC'],ordered=True,categories=ordered_PC)
    X.loc[:,'PC'] = X['PC'].cat.codes.astype(str)
    return X

# 5.2. 2) Feature subsets -- Remove list of variables
def func_remove_var (df,remove_list):
    variables=np.array(df.columns.tolist())
    remove_index=[]
    variable_index=list(range(len(variables)))
    for i in range(len(remove_list)):
        remove_index.append(np.where(variables == remove_list[i])[0][0])
    df_sub=df.drop(df.columns[remove_index],axis=1,inplace=False)
    return df_sub

# 5.2. 2) Feature subsets -- Keep list of variables
def func_keep_var (df,keep_list):
    variables=np.array(df.columns.tolist())
    keep_index=[]
    for i in range(len(keep_list)):
        keep_index.append(np.where(variables == keep_list[i])[0][0])
    return df.iloc[:,keep_index]

# 6.X. 1) Sample subsets -- multi-class classification models
## Convert lable crash count to 4 crash categories and sample the training data for quick test of models
def func_Shuffle_cat (X, y, train_size):
    y_cat = y.mask(y >= 4, 3)
    y_cat_label=LabelEncoder()
    y_cat=y_cat_label.fit_transform(y_cat)
    split=StratifiedShuffleSplit(n_splits=1, train_size=train_size, random_state=42)
    for train_index, test_index in split.split(X,y_cat):
        X_sample=X.iloc[train_index]
        y_sample=y_cat[train_index]
    return X_sample, y_sample

# 5.2. 1) Sample subsets -- binary classification models
# For binary classification models, merge all non-zero crash count to 1 category and sample the training data for quick test of models
def func_Shuffle_simple_cat (X, y, train_size):
    y_cat = y.mask(y >= 1, 1)
    y_cat_label=LabelEncoder()
    y_cat=y_cat_label.fit_transform(y_cat)
    split=StratifiedShuffleSplit(n_splits=1, train_size=train_size, random_state=42)
    for train_index, test_index in split.split(X,y_cat):
        X_sample=X.iloc[train_index]
        y_sample=y_cat[train_index]
    return X_sample, y_sample

##########Feature Transformation Functions###################
# 5.2. 3) Production Feature Preprocessing -- transform numerical and categorical variables
def func_OHtransform_var (df):
    df_variables=df.columns.tolist()
    num_attributes=df.describe().T.index.tolist()
    cat_attributes=[x for x in df_variables if x not in num_attributes]
    variables_pipeline = ColumnTransformer([
        ("num", StandardScaler(), num_attributes),
        ("cat", OneHotEncoder(), cat_attributes),
    ])
    df_transformed = variables_pipeline.fit_transform(df)
    return df_transformed

# 5.2. 3) Production Feature Preprocessing -- get the variable names of the transformed training data
def func_OHtransformed_var (df):
    df_variables=df.columns.tolist()
    num_attributes=df.describe().T.index.tolist()
    cat_attributes=[x for x in df_variables if x not in num_attributes]
    num_feature_names = num_attributes
    cat_feature_names = pd.get_dummies(df[cat_attributes],columns=cat_attributes).columns.tolist()
    df_feature_names = num_feature_names+cat_feature_names
    feature_sel = range(len(df_feature_names))
    fnames = np.array(df_feature_names)[feature_sel]
    return fnames

# 5.2. 3) Development Feature Preprocessing --categorical variable embedding transformation parameters
def func_NNembed_cat_transf_para (X_cat,y, epochs=10, batch_size=256):
    X_encoded,encoders = ce.get_label_encoded_data(X_cat)
    embedding_info = ce.get_embedding_info(X_cat)
    embeddings = ce.get_embeddings(X_encoded, pd.Series(y),
                               categorical_embedding_info=embedding_info,
                               is_classification=True,
                               epochs=epochs,
                               batch_size=batch_size)
    return embeddings,encoders, embedding_info

# 5.2. 3) Development Feature Preprocessing --categorical variable embedding transformation
def func_NNembed_cat_trans (X_cat):
    X = ce.fit_transform(X_cat, embeddings=embeddings, encoders=encoders, drop_categorical_vars=True)
    return X

# 5.2. 3) Development Feature Preprocessing -- transform numerical and categorical (embedded) variables
def func_NNembed_transform_var (df):
    df_variables=df.columns.tolist()
    num_attributes=df.describe().T.index.tolist()
    cat_attributes=[x for x in df_variables if x not in num_attributes]
    variables_pipeline = ColumnTransformer([
        ("num", StandardScaler(), num_attributes),
        ("cat", FunctionTransformer(func_NNembed_cat_trans), cat_attributes),
    ])
    df_transformed = variables_pipeline.fit_transform(df)
    return df_transformed

# Classification model pipeline
def func_OH_classfication_pipeline (func_OHtransform_var, model, sampling):
    return (Pipeline(
    steps=[
        ('variable_transformer', FunctionTransformer(func_OHtransform_var, validate=False)),
        ('sampling',sampling),
        ('model', model)
    ], verbose=True))


# NN Embeded categories classification model pipeline
def func_NNembed_classfication_pipeline (func_NNembed_transform_var,model, sampling):
    return (Pipeline(
    steps=[
        ('variable_transformer',FunctionTransformer(func_NNembed_transform_var)),
        ('sampling',sampling),
        ('model', model)
    ], verbose=True))

# 5.2. 3) Development Feature Preprocessing -- get the variable names of the transformed training data
def func_NNembed_transformed_var (embedding_info,df):
    df_variables=df.columns.tolist()
    num_attributes=df.describe().T.index.tolist()
    embed_size=pd.DataFrame(embedding_info)
    cat_attributes=[]
    for i in range(0,embed_size.shape[1]):
        s=embed_size.columns[i]
        for j in range(0,embed_size.iloc[1,i]):
            code=str(j)
            cat_attributes.append(s+'_embed_'+code)
    df_feature_names = num_attributes+cat_attributes
    feature_sel = range(len(df_feature_names))
    fnames = np.array(df_feature_names)[feature_sel]
    return fnames


########Evalution Matrices############
# plot no skill and model precision-recall curves
def plot_pr_curve (y, y_pred):
    # calculate the no skill line as the proportion of the positive class
    no_skill = len(y[y==1]) / len(y)
    # plot the no skill precision-recall curve
    plt.plot([0, 1], [no_skill, no_skill], linestyle='--', label='No Skill')
    # plot model precision-recall curve
    precision, recall, thresholds = precision_recall_curve(y, y_pred)
    # convert to f2 score
    beta = 2
    f2score = ((1+beta**2) * precision * recall) / (beta**2*precision + recall)
    # locate the index of the largest f score
    ix = np.where(f2score==np.nanmax(f2score))
    print('Best Threshold=%f, precision=%.3f, recall=%.3f, F2_Score=%.3f' % (thresholds[ix][0], precision[ix][0], recall[ix][0], f2score[ix][0]))
    plt.plot(recall, precision, marker='.', label='Model')
    plt.plot(recall[ix][0],precision[ix][0],marker='o',color='black')
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.legend()
    plt.show()



def Bi_F2(X_train_transformed, y_train, X_test_transformed, y_test, pipeline):
    # no skill model, stratified random class predictions
    noskill_model = DummyClassifier(strategy='stratified')
    noskill_model.fit(X_train_transformed, y_train)
    noskill_pred = noskill_model.predict(X_test_transformed)
    f_noskill = fbeta_score(y_test, noskill_pred, beta=2)
    print('No Skill Model F2: %.3f' % f_noskill)
    # perfect model
    f_perfect = fbeta_score(y_test, y_test, beta=2)
    print('Perfect Model F2: %.3f' % f_perfect)
    #model
    y_pred=pipeline.named_steps['model'].predict(X_test_transformed)
    f = fbeta_score(y_test, y_pred, beta=2)
    print('Model F2: %.3f' % f)
    return f_noskill,  f_perfect, f

# calculate the brier skill score
def brier_skill_score(y, y_prob):
    # calculate reference brier score
    ref_prob= len(y[y==1])/len(y)
    ref_probs = [ref_prob for _ in range(len(y))]
    bs_ref = brier_score_loss(y, ref_probs)
    # calculate model brier score
    bs_model = brier_score_loss(y, y_prob)
    # calculate skill score
    return 1.0 - (bs_model / bs_ref)


# 5.3. 6) Mapping -- Observed crashes mapping
def observed_crashes_map (axplace,df_geom,geo_SegmentID,year):
    warnings.simplefilter(action='ignore', category=UserWarning)
    gdf_geom  = gpd.GeoDataFrame(df_geom, geometry=df_geom['geometry'])
    gdf_geom['Crashes_cat']=gdf_geom['Crashes']
    gdf_geom.loc[gdf_geom['Crashes']>3,'Crashes_cat']='4+'
    gdf_geom['Crashes_cat']=gdf_geom['Crashes_cat'].astype('category')
    gdf_geom['Crashes_cat']=gdf_geom['Crashes_cat'].apply(str)
    geo_SegmentID.plot(ax=axplace, color='grey')
    gdf_geom[
        (gdf_geom['Year']==year)&
        (gdf_geom['Crashes_cat']=='1.0')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='yellow')
    gdf_geom[
        (gdf_geom['Year']==year)&
        (gdf_geom['Crashes_cat']=='2.0')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='orange')
    gdf_geom[
        (gdf_geom['Year']==year)&
        (gdf_geom['Crashes_cat']=='3.0')].plot(column='Crashes_cat',ax=axplace, linewidth=3,legend=True,color='red')
    gdf_geom[
        (gdf_geom['Year']==year)&
        (gdf_geom['Crashes_cat']=='4+')].plot(column='Crashes_cat',ax=axplace, linewidth=4,legend=True,color='brown')
    axplace.title.set_text(str(year))

# 5.3. 6) Mapping -- Observed crashes (binary) mapping
def observed_crashes_binary_map (axplace,df_geom,geo_SegmentID,year):
    warnings.simplefilter(action='ignore', category=UserWarning)
    gdf_geom  = gpd.GeoDataFrame(df_geom, geometry=df_geom['geometry'])
    gdf_geom['Crashes_cat']=gdf_geom['Crashes']
    gdf_geom.loc[gdf_geom['Crashes']>1,'Crashes_cat']='>=2'
    gdf_geom['Crashes_cat']=gdf_geom['Crashes_cat'].astype('category')
    gdf_geom['Crashes_cat']=gdf_geom['Crashes_cat'].apply(str)
#     fig, ax = plt.subplots(1, 1, figsize=(15,15))
    geo_SegmentID.plot(axes=axplace, linewidth=0.5, color='grey')
    gdf_geom[
        (gdf_geom['Year']==year)&
        (gdf_geom['Crashes_cat']=='1.0')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='red')
    gdf_geom[
        (gdf_geom['Year']==year)&
        (gdf_geom['Crashes_cat']=='>=2')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='red')
    axplace.title.set_text(str(year))

def observed_crashes_binary_map_byrdclass (axplace,df_geom,geo_SegmentID,year):
    warnings.simplefilter(action='ignore', category=UserWarning)
    gdf_geom  = gpd.GeoDataFrame(df_geom, geometry=df_geom['geometry'])
    gdf_geom['Crashes_cat']=gdf_geom['Crashes']
    gdf_geom.loc[gdf_geom['Crashes']>1,'Crashes_cat']='>=2'
    gdf_geom['Crashes_cat']=gdf_geom['Crashes_cat'].astype('category')
    gdf_geom['Crashes_cat']=gdf_geom['Crashes_cat'].apply(str)
    geo_SegmentID.plot(axes=axplace, linewidth=0.5, color='grey')
    gdf_geom[(gdf_geom['Year']==year)&(gdf_geom['Crashes_cat']!='0.0')&(gdf_geom['functional_classification']=='Local Road or Street')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='yellow')
    gdf_geom[(gdf_geom['Year']==year)&(gdf_geom['Crashes_cat']!='0.0')&(gdf_geom['functional_classification']=='Minor Collector')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='orange')
    gdf_geom[(gdf_geom['Year']==year)&(gdf_geom['Crashes_cat']!='0.0')&(gdf_geom['functional_classification']=='Major Collector')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='orange')
    gdf_geom[(gdf_geom['Year']==year)&(gdf_geom['Crashes_cat']!='0.0')&(gdf_geom['functional_classification']=='Minor Arterial')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='red')
    gdf_geom[(gdf_geom['Year']==year)&(gdf_geom['Crashes_cat']!='0.0')&(gdf_geom['functional_classification']=='Major Arterial')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='red')
    gdf_geom[(gdf_geom['Year']==year)&(gdf_geom['Crashes_cat']!='0.0')&(gdf_geom['functional_classification']=='Interstate')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='purple')
    axplace.title.set_text(str(year))

# 5.3. 6) Mapping -- # Trainig set modeled crashes mapping
def prediction_maps (axplace,geo_SegmentID,df_geom, prediction, year, segment_intersection, model_geography, model_name, map_name):
    prediction_geom = df_geom.merge(pd.Series(prediction,name='prediction').to_frame(),  left_index=True, right_index=True, how='right')
    prediction_geom  = gpd.GeoDataFrame(prediction_geom , geometry=prediction_geom['geometry'])
    warnings.simplefilter(action='ignore', category=UserWarning)
    geo_SegmentID.plot(ax=axplace, color='grey')
    prediction_geom[
        (prediction_geom['Year']==year)&
        (prediction_geom['prediction']>0)&
        (prediction_geom['prediction']<=1)].plot(column='Crashes_cat',ax=axplace, linewidth=1,legend=True,color='yellow')
    prediction_geom[
        (prediction_geom['Year']==year)&
        (prediction_geom['prediction']>1)&
        (prediction_geom['prediction']<=2)].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='orange')
    prediction_geom[
        (prediction_geom['Year']==year)&
        (prediction_geom['prediction']>2)&
        (prediction_geom['prediction']<=3)].plot(column='Crashes_cat',ax=axplace, linewidth=3,legend=True,color='red')
    prediction_geom[
        (prediction_geom['Year']==year)&
        (prediction_geom['prediction']>=4)].plot(column='Crashes_cat',ax=axplace, linewidth=4,legend=True,color='brown')
    axplace.title.set_text(str(year))
    # os.makedirs('../reports/figures/model_results/'+segment_intersection+'/'+model_geography+'/'+model_name+'/', exist_ok=True)
    # fig.savefig('../reports/figures/model_results/'+segment_intersection+'/'+model_geography+'/'+model_name+'/'+str(year)+'_'+map_name+'.png')

# 5.3. 6) Mapping -- # Trainig set modeled crashes (binary) mapping
def prediction_binary_maps (axplace,geo_SegmentID,df_geom, prediction, year):
    prediction_geom = df_geom.copy(deep=True)
    prediction_geom['prediction'] = prediction
    prediction_geom  = gpd.GeoDataFrame(prediction_geom , geometry=prediction_geom['geometry'])
    warnings.simplefilter(action='ignore', category=UserWarning)
    geo_SegmentID.plot(ax=axplace, linewidth=0.5, color='grey')
    prediction_geom[
        (prediction_geom['Year']==year)&
        (prediction_geom['prediction']>0)].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='red')
    axplace.title.set_text(str(year))
    # os.makedirs('../reports/figures/model_results/'+segment_intersection+'/'+model_geography+'/'+model_name+'/', exist_ok=True)
    # fig.savefig('../reports/figures/model_results/'+segment_intersection+'/'+model_geography+'/'+model_name+'/'+str(year)+'_'+map_name+'.png')
def prediction_binary_maps_byrdclass (axplace,geo_SegmentID,df_geom, prediction, year):
    prediction_geom = df_geom.copy(deep=True)
    prediction_geom['prediction'] = prediction
    prediction_geom  = gpd.GeoDataFrame(prediction_geom , geometry=prediction_geom['geometry'])
    warnings.simplefilter(action='ignore', category=UserWarning)
    geo_SegmentID.plot(ax=axplace, linewidth=0.5, color='grey')
    prediction_geom[ (prediction_geom['Year']==year)& (prediction_geom['prediction']>0)&(prediction_geom['functional_classification']=='Local Road or Street')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='yellow')
    prediction_geom[ (prediction_geom['Year']==year)& (prediction_geom['prediction']>0)&(prediction_geom['functional_classification']=='Minor Collector')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='orange')
    prediction_geom[ (prediction_geom['Year']==year)& (prediction_geom['prediction']>0)&(prediction_geom['functional_classification']=='Major Collector')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='orange')
    prediction_geom[ (prediction_geom['Year']==year)& (prediction_geom['prediction']>0)&(prediction_geom['functional_classification']=='Minor Arterial')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='red')
    prediction_geom[ (prediction_geom['Year']==year)& (prediction_geom['prediction']>0)&(prediction_geom['functional_classification']=='Major Arterial')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='red')
    prediction_geom[ (prediction_geom['Year']==year)& (prediction_geom['prediction']>0)&(prediction_geom['functional_classification']=='Interstate')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='purple')
    axplace.title.set_text(str(year))

def prediction_binary_diff_maps (axplace,df_geom, y_true, prediction_result, year, segment_intersection, model_name, map_name):
    # Plot TP,FN,TN,FP by colour
    # TP_id,FN_id,FP_id,TN_id = [],[],[],[]
    Judg = []

    prediction_geom = df_geom.copy(deep=True)
    prediction_geom['prediction'] = prediction_result
    prediction_geom  = gpd.GeoDataFrame(prediction_geom , geometry=prediction_geom['geometry'])
    warnings.simplefilter(action='ignore', category=UserWarning)

    for i in range(len(y_true.values)):
        if y_true.values[i]==1:
            if prediction_result[i]==1:
                Judg.append('TP')
            else:
                Judg.append('FN')
        else:
            if prediction_result[i]==1:
                Judg.append('FP')
            else:
                Judg.append('TN')
    prediction_geom['Judg'] = Judg

    prediction_geom[(prediction_geom['Year']==year)&
                    (prediction_geom['Judg']=='TP')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='red',label='TP')
    prediction_geom[(prediction_geom['Year']==year)&
                    (prediction_geom['Judg']=='FN')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='blue',label='FN')
    prediction_geom[(prediction_geom['Year']==year)&
                    (prediction_geom['Judg']=='TN')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='grey',label='TN')
    prediction_geom[(prediction_geom['Year']==year)&
                    (prediction_geom['Judg']=='FP')].plot(column='Crashes_cat',ax=axplace, linewidth=2,legend=True,color='orange',label='FP')
    axplace.title.set_text(str(year))
    axplace.legend()
    plt.suptitle(map_name, fontsize=16)

# 5.3. 6) Mapping --  Test set modeled crashes mapping
def test_prediction_maps (df_geom, prediction, segment_intersection, model_geography, model_name, map_name, start_index=979036):
    df_prediction=pd.Series(prediction,name='prediction').to_frame()
    df_prediction.index+= start_index
    prediction_geom = df_geom.merge(df_prediction, left_index=True, right_index=True, how='right')
    prediction_geom  = gpd.GeoDataFrame(prediction_geom , geometry=prediction_geom['geometry'])
    warnings.simplefilter(action='ignore', category=UserWarning)
    fig, ax = plt.subplots(1, 1, figsize=(15,15))
    prediction_geom[prediction_geom['prediction']<=0].plot(column='prediction',ax=ax, linewidth=0.5,legend=True,color='grey')
    prediction_geom[
        (prediction_geom['prediction']>0)&
        (prediction_geom['prediction']<=1)].plot(column='Crashes_cat',ax=ax, linewidth=1,legend=True,color='orange')
    prediction_geom[
        (prediction_geom['prediction']>1)&
        (prediction_geom['prediction']<=2)].plot(column='Crashes_cat',ax=ax, linewidth=2,legend=True,color='red')
    prediction_geom[
        (prediction_geom['prediction']>2)&
        (prediction_geom['prediction']<=3)].plot(column='Crashes_cat',ax=ax, linewidth=3,legend=True,color='brown')
    prediction_geom[prediction_geom['prediction']>=4].plot(column='Crashes_cat',ax=ax, linewidth=4,legend=True,color='black')
    # fig.savefig('../reports/figures/model_results/'+segment_intersection+'/'+model_geography+'/'+model_name+'/'+'2018_'+map_name+'.png')

# 5.3. 6) Mapping --  Test set modeled crashes (binary) mapping
#def test_prediction_binary_maps (df_geom, prediction, segment_intersection, model_geography, model_name, map_name, start_index=979036):

# 5.3.7) Summary report
def Bi_PR_AUC(X_train_transformed, y_train, X_test_transformed, y_test, model):
    # no skill model, stratified random class predictions
    model_naive = DummyClassifier(strategy='stratified')
    model_naive.fit(X_train_transformed, y_train)
    yhat_naive = model_naive.predict_proba(X_test_transformed)
    probs_naive = yhat_naive[:, 1]
    # calculate the precision-recall auc
    precision_naive, recall_naive, _ = precision_recall_curve(y_test, probs_naive)
    auc_score_naive = auc(recall_naive, precision_naive)
    print('No Skill Model PR AUC: %.3f' % auc_score_naive)

    # fit a model
    y_train_pred=model.predict_proba(X_train_transformed)[:, 1]
    precision, recall, thresholds = precision_recall_curve(y_train, y_train_pred)
    beta = 2
    f2score = ((1+beta**2) * precision * recall) / (beta**2*precision + recall)
    # locate the index of the largest f score
    # ix = argmax(f2score)
    ix = np.where(f2score==np.nanmax(f2score))
    maxf2 = f2score[ix]
    best_thres = thresholds[ix]
    auc_score_model = auc(recall, precision)
    print('Model PR AUC: %.3f' % auc_score_model)
    print('From Training data:')
    auc_fig=plot_pr_curve(y_train,y_train_pred)

    # show performance on test model
    print('Results of testing data:')
    test_pred_probs=model.predict_proba(X_test_transformed)[:, 1]
    test_pred_adj = [1 if y>=best_thres else 0 for y in test_pred_probs]
    testing_true_negative,testing_false_positive,testing_false_negative,testing_true_positive = cfm(y_test, test_pred_adj)
    precision = testing_true_positive/(testing_true_positive+testing_false_positive)
    fpr = testing_false_positive/(testing_false_positive+testing_true_negative)
    recall = testing_true_positive/(testing_true_positive+testing_false_negative)
    f1score = ((1+1**2) * precision * recall) / (1**2*precision + recall)
    f2score = ((1+2**2) * precision * recall) / (2**2*precision + recall)

    return auc_score_naive,auc_score_model,best_thres,f2score

def Bi_brier(X_transformed, y, model):
    ref_prob= len( y[y==1])/len(y)
    ref_probs = [ref_prob for _ in range(len(y))]
    bss_ref = brier_score_loss(y, ref_probs)
    # no skill prediction 0
    probabilities_0 = [0.0 for _ in range(len(y))]
    bss_noskill_0 = brier_skill_score(y, probabilities_0)
    print('No skill P(class1=0): BSS=%.4f' % (bss_noskill_0))
    # no skill prediction 1
    probabilities_1 = [1.0 for _ in range(len(y))]
    bss_noskill_1 = brier_skill_score(y, probabilities_1)
    print('No skill P(class1=1): BSS=%.4f' % (bss_noskill_1))
    # model probabilities
    probabilities = model.predict_proba(X_transformed)[:, 1]
    precision, recall, thresholds = precision_recall_curve(y, probabilities)
    # convert to f2 score
    beta = 2
    f2score = ((1+beta**2) * precision * recall) / (beta**2*precision + recall)
    # locate the index of the largest f score
    ix = argmax(f2score)
    best_thres = thresholds[ix]
    prob_adj = [1 if ypred>=best_thres else 0 for ypred in probabilities]
    bss = brier_skill_score(y, prob_adj)
    print('Model BSS=%.4f (f2-score applied for searching the best threshold)' % (bss))
    return bss_noskill_0, bss_noskill_1, bss

def binary_summary_report (X_train_transformed, y_train, X_test_transformed, y_test, model):
    # precision-recall curve & best F2
    _,auc_score_model,best_thres,max_f2=Bi_PR_AUC(X_train_transformed, y_train, X_test_transformed, y_test, model)
    # brier skill score
    # _,_,brier=Bi_brier(X_test_transformed, y_test, model)
    return [auc_score_model, best_thres,max_f2]

def model_perf_overview_plot(model_fitted,X_test_transformed,y_test):

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2,figsize=(10, 8))

    pred_probs = model_fitted.predict_proba(X_test_transformed)
    pred_probs = pred_probs[:, 1] # keep probabilities for the positive outcome only
    pred_auc = roc_auc_score(y_test, pred_probs)
    fpr, tpr, thresholds = roc_curve(y_test, pred_probs)

    # Figure 1: Trained model VS no-skill model
    ns_probs = [0 for _ in range(len(y_test))]
    ns_fpr, ns_tpr, ns_thresholds = roc_curve(y_test, ns_probs) # no skill model
    # A no-skill classifier is one that cannot discriminate between the classes and would predict a random class or a constant class in all cases.
    ns_auc = roc_auc_score(y_test, ns_probs)

    # plot the roc curve for the model
    ax1.plot(ns_fpr, ns_tpr, linestyle='--', label='No Skill')
    ax1.plot(fpr, tpr, marker='.', label='Model')
    ax1.set_xlabel('False Positive Rate')
    ax1.set_ylabel('True Positive Rate')
    ax1.legend()
    print('No Skill: ROC AUC=%.3f' % (ns_auc)) # Recall = 0.5
    print('Model: ROC AUC=%.3f' % (pred_auc))

    # Figure 2: False positive rate and true positive rate under different thresholds
    ax2.plot(thresholds,fpr, linestyle='--', label='False positive rate')
    ax2.plot(thresholds,tpr, marker='.', label='True positive rate')
    ax2.set_xlabel('Threshold')
    ax2.legend()
    # By selecting the threshold, we want a high TPR and a low FPR => High precision
    # A small threshold would lead to a high TPR and also a high FPR, a large threshold would lead to a small TPR and also a small FPR
    # We need to find a balance

    # Figure 3: Precision,Recall under different thresholds
    # calculate the precision-recall auc
    precision, recall, thresholds = precision_recall_curve(y_test, pred_probs)
    ax3.plot(thresholds,precision[:-1],linestyle='--', label='Precision')
    ax3.plot(thresholds,recall[:-1], marker='.', label='Recall')
    ax3.set_xlabel('Threshold')
    ax3.set_ylabel('Score')
    ax3.legend()


    # Figure 4: f-score under different thresholds
    f1score = ((1+1**2) * precision * recall) / (1**2*precision + recall)
    plt.plot(thresholds, f1score[:-1],linestyle='--', label='F1')
    f2score = ((1+2**2) * precision * recall) / (2**2*precision + recall)
    plt.plot(thresholds, f2score[:-1], marker='.', label='F2')
    ax4.set_xlabel('Threshold')
    ax4.set_ylabel('F-Score')
    ax4.legend()

def model_log (file_name, dict_of_elem, field_names):
    with open (file_name, 'a+', newline='') as write_obj:
        dict_writer = DictWriter (write_obj, fieldnames=field_names)
        dict_writer.writerow (dict_of_elem)

def model_report(model,X_test_transformed,y_test):
    y_pred = model.predict_proba(X_test_transformed)[:,1]
    precision, recall, thresholds = precision_recall_curve(y_test, y_pred)
    beta = 2
    f2score = ((1+beta**2) * precision * recall) / (beta**2*precision + recall)
    # locate the index of the largest f score
    ix = np.where(f2score==np.nanmax(f2score))
    maxf2 = f2score[ix][0]
    best_thres = thresholds[ix][0]
    auc_score_model = auc(recall, precision)
    print('Model PR AUC: %.3f' % auc_score_model)
    auc_fig=plot_pr_curve(y_test,y_pred)

    return best_thres,maxf2

## Random Oversampling and Downsampling a pecific roadway functional classification

def dup_rdclass(dup_pos_ratio,X_train_Rdclass,X_train_OHtransformed_Rdclass,y_train_Rdclass):

    addpos_counts_Rdclass = int(np.shape(X_train_OHtransformed_Rdclass[y_train_Rdclass==1])[0]*dup_pos_ratio)
    index_to_dup_Rdclass = sorted(np.random.choice(X_train_Rdclass[y_train_Rdclass==1].index,
                                                size=addpos_counts_Rdclass, replace=False))
    id_to_dup_Rdclass = np.where(X_train_Rdclass.index.isin(index_to_dup_Rdclass))[0]
    X_train_Rdclass_Oversampled = pd.concat([X_train_Rdclass,X_train_Rdclass.loc[index_to_dup_Rdclass,:]])
    X_train_OHtransformed_Rdclass_Oversampled = np.concatenate((X_train_OHtransformed_Rdclass,X_train_OHtransformed_Rdclass[id_to_dup_Rdclass,:]),axis=0)
    y_train_Rdclass_Oversampled = pd.concat([y_train_Rdclass,y_train_Rdclass.loc[index_to_dup_Rdclass]],axis=0)
    return X_train_Rdclass_Oversampled,X_train_OHtransformed_Rdclass_Oversampled,y_train_Rdclass_Oversampled


def drop_rdclass(drop_neg_ratio,X_train_Rdclass,X_train_OHtransformed_Rdclass,y_train_Rdclass):

    rd_removesize = int(np.shape(X_train_OHtransformed_Rdclass[y_train_Rdclass==0])[0]*drop_neg_ratio)
    rd_index_to_remove = sorted(np.random.choice(X_train_Rdclass[y_train_Rdclass==0].index,
                                    size=rd_removesize, replace=False))
    rd_id_to_remove = np.where(X_train_Rdclass.index.isin(rd_index_to_remove))[0]
    X_train_Rd_drop = X_train_Rdclass.drop(rd_index_to_remove,axis=0)
    y_train_Rd_drop = y_train_Rdclass.drop(rd_index_to_remove,axis=0)
    X_train_OHtransformed_Rd_drop = np.delete(X_train_OHtransformed_Rdclass, rd_id_to_remove, axis=0)
    return X_train_Rd_drop,X_train_OHtransformed_Rd_drop,y_train_Rd_drop


############Regression Modeling###################
# Regression model pipeline
def func_regression_pipeline (model):
    return (Pipeline(
    steps=[
        ('variable_transformer', FunctionTransformer(func_transform_var, validate=False)),
        ('model', model)
    ], verbose=True))

# For regression models, merge all crash cases with less than 2 cases to one and sample the training data for quick test of models
def func_Shuffle_num (X, y, train_size):
    crash_count=y.value_counts()
    try:
        update_crashes=crash_count[crash_count == 2].index[0]
        y_upate=y.mask(y >= update_crashes, update_crashes-1)
    except:
        y_update=y
    split=StratifiedShuffleSplit(n_splits=1, train_size=train_size, random_state=42)
    for train_index, test_index in split.split(X,y_update):
        X_sample=X.iloc[train_index]
        y_sample=y_update.iloc[train_index].to_numpy()
    return X_sample, y_sample

# 5.3. 1) Overall Mean Squared Error
def func_rmse (label,prediction):
    mse = mean_squared_error(label, prediction)
    rmse = np.sqrt(mse)
    return rmse

# 5.3. 2) Cross Valuation Score
def func_cross_val (model, df, label, cv=2, scoring="neg_mean_squared_error"): # scoring can be changed, esp. for classification problem
    model_scoresx=cross_val_score(model, df, label, cv=cv, scoring=scoring)
    return model_scoresx #("%0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

# 5.3.3) Mean Squared Error by number of crashes
def func_rmse_crashes (label,prediction):
    prediction=pd.DataFrame(prediction)
    # rmse for 0 observed crashes
    label_0=label[label==0].reset_index(drop=True)
    label_0_index=label_0.index.tolist()
    prediction_0=prediction.iloc[label_0_index,0]
    mse_0 = mean_squared_error(label_0, prediction_0)
    rmse_0 = np.sqrt(mse_0)
    # rmse for 1 observed crashes
    label_1=label[label==1].reset_index(drop=True)
    label_1_index=label_1.index.tolist()
    prediction_1=prediction.iloc[label_1_index,0]
    mse_1 = mean_squared_error(label_1, prediction_1)
    rmse_1 = np.sqrt(mse_1)
    # rmse for 2 observed crashes
    label_2=label[label==2].reset_index(drop=True)
    label_2_index=label_2.index.tolist()
    prediction_2=prediction.iloc[label_2_index,0]
    mse_2 = mean_squared_error(label_2, prediction_2)
    rmse_2 = np.sqrt(mse_2)
    # rmse for 3 observed crashes
    label_3=label[label==3].reset_index(drop=True)
    label_3_index=label_3.index.tolist()
    prediction_3=prediction.iloc[label_3_index,0]
    mse_3 = mean_squared_error(label_3, prediction_3)
    rmse_3 = np.sqrt(mse_3)
    # rmse for more than 3 observed crashes
    label_4=label[label>=4].reset_index(drop=True)
    label_4_index=label_4.index.tolist()
    prediction_4=prediction.iloc[label_4_index,0]
    mse_4 = mean_squared_error(label_4, prediction_4)
    rmse_4 = np.sqrt(mse_4)
    return [rmse_0, rmse_1, rmse_2, rmse_3, rmse_4]

# 7.3. x) Error by functional class
def func_functional_compare(df_Func,label,prediction):
    # prepare data
    df_Func=pd.DataFrame.sparse.from_spmatrix(df_Func)
    df_Func.columns = ["Interstate", "Major Arterial", "Minor Arterial", "Major Collector", "Minor Collector","Local Road or Street"]
    df_Func['crashes']=label
    df_Func['modeled']=prediction
    df_Func=df_Func.fillna(0)
    # filter data by functioanl class
    Interstate=df_Func[df_Func['Interstate']==1]
    MajorArterial=df_Func[df_Func['Major Arterial']==1]
    MinorArterial=df_Func[df_Func['Minor Arterial']==1]
    MajorCollector=df_Func[df_Func['Major Collector']==1]
    MinorCollector=df_Func[df_Func['Minor Collector']==1]
    LocalRoad=df_Func[df_Func['Local Road or Street']==1]
    # get rmse by functional class
    rmse_interstate=np.sqrt(mean_squared_error(Interstate.crashes, Interstate.modeled))
    rmse_MajorArterial=np.sqrt(mean_squared_error(MajorArterial.crashes, MajorArterial.modeled))
    rmse_MinorArterial=np.sqrt(mean_squared_error(MinorArterial.crashes, MinorArterial.modeled))
    rmse_MajorCollector=np.sqrt(mean_squared_error(MajorCollector.crashes, MajorCollector.modeled))
    rmse_MinorCollector=np.sqrt(mean_squared_error(MinorCollector.crashes, MinorCollector.modeled))
    rmse_LocalRoad=np.sqrt(mean_squared_error(LocalRoad.crashes, LocalRoad.modeled))
    return [rmse_interstate, rmse_MajorArterial, rmse_MinorArterial, rmse_MajorCollector, rmse_MinorCollector, rmse_LocalRoad]

# 7.3. x) Histogram
def func_hist (label,prediction):
    histo_label=pd.DataFrame(label.value_counts()[0:3])
    histo_label.loc[4]=label.value_counts()[4:].sum()
    ranges = [-1,0,1,2,3,4,5,6,7,8,9,10]
    prediction_df=pd.DataFrame(prediction)
    prediction_df.columns=['modeled']
    modeled=prediction_df.modeled.groupby(pd.cut(prediction_df.modeled, ranges)).count()
    modeled=modeled.reset_index(drop=True)
    histo_modeled=pd.DataFrame(modeled[0:4])
    histo_modeled.loc[4]=modeled[4:].sum()
    histo_label['modeled']=histo_modeled.modeled
    histo_label['diff']=(histo_label['Crashes']-histo_label['modeled'])/histo_label['Crashes']
    return histo_label['diff']

# 5.3.7) Summary report
def summary_report (reg, label, prediction, df, n, scoring, fnames):
    # overall rmse
    overall_rmse=func_rmse(label,prediction)
    # cross valuation score
    cross_val_score=func_cross_val(reg, df, label, n, scoring)
    # rmse by number of crashes
    crashes_rmse=func_rmse_crashes(label,prediction)
    # error by functional class
    keep_list=['functional_classification_Interstate',
           'functional_classification_Major Arterial',
           'functional_classification_Minor Arterial',
           'functional_classification_Major Collector',
           'functional_classification_Minor Collector',
           'functional_classification_Local Road or Street',]
    df_Func=func_keep_var(df,keep_list,fnames)
    func_error=func_functional_compare(df_Func,label,prediction)
    # histogram
    histo=func_hist(label,prediction)
    return [overall_rmse, cross_val_score, crashes_rmse, func_error, histo]

def cfm(y, y_pred_adj):
    all_confusionmatrix = pd.DataFrame(confusion_matrix(y, y_pred_adj),
                           columns=['pred_neg', 'pred_pos'],
                           index=['true_neg', 'true_pos'])
    true_negative = all_confusionmatrix.iloc[0,0]
    false_positive = all_confusionmatrix.iloc[0,1]
    false_negative = all_confusionmatrix.iloc[1,0]
    true_positive = all_confusionmatrix.iloc[1,1]
    print(all_confusionmatrix)

    return true_negative,false_positive,false_negative,true_positive

def cfm_fc(X, y, y_pred_adj):

    analysis_geom = X.copy(deep=True)
    analysis_geom['true'] = y
    analysis_geom['prediction'] = y_pred_adj

    # confusion matrix for each functional_class
    fc_name = []
    fc_tn = []
    fc_fp = []
    fc_fn = []
    fc_tp = []
    cfm_fc_df = pd.DataFrame(columns=['f2score','precision','recall','fpr'])
    for fc in np.unique(analysis_geom['functional_classification']):
        fc_name.append(fc)
        fc_result = analysis_geom[analysis_geom['functional_classification']==fc]
        print(fc)
        fc_confusionmatrix = pd.DataFrame(confusion_matrix(fc_result['true'],fc_result['prediction'] ),
                               columns=['pred_neg', 'pred_pos'],
                               index=['true_neg', 'true_pos'])
        tn = fc_confusionmatrix.iloc[0,0]
        fp = fc_confusionmatrix.iloc[0,1]
        fn = fc_confusionmatrix.iloc[1,0]
        tp = fc_confusionmatrix.iloc[1,1]

        fc_pct_confusionmatrix = fc_confusionmatrix/sum(sum(fc_confusionmatrix.values))
        fc_tn.append(fc_pct_confusionmatrix.iloc[0,0])
        fc_fp.append(fc_pct_confusionmatrix.iloc[0,1])
        fc_fn.append(fc_pct_confusionmatrix.iloc[1,0])
        fc_tp.append(fc_pct_confusionmatrix.iloc[1,1])
        print(fc_confusionmatrix)

        precision = tp/(tp+fp)
        recall = tp/(tp+fn)
        fpr = fp/(fp+tn)
        f1score = ((1+1**2) * precision * recall) / (1**2*precision + recall)
        f2score = ((1+2**2) * precision * recall) / (2**2*precision + recall)
        # print("F2score: {} Precision: {} Recall: {} Fpr: {}".format(round(f2score,2), round(precision,2),round(recall,2),round(fpr,2)))

        cfm_fc_df.loc[fc,:] = [round(f2score,2), round(precision,2),round(recall,2),round(fpr,2)]

    print('Overall')
    testing_true_negative,testing_false_positive,testing_false_negative,testing_true_positive = cfm(y, y_pred_adj)
    precision = testing_true_positive/(testing_true_positive+testing_false_positive)
    fpr = testing_false_positive/(testing_false_positive+testing_true_negative)
    recall = testing_true_positive/(testing_true_positive+testing_false_negative)
    f1score = ((1+1**2) * precision * recall) / (1**2*precision + recall)
    f2score = ((1+2**2) * precision * recall) / (2**2*precision + recall)
    print("F2score: {} Precision: {} Recall: {} Fpr: {}".format(round(f2score,2), round(precision,2),round(recall,2),round(fpr,2)))

    cfm_fc_df.loc['Overall',:] =  [round(f2score,2), round(precision,2),round(recall,2),round(fpr,2)]
    fig,ax = plt.subplots()
    for col in cfm_fc_df.T.columns:
        if col!= 'Overall':
            plt.scatter(x=['f2score','precision','recall','fpr'],y=cfm_fc_df.T[col],label=col)
            plt.plot(cfm_fc_df.T[col],linestyle = 'dotted')
    plt.scatter(x=['f2score','precision','recall','fpr'],y=cfm_fc_df.T['Overall'],label=col,marker='^',c='black')
    plt.plot(cfm_fc_df.T['Overall'],c='black')
    plt.legend(bbox_to_anchor=(1, 1))

    #return fc_name,fc_tn,fc_fp,fc_fn,fc_tp
    return cfm_fc_df

def plot_dup_rdclass_perform(pltdf):
    fig,ax = plt.subplots(dpi=60)
    OverEvalCols = ['f2score','precision','recall','fpr']
    for cols in OverEvalCols:
        plt.scatter(x=pltdf['Dupratio'],y=pltdf[cols],label=cols, marker="x")
        plt.plot(pltdf['Dupratio'],pltdf[cols])
    plt.xticks(pltdf['Dupratio'],rotation=90)
    plt.title('Overall Performance')
    plt.legend(bbox_to_anchor=(1, 1))

    fig,ax = plt.subplots(3,2,dpi=60,figsize=(12,10))

    IntEvalCols = ['Int_f2score','Int_precision','Int_recall','Int_fpr']
    for cols in IntEvalCols:
        ax[0,0].scatter(x=pltdf['Dupratio'],y=pltdf[cols],label=cols, marker="x")
        ax[0,0].plot(pltdf['Dupratio'],pltdf[cols])
        ax[0,0].title.set_text('Interstate')
        ax[0,0].set_xticks(pltdf['Dupratio'])
        ax[0,0].set_xticklabels(ax[0,0].get_xticks(), rotation = 90,fontsize=8)
        ax[0,0].set_ylim([0, 1])

    LocRdEvalCols = ['LocRd_f2score','LocRd_precision','LocRd_recall','LocRd_fpr']
    for cols in LocRdEvalCols:
        ax[0,1].scatter(x=pltdf['Dupratio'],y=pltdf[cols],label=cols, marker="x")
        ax[0,1].plot(pltdf['Dupratio'],pltdf[cols])
        ax[0,1].title.set_text('Local Road or Street')
        ax[0,1].set_xticks(pltdf['Dupratio'])
        ax[0,1].set_xticklabels(ax[0,1].get_xticks(), rotation = 90,fontsize=8)
        ax[0,1].set_ylim([0, 1])

    MajArtEvalCols = ['MajArt_f2score','MajArt_precision','MajArt_recall','MajArt_fpr']
    for cols in MajArtEvalCols:
        ax[1,0].scatter(x=pltdf['Dupratio'],y=pltdf[cols],label=cols, marker="x")
        ax[1,0].plot(pltdf['Dupratio'],pltdf[cols])
        ax[1,0].title.set_text('Major Arterial')
        ax[1,0].set_xticks(pltdf['Dupratio'])
        ax[1,0].set_xticklabels(ax[1,0].get_xticks(), rotation = 90,fontsize=8)
        ax[1,0].set_ylim([0, 1])

    MajColEvalCols = ['MajCol_f2score','MajCol_precision','MajCol_recall','MajCol_fpr']
    for cols in MajColEvalCols:
        ax[1,1].scatter(x=pltdf['Dupratio'],y=pltdf[cols],label=cols, marker="x")
        ax[1,1].plot(pltdf['Dupratio'],pltdf[cols])
        ax[1,1].title.set_text('Major Collector')
        ax[1,1].set_xticks(pltdf['Dupratio'])
        ax[1,1].set_xticklabels(ax[1,1].get_xticks(), rotation = 90,fontsize=8)
        ax[1,1].set_ylim([0, 1])

    MinArtEvalCols = ['MinArt_f2score','MinArt_precision','MinArt_recall','MinArt_fpr']
    for cols in MinArtEvalCols:
        ax[2,0].scatter(x=pltdf['Dupratio'],y=pltdf[cols],label=cols, marker="x")
        ax[2,0].plot(pltdf['Dupratio'],pltdf[cols])
        ax[2,0].title.set_text('Minor Arterial')
        ax[2,0].set_xticks(pltdf['Dupratio'])
        ax[2,0].set_xticklabels(ax[2,0].get_xticks(), rotation = 90,fontsize=8)
        ax[2,0].set_ylim([0, 1])

    MinColCols = ['MinCol_f2score','MinCol_precision','MinCol_recall','MinCol_fpr']
    for cols in MinColCols:
        ax[2,1].scatter(x=pltdf['Dupratio'],y=pltdf[cols],label=cols, marker="x")
        ax[2,1].plot(pltdf['Dupratio'],pltdf[cols])
        ax[2,1].title.set_text('Minor Collector')
        ax[2,1].set_xticks(pltdf['Dupratio'])
        ax[2,1].set_xticklabels(ax[2,1].get_xticks(), rotation = 90,fontsize=8)
        ax[2,1].set_ylim([0, 1])

############Feature Examination###################
def plt_Pop_Year(year,df,geo_SegmentID):
    # plot Pop in year
    # df = df_urban_train OR df_urban_test
    col = 'Pop_den'
    df_forplot = gpd.GeoDataFrame(
            df,
            geometry=df.geometry,
            crs = {'init': 'epsg:3435'})

    df_forplot_yr = df_forplot[df_forplot['Year']==year]

    lev=[250,500,1000,2000]
    fig, ax = plt.subplots(figsize=(10, 20))
    ax.set_aspect('equal')
    geo_SegmentID.plot(ax=ax, color='grey',zorder=1)
    df_forplot_yr[df_forplot_yr[col]<=lev[0]].plot(ax=ax,column=col,zorder=2,color="#90BE6D")
    df_forplot_yr[(df_forplot_yr[col]>lev[0])&(df_forplot_yr[col]<=lev[1])].plot(ax=ax,column=col,zorder=2,color="#F9C74F")
    df_forplot_yr[(df_forplot_yr[col]>lev[1])&(df_forplot_yr[col]<=lev[2])].plot(ax=ax,column=col,zorder=2,color="#F8961E")
    df_forplot_yr[(df_forplot_yr[col]>lev[2])&(df_forplot_yr[col]<=lev[3])].plot(ax=ax,column=col,zorder=2,color="#F3722C")
    df_forplot_yr[df_forplot_yr[col]>lev[3]].plot(ax=ax,column=col,zorder=2,color="#F94144")
    ax.legend(['base',col+'<='+str(lev[0]),col+'<='+str(lev[1]),col+'<='+str(lev[2]),col+'<='+str(lev[3]),col+'>'+str(lev[3])])
    plt.title(col+str(year))
    plt.show()

def plt_Pop_Year_diff(year1,df_plot1,year2,df_plot2,geo_SegmentID):
    # plot the diff of AADT in year1 and year2
    # df_plot1 and df_plot2 can be df_urban_train OR df_urban_test
    df_plot1_seg = list(df_plot1.segment_id.values)
    df_plot2_seg = list(df_plot2.segment_id.values)
    drop_seg=list(set(df_plot1_seg) - set(df_plot2_seg))
    df_plot1 = df_plot1[~df_plot1.segment_id.isin(drop_seg)]

    col = 'Pop_den'
    df1_forplot = gpd.GeoDataFrame(
            df_plot1,
            geometry=df_plot1.geometry,
            crs = {'init': 'epsg:3435'})
    df1_forplot_yr = df1_forplot[df1_forplot['Year']==year1]

    df2_forplot = gpd.GeoDataFrame(
            df_plot2,
            geometry=df_plot2.geometry,
            crs = {'init': 'epsg:3435'})
    df2_forplot_yr = df2_forplot[df2_forplot['Year']==year2]

    df2_forplot_yr['diff'] = df2_forplot_yr[col].values-df1_forplot_yr[col].values

    lev=[-200,0,200]
    fig, ax = plt.subplots(figsize=(10, 20))
    ax.set_aspect('equal')
    geo_SegmentID.plot(ax=ax, color='grey',zorder=1)
    df2_forplot_yr[df2_forplot_yr['diff']<=lev[0]].plot(ax=ax,column='diff',zorder=2,color='blue')
    df2_forplot_yr[(df2_forplot_yr['diff']<=lev[1])&(df2_forplot_yr['diff']>lev[0])].plot(ax=ax,column='diff',zorder=2,color='green')
    df2_forplot_yr[(df2_forplot_yr['diff']>lev[1])&(df2_forplot_yr['diff']<=lev[2])].plot(ax=ax,column='diff',zorder=2,color='orange')
    df2_forplot_yr[df2_forplot_yr['diff']>lev[2]].plot(ax=ax,column='diff',zorder=2,color='red')
    ax.legend(['base','Pop_den decreases more than 200','Pop_den decreases less than 200','Pop_den increases less than 200','Pop_den increases more than 200'])
    plt.title(str(year2)+'_Pop_den - '+str(year1)+'_Pop_den')
    plt.show()

def plt_AADT_Year(year,df,geo_SegmentID):
    # plot AADT in year
    # df = df_urban_train OR df_urban_test
    # e.g. plt_AADT_Year(2017,df_urban_train,geo_SegmentID)
    col = 'AADT'
    df_forplot = gpd.GeoDataFrame(
            df,
            geometry=df.geometry,
            crs = {'init': 'epsg:3435'})
    df_forplot_yr = df_forplot[df_forplot['Year']==year]

    lev=[2500,5000,10000,20000]
    fig, ax = plt.subplots(figsize=(10, 20))
    ax.set_aspect('equal')
    geo_SegmentID.plot(ax=ax, color='grey',zorder=1)
    df_forplot_yr[df_forplot_yr[col]<=lev[0]].plot(ax=ax,column=col,zorder=2,color="#90BE6D")
    df_forplot_yr[(df_forplot_yr[col]>lev[0])&(df_forplot_yr[col]<=lev[1])].plot(ax=ax,column=col,zorder=2,color="#F9C74F")
    df_forplot_yr[(df_forplot_yr[col]>lev[1])&(df_forplot_yr[col]<=lev[2])].plot(ax=ax,column=col,zorder=2,color="#F8961E")
    df_forplot_yr[(df_forplot_yr[col]>lev[2])&(df_forplot_yr[col]<=lev[3])].plot(ax=ax,column=col,zorder=2,color="#F3722C")
    df_forplot_yr[df_forplot_yr[col]>lev[3]].plot(ax=ax,column=col,zorder=2,color="#F94144")
    ax.legend(['base',col+'<='+str(lev[0]),col+'<='+str(lev[1]),col+'<='+str(lev[2]),col+'<='+str(lev[3]),col+'>'+str(lev[3])])
    plt.title(col+str(year))
    plt.show()

def plt_AADT_Year_diff(year1,df1,year2,df2,geo_SegmentID):
    # plot the diff of AADT in year1 and year2
    # df1 and df2 can be df_urban_train OR df_urban_test
    # e.g.plt_AADT_Year_diff(2014,df_urban_train,2018,df_urban_test,geo_SegmentID)
    df_plot1_seg = list(df1.segment_id.values)
    df_plot2_seg = list(df2.segment_id.values)
    drop_seg=list(set(df_plot1_seg) - set(df_plot2_seg))
    df1 = df1[~df1.segment_id.isin(drop_seg)]

    col = 'AADT'
    df1_forplot = gpd.GeoDataFrame(
            df1,
            geometry=df1.geometry,
            crs = {'init': 'epsg:3435'})
    df1_forplot_yr = df1_forplot[df1_forplot['Year']==year1]

    df2_forplot = gpd.GeoDataFrame(
            df2,
            geometry=df2.geometry,
            crs = {'init': 'epsg:3435'})
    df2_forplot_yr = df2_forplot[df2_forplot['Year']==year2]

    df2_forplot_yr['diff'] = df2_forplot_yr[col].values-df1_forplot_yr[col].values
    lev = [-1000,0,1000]
    fig, ax = plt.subplots(figsize=(10, 20))
    ax.set_aspect('equal')
    geo_SegmentID.plot(ax=ax, color='grey',zorder=1)
    df2_forplot_yr[df2_forplot_yr['diff']<=lev[0]].plot(ax=ax,column='diff',zorder=2,color='blue')
    df2_forplot_yr[(df2_forplot_yr['diff']<lev[1])&(df2_forplot_yr['diff']>-lev[2])].plot(ax=ax,column='diff',zorder=2,color='green')
    df2_forplot_yr[(df2_forplot_yr['diff']>lev[1])&(df2_forplot_yr['diff']<lev[2])].plot(ax=ax,column='diff',zorder=2,color='orange')
    df2_forplot_yr[df2_forplot_yr['diff']>=lev[2]].plot(ax=ax,column='diff',zorder=2,color='red')
    ax.legend(['base','AADT decreases more than 1000','AADT  decreases less than 1000','AADT increases less than 1000','AADT increases more than 1000'])
    plt.title(str(year2)+'_AADT - '+str(year1)+'_AADT')
    plt.show()

def plt_crossing_aadt_Year_diff(year1,df1,year2,df2,geo_SegmentID):
    # plot the diff of crossing_aadt in year1 and year2
    # df1 and df2 can be df_urban_train OR df_urban_test
    # e.g.plt_AADT_Year_diff(2014,df_urban_train,2018,df_urban_test,geo_SegmentID)

    df_plot1_seg = list(df1.segment_id.values)
    df_plot2_seg = list(df2.segment_id.values)
    drop_seg=list(set(df_plot1_seg) - set(df_plot2_seg))
    df1 = df1[~df1.segment_id.isin(drop_seg)]

    col = 'crossing_aadt'
    df1_forplot = gpd.GeoDataFrame(
            df1,
            geometry=df1.geometry,
            crs = {'init': 'epsg:3435'})
    df1_forplot_yr = df1_forplot[df1_forplot['Year']==year1]

    df2_forplot = gpd.GeoDataFrame(
            df2,
            geometry=df2.geometry,
            crs = {'init': 'epsg:3435'})
    df2_forplot_yr = df2_forplot[df2_forplot['Year']==year2]

    df2_forplot_yr['diff'] = df2_forplot_yr[col].values-df1_forplot_yr[col].values

    lev = [-1000,0,1000]
    fig, ax = plt.subplots(figsize=(10, 20))
    ax.set_aspect('equal')
    geo_SegmentID.plot(ax=ax, color='grey',zorder=1)
    df2_forplot_yr[df2_forplot_yr['diff']<=lev[0]].plot(ax=ax,column='diff',zorder=2,color='blue')
    df2_forplot_yr[(df2_forplot_yr['diff']<lev[1])&(df2_forplot_yr['diff']>lev[0])].plot(ax=ax,column='diff',zorder=2,color='green')
    df2_forplot_yr[(df2_forplot_yr['diff']>lev[1])&(df2_forplot_yr['diff']<lev[2])].plot(ax=ax,column='diff',zorder=2,color='orange')
    df2_forplot_yr[df2_forplot_yr['diff']>=lev[2]].plot(ax=ax,column='diff',zorder=2,color='red')
    ax.legend(['base','crossing_aadt decreases more than 1000','crossing_aadt  decreases less than 1000','crossing_aadt increases less than 1000','crossing_aadt increases more than 1000'])
    plt.title(str(year2)+'_crossing_aadt - '+str(year1)+'_crossing_aadt')
    plt.show()

def plt_bcywidth_Year_diff(year1,df1,year2,df2,geo_SegmentID):
    # plot the diff of bicycle_facility_width in year1 and year2
    # df1 and df2 can be df_urban_train OR df_urban_test

    df_plot1_seg = list(df1.segment_id.values)
    df_plot2_seg = list(df2.segment_id.values)
    drop_seg=list(set(df_plot1_seg) - set(df_plot2_seg))
    df1 = df1[~df1.segment_id.isin(drop_seg)]

    col = 'bicycle_facility_width'
    df1_forplot = gpd.GeoDataFrame(
            df1,
            geometry=df1.geometry,
            crs = {'init': 'epsg:3435'})
    df1_forplot_yr = df1_forplot[df1_forplot['Year']==year1]

    df2_forplot = gpd.GeoDataFrame(
            df2,
            geometry=df2.geometry,
            crs = {'init': 'epsg:3435'})
    df2_forplot_yr = df2_forplot[df2_forplot['Year']==year2]

    df2_forplot_yr['diff'] = df2_forplot_yr[col].values-df1_forplot_yr[col].values

    fig, ax = plt.subplots(figsize=(10, 20))
    ax.set_aspect('equal')
    geo_SegmentID.plot(ax=ax, color='grey',zorder=1)
    df2_forplot_yr[df2_forplot_yr['diff']<=-50].plot(ax=ax,column='diff',zorder=2,color='blue')
    df2_forplot_yr[(df2_forplot_yr['diff']<=0)&(df2_forplot_yr['diff']>-50)].plot(ax=ax,column='diff',zorder=2,color='green')
    df2_forplot_yr[(df2_forplot_yr['diff']>0)&(df2_forplot_yr['diff']<=50)].plot(ax=ax,column='diff',zorder=2,color='orange')
    df2_forplot_yr[df2_forplot_yr['diff']>50].plot(ax=ax,column='diff',zorder=2,color='red')
    ax.legend(['base','bicycle_facility_width decreases more than 50','bicycle_facility_width decreases less than 50', 'bicycle_facility_width increases less than 50','bicycle_facility_width increases more than 50'])
    plt.title(str(year2)+'_bicycle_facility_width - '+str(year1)+'_bicycle_facility_width')
    plt.show()

def plt_sidewalk_buffer_width_Year_diff(year1,df1,year2,df2,geo_SegmentID):
    # plot the diff of sidewalk_buffer_width in year1 and year2
    # df1 and df2 can be df_urban_train OR df_urban_test
    df_plot1_seg = list(df1.segment_id.values)
    df_plot2_seg = list(df2.segment_id.values)
    drop_seg=list(set(df_plot1_seg) - set(df_plot2_seg))
    df1 = df1[~df1.segment_id.isin(drop_seg)]

    col = 'sidewalk_buffer_width'
    df1_forplot = gpd.GeoDataFrame(
            df1,
            geometry=df1.geometry,
            crs = {'init': 'epsg:3435'})
    df1_forplot_yr = df1_forplot[df1_forplot['Year']==year1]

    df2_forplot = gpd.GeoDataFrame(
            df2,
            geometry=df2.geometry,
            crs = {'init': 'epsg:3435'})
    df2_forplot_yr = df2_forplot[df2_forplot['Year']==year2]

    df2_forplot_yr['diff'] = df2_forplot_yr[col].values-df1_forplot_yr[col].values

    fig, ax = plt.subplots(figsize=(10, 20))
    ax.set_aspect('equal')
    geo_SegmentID.plot(ax=ax, color='grey',zorder=1)
    df2_forplot_yr[df2_forplot_yr['diff']<=-100].plot(ax=ax,column='diff',zorder=2,color='blue')
    df2_forplot_yr[(df2_forplot_yr['diff']<=0)&(df2_forplot_yr['diff']>-100)].plot(ax=ax,column='diff',zorder=2,color='green')
    df2_forplot_yr[(df2_forplot_yr['diff']>0)&(df2_forplot_yr['diff']<=100)].plot(ax=ax,column='diff',zorder=2,color='orange')
    df2_forplot_yr[df2_forplot_yr['diff']>100].plot(ax=ax,column='diff',zorder=2,color='red')
    ax.legend(['base','sidewalk_buffer_width decreases more than 100','sidewalk_buffer_width decreases less than 100', 'sidewalk_buffer_width increases less than 100','sidewalk_buffer_width increases more than 100'])
    plt.title(str(year2)+'_sidewalk_buffer_width - '+str(year1)+'_sidewalk_buffer_width')
    plt.show()

def plt_sidewalk_width_Year_diff(year1,df1,year2,df2,geo_SegmentID):
    # plot the diff of sidewalk_width in year1 and year2
    # df1 and df2 can be df_urban_train OR df_urban_test
    df_plot1_seg = list(df1.segment_id.values)
    df_plot2_seg = list(df2.segment_id.values)
    drop_seg=list(set(df_plot1_seg) - set(df_plot2_seg))
    df1 = df1[~df1.segment_id.isin(drop_seg)]

    col = 'sidewalk_width'
    df1_forplot = gpd.GeoDataFrame(
            df_plot1,
            geometry=df_plot1.geometry,
            crs = {'init': 'epsg:3435'})
    df1_forplot_yr = df1_forplot[df1_forplot['Year']==year1]

    df2_forplot = gpd.GeoDataFrame(
            df_plot2,
            geometry=df_plot2.geometry,
            crs = {'init': 'epsg:3435'})
    df2_forplot_yr = df2_forplot[df2_forplot['Year']==year2]

    df2_forplot_yr['diff'] = df2_forplot_yr[col].values-df1_forplot_yr[col].values

    fig, ax = plt.subplots(figsize=(10, 20))
    ax.set_aspect('equal')

    geo_SegmentID.plot(ax=ax, color='grey',zorder=1)
    df2_forplot_yr[df2_forplot_yr['diff']<=-100].plot(ax=ax,column='diff',zorder=2,color='blue')
    df2_forplot_yr[(df2_forplot_yr['diff']<=0)&(df2_forplot_yr['diff']>-100)].plot(ax=ax,column='diff',zorder=2,color='green')
    df2_forplot_yr[(df2_forplot_yr['diff']>0)&(df2_forplot_yr['diff']<=100)].plot(ax=ax,column='diff',zorder=2,color='orange')
    df2_forplot_yr[df2_forplot_yr['diff']>100].plot(ax=ax,column='diff',zorder=2,color='red')
    ax.legend(['base','sidewalk_width decreases more than 100','sidewalk_width decreases less than 100', 'sidewalk_width increases less than 100','sidewalk_width increases more than 100'])
    plt.title(str(year2)+'_sidewalk_width - '+str(year1)+'_sidewalk_width')
    plt.show()

def plt_bicycle_path_category_Year(year,df,geo_SegmentID):
    # plot bicycle_path_category in year
    # df = df_urban_train OR df_urban_test
    df_plot1_seg = list(df1.segment_id.values)
    df_plot2_seg = list(df2.segment_id.values)
    drop_seg=list(set(df_plot1_seg) - set(df_plot2_seg))
    df1 = df1[~df1.segment_id.isin(drop_seg)]

    col = 'bicycle_path_category'
    df_forplot = gpd.GeoDataFrame(
            df,
            geometry=df.geometry,
            crs = {'init': 'epsg:3435'})

    df_forplot_yr = df_forplot[df_forplot['Year']==year]

    fig, ax = plt.subplots(figsize=(10, 20))
    ax.set_aspect('equal')
    geo_SegmentID.plot(ax=ax, color='grey',zorder=1)
    df_forplot_yr[df_forplot_yr[col]=='None'].plot(ax=ax,column=col,zorder=2,color='Blue')
    df_forplot_yr[df_forplot_yr[col]=='Off-Street Trail'].plot(ax=ax,column=col,zorder=2,color='Red')
    df_forplot_yr[df_forplot_yr[col]=='On-Street Bikeway'].plot(ax=ax,column=col,zorder=2,color='Green')
    plt.legend(['base','None','Off-Street Trail','On-Street Bikeway'])
    plt.title(col+str(year))
    plt.show()

def plt_binary_risky_road_diff(df_year1,y_pred1,Year1,df_year2,y_pred2,Year2,geo_SegmentID):
    "This function compares the results in Year1 and Year2"
    "and plots (i)roads that are not risky anymore in Year2"
    "(ii)roads that are identified as new risky roads in Year2"
    "(iii)roads that are identified as risky roads in both years"
    "Note: Road that have geometry changes are identified as different roads"

    df_1 = df_year1.copy()
    df_1['df1_predicted'] = y_pred1
    df_1 = gpd.GeoDataFrame(df_1 , geometry=df_1['geometry'])
    df_1 = df_1[df_1['Year']==Year1]
    df_1.index = df_1.segment_id

    df_2 = df_year2.copy()
    df_2['df2_predicted'] = y_pred2
    df_2 = gpd.GeoDataFrame(df_2 , geometry=df_2['geometry'])
    df_2 = df_2[df_2['Year']==Year2]
    df_2.index = df_2.segment_id

    fig, ax = plt.subplots(figsize=(10, 20))
    ax.set_aspect('equal')
    geo_SegmentID.plot(ax=ax, color='#D3D4D7',alpha=0.5,zorder=1)

    df1_pos_idx = df_1[df_1['df1_predicted']==1].index
    df1_neg_idx = df_1[df_1['df1_predicted']==0].index
    df2_pos_idx = df_2[df_2['df2_predicted']==1].index
    df2_neg_idx = df_2[df_2['df2_predicted']==0].index

    def common_elements(list1, list2):
        return list(set(list1) & set(list2))

    df_2.loc[common_elements(df1_pos_idx, df2_neg_idx),:].plot(ax=ax,column='df2_predicted', linewidth=2,legend=True,color='green',zorder=2)
    df_2.loc[common_elements(df1_neg_idx, df2_pos_idx),:].plot(ax=ax,column='df2_predicted', linewidth=2,legend=True,color='red',zorder=3)
    df_2.loc[common_elements(df1_pos_idx, df2_pos_idx),:].plot(ax=ax,column='df2_predicted', linewidth=2,legend=True,color='orange',zorder=4)

    plt.legend(['base','Risk-eliminated road','New risky road','Risky road in both years'])
    plt.title('Risky Road Prediction (difference)')
    plt.show()

def plt_low_medium_high_risk_roads(BinaryMulticlass_results,map_name):
    BinaryMulticlass_results = gpd.GeoDataFrame(BinaryMulticlass_results , geometry=BinaryMulticlass_results['geometry'])
    fig,ax = plt.subplots(figsize=(10, 20))
    BinaryMulticlass_results[(BinaryMulticlass_results['Binary']==0)&(BinaryMulticlass_results['MultiClass']==0.0)].plot(ax=ax,color='grey')
    BinaryMulticlass_results[(BinaryMulticlass_results['Binary']==0)&(BinaryMulticlass_results['MultiClass']>0.0) |
                             (BinaryMulticlass_results['Binary']==1)&(BinaryMulticlass_results['MultiClass']<2.0)].plot(ax=ax,color='orange')
    BinaryMulticlass_results[(BinaryMulticlass_results['Binary']==1)&(BinaryMulticlass_results['MultiClass']==2.0)].plot(ax=ax,color='red')
    plt.legend(['Low risk','Medium risk','High risk'])
    plt.title(map_name)

def table_risky_road_diff(df_year1,y_pred1,Year1,df_year2,y_pred2,Year2):
    "This function compares the results in Year1 and Year2"
    "and summarizes (i)roads that are not risky anymore in Year2"
    "(ii)roads that are identified as new risky roads in Year2"
    "(iii)roads that are identified as risky roads in both years"
    "Note: Road that have geometry changes are identified as different roads"

    df_1 = df_year1.copy()
    df_1['df1_predicted'] = y_pred1
    df_1 = df_1[df_1['Year']==Year1]
    df_1 = gpd.GeoDataFrame(df_1 , geometry=df_1['geometry'],crs = {'init': 'epsg:3435'})

    df_2 = df_year2.copy()
    df_2['df2_predicted'] = y_pred2
    df_2 = df_2[df_2['Year']==Year2]
    df_2 = gpd.GeoDataFrame(df_2 , geometry=df_2['geometry'],crs = {'init': 'epsg:3435'})

#     print(df_1.crs)

    if df_1.shape==df_2.shape:
        Elim_risky = df_1[(df_1['df1_predicted']==1).values&(df_2['df2_predicted']==0).values]
        New_risky = df_2[(df_2['df2_predicted']==1).values&(df_1['df1_predicted']==0).values]
        Both_risky = df_1[(df_1['df1_predicted']==1).values&(df_2['df2_predicted']==1).values]

        Scenario_1 = str(Year1)
        Scenario_2 = str(Year2)
        risk_sumry = pd.DataFrame(columns=['Counts of risky segments','Miles of risky segments'],index=[Scenario_1,Scenario_2,'Comparison','Common risky segments','New risky segments','Risk eliminated segments'])

        risk_sumry.loc[Scenario_1,'Counts of risky segments'] = df_1[df_1['df1_predicted']==1].shape[0]
        risk_sumry.loc[Scenario_2,'Counts of risky segments'] = df_2[df_2['df2_predicted']==1].shape[0]
        risk_sumry.loc['Common risky segments','Counts of risky segments'] = Both_risky.shape[0]
        risk_sumry.loc['New risky segments','Counts of risky segments'] = New_risky.shape[0]
        risk_sumry.loc['Risk eliminated segments','Counts of risky segments'] = Elim_risky.shape[0]
        risk_sumry.loc['Comparison','Counts of risky segments'] = ''

        risk_sumry.loc[Scenario_1,'Miles of risky segments'] = sum(df_1[df_1['df1_predicted']==1].geometry.length.values*0.000189394)
        risk_sumry.loc[Scenario_2,'Miles of risky segments'] = sum(df_2[df_2['df2_predicted']==1].geometry.length.values*0.000189394)
        risk_sumry.loc['Common risky segments','Miles of risky segments'] = sum(Both_risky['geometry'].length.values*0.000189394)
        risk_sumry.loc['New risky segments','Miles of risky segments'] = sum(New_risky['geometry'].length.values*0.000189394)
        risk_sumry.loc['Risk eliminated segments','Miles of risky segments'] = sum(Elim_risky['geometry'].length.values*0.000189394)
        risk_sumry.loc['Comparison','Miles of risky segments'] = ''

    else:
        print('Check geometry of the input road map, the two scenarios should have the same roadway segments')

    return risk_sumry

def table_risky_road_diff_counts_fc(df_year1,y_pred1,Year1,df_year2,y_pred2,Year2):
    "This function compares the results in Year1 and Year2"
    "and summarizes (i)roads that are not risky anymore in Year2"
    "(ii)roads that are identified as new risky roads in Year2"
    "(iii)roads that are identified as risky roads in both years"
    "Note: Road that have geometry changes are identified as different roads"

    df_1 = df_year1.copy()
    df_1['df1_predicted'] = y_pred1
    df_1 = df_1[df_1['Year']==Year1]
    df_1 = gpd.GeoDataFrame(df_1 , geometry=df_1['geometry'],crs = {'init': 'epsg:3435'})

    df_2 = df_year2.copy()
    df_2['df2_predicted'] = y_pred2
    df_2 = df_2[df_2['Year']==Year2]
    df_2 = gpd.GeoDataFrame(df_2 , geometry=df_2['geometry'],crs = {'init': 'epsg:3435'})

    if df_1.shape==df_2.shape:
        Elim_risky = df_1[(df_1['df1_predicted']==1).values&(df_2['df2_predicted']==0).values]
        New_risky = df_2[(df_2['df2_predicted']==1).values&(df_1['df1_predicted']==0).values]
        Both_risky = df_1[(df_1['df1_predicted']==1).values&(df_2['df2_predicted']==1).values]

        Scenario_1 = str(Year1)
        Scenario_2 = str(Year2)
        fc = pd.unique(df_year1.functional_classification)
        risk_sumry = pd.DataFrame(columns=['Counts of risky Interstate','Counts of risky Major Arterial','Counts of risky Minor Arterial',
                                           'Counts of risky Major Collector','Counts of risky Minor Collector', 'Counts of risky Local Road or Street','Sum'],
                          index=[Scenario_1,Scenario_2,'Comparison','Common risky segments','New risky segments','Risk eliminated segments'])

        for rdtype in fc:
            risk_sumry.loc[Scenario_1,'Counts of risky '+rdtype] = df_1[(df_1['df1_predicted']==1)&(df_1['functional_classification']==rdtype)].shape[0]
            risk_sumry.loc[Scenario_2,'Counts of risky '+rdtype] = df_2[(df_2['df2_predicted']==1)&(df_2['functional_classification']==rdtype)].shape[0]
            risk_sumry.loc['Common risky segments','Counts of risky '+rdtype] = Both_risky[Both_risky['functional_classification']==rdtype].shape[0]
            risk_sumry.loc['New risky segments','Counts of risky '+rdtype] = New_risky[New_risky['functional_classification']==rdtype].shape[0]
            risk_sumry.loc['Risk eliminated segments','Counts of risky '+rdtype] = Elim_risky[Elim_risky['functional_classification']==rdtype].shape[0]

            risk_sumry.loc[Scenario_1,'Sum'] = df_1[df_1['df1_predicted']==1].shape[0]
            risk_sumry.loc[Scenario_2,'Sum'] = df_2[df_2['df2_predicted']==1].shape[0]
            risk_sumry.loc['Common risky segments','Sum'] = Both_risky.shape[0]
            risk_sumry.loc['New risky segments','Sum'] = New_risky.shape[0]
            risk_sumry.loc['Risk eliminated segments','Sum'] = Elim_risky.shape[0]

            risk_sumry.loc['Comparison','Counts of risky '+rdtype] = ''
            risk_sumry.loc['Comparison','Sum'] = ''

    else:
        print('Check geometry of the input road map, the two scenarios should have the same roadway segments')

    return risk_sumry

def table_risky_road_diff_miles_fc(df_year1,y_pred1,Year1,df_year2,y_pred2,Year2):
    "This function compares the results in Year1 and Year2"
    "and summarizes (i)roads that are not risky anymore in Year2"
    "(ii)roads that are identified as new risky roads in Year2"
    "(iii)roads that are identified as risky roads in both years"
    "Note: Road that have geometry changes are identified as different roads"

    df_1 = df_year1.copy()
    df_1['df1_predicted'] = y_pred1
    df_1 = df_1[df_1['Year']==Year1]
    df_1 = gpd.GeoDataFrame(df_1 , geometry=df_1['geometry'],crs = {'init': 'epsg:3435'})

    df_2 = df_year2.copy()
    df_2['df2_predicted'] = y_pred2
    df_2 = df_2[df_2['Year']==Year2]
    df_2 = gpd.GeoDataFrame(df_2 , geometry=df_2['geometry'],crs = {'init': 'epsg:3435'})

    if df_1.shape==df_2.shape:
        Elim_risky = df_1[(df_1['df1_predicted']==1).values&(df_2['df2_predicted']==0).values]
        New_risky = df_2[(df_2['df2_predicted']==1).values&(df_1['df1_predicted']==0).values]
        Both_risky = df_1[(df_1['df1_predicted']==1).values&(df_2['df2_predicted']==1).values]

        Scenario_1 = str(Year1)
        Scenario_2 = str(Year2)
        fc = pd.unique(df_year1.functional_classification)
        risk_sumry = pd.DataFrame(columns=['Miles of risky Interstate','Miles of risky Major Arterial','Miles of risky Minor Arterial',
                                           'Miles of risky Major Collector','Miles of risky Minor Collector', 'Miles of risky Local Road or Street','Sum'],
                          index=[Scenario_1,Scenario_2,'Comparison','Common risky segments','New risky segments','Risk eliminated segments'])


        for rdtype in fc:
            risk_sumry.loc[Scenario_1,'Miles of risky '+rdtype] = round(sum(df_1[(df_1['df1_predicted']==1)&(df_1['functional_classification']==rdtype)].geometry.length.values*0.000189394),1)
            risk_sumry.loc[Scenario_2,'Miles of risky '+rdtype] = round(sum(df_2[(df_2['df2_predicted']==1)&(df_2['functional_classification']==rdtype)].geometry.length.values*0.000189394),1)
            risk_sumry.loc['Common risky segments','Miles of risky '+rdtype] = round(sum(Both_risky[Both_risky['functional_classification']==rdtype].geometry.length.values*0.000189394),1)
            risk_sumry.loc['New risky segments','Miles of risky '+rdtype] = round(sum(New_risky[New_risky['functional_classification']==rdtype].geometry.length.values*0.000189394),1)
            risk_sumry.loc['Risk eliminated segments','Miles of risky '+rdtype] = round(sum(Elim_risky[Elim_risky['functional_classification']==rdtype].geometry.length.values*0.000189394),1)

            risk_sumry.loc[Scenario_1,'Sum'] = round(sum(df_1[df_1['df1_predicted']==1].geometry.length.values*0.000189394),1)
            risk_sumry.loc[Scenario_2,'Sum'] = round(sum(df_2[df_2['df2_predicted']==1].geometry.length.values*0.000189394),1)
            risk_sumry.loc['Common risky segments','Sum'] = round(sum(Both_risky['geometry'].length.values*0.000189394),1)
            risk_sumry.loc['New risky segments','Sum'] = round(sum(New_risky['geometry'].length.values*0.000189394),1)
            risk_sumry.loc['Risk eliminated segments','Sum'] = round(sum(Elim_risky['geometry'].length.values*0.000189394),1)

            risk_sumry.loc['Comparison','Miles of risky '+rdtype] = ''
            risk_sumry.loc['Comparison','Sum'] = ''

    else:
        print('Check geometry of the input road map, the two scenarios should have the same roadway segments')

    return risk_sumry

AADT_change_thres = [-1000,-500,500,1000]
def AADT_change_score(aadt_change,AADT_change_thres):
    if aadt_change<=AADT_change_thres[0]:
        change_score = -2
    elif (aadt_change>AADT_change_thres[0])&(aadt_change<AADT_change_thres[1]):
        change_score = -1
    elif (aadt_change>=AADT_change_thres[1])&(aadt_change<=AADT_change_thres[2]):
        change_score = 0
    elif (aadt_change>AADT_change_thres[2])&(aadt_change<AADT_change_thres[3]):
        change_score = 1
    elif aadt_change>=AADT_change_thres[3]:
        change_score = 2
    return change_score

AADT_pct_change_thres = [-0.2,-0.1,0.1,0.2]
def AADT_pct_change_score(aadt_pct_change,AADT_pct_change_thres):
    if aadt_pct_change<=AADT_pct_change_thres[0]:
        change_score = -2
    elif (aadt_pct_change>AADT_pct_change_thres[0])&(aadt_pct_change<AADT_pct_change_thres[1]):
        change_score = -1
    elif (aadt_pct_change>=AADT_pct_change_thres[1])&(aadt_pct_change<=AADT_pct_change_thres[2]):
        change_score = 0
    elif (aadt_pct_change>AADT_pct_change_thres[2])&(aadt_pct_change<AADT_pct_change_thres[3]):
        change_score = 1
    elif aadt_pct_change>=AADT_pct_change_thres[3]:
        change_score = 2
    return change_score

Pop_change_thres = [-200,-50,50,200]
def Pop_change_score(pop_change,Pop_change_thres):
    if pop_change<=Pop_change_thres[0]:
        change_score = -2
    elif (pop_change>Pop_change_thres[0])&(pop_change<Pop_change_thres[1]):
        change_score = -1
    elif (pop_change>=Pop_change_thres[1])&(pop_change<=Pop_change_thres[2]):
        change_score = 0
    elif (pop_change>Pop_change_thres[2])&(pop_change<Pop_change_thres[3]):
        change_score = 1
    elif pop_change>=Pop_change_thres[3]:
        change_score = 2
    return change_score

Pop_pct_change_thres = [-0.2,-0.1,0.1,0.2]
def Pop_pct_change_score(pop_pct_change,Pop_pct_change_thres):
    if pop_pct_change<=Pop_pct_change_thres[0]:
        change_score = -2
    elif (pop_pct_change>Pop_pct_change_thres[0])&(pop_pct_change<Pop_pct_change_thres[1]):
        change_score = -1
    elif (pop_pct_change>=Pop_pct_change_thres[1])&(pop_pct_change<=Pop_pct_change_thres[2]):
        change_score = 0
    elif (pop_pct_change>Pop_pct_change_thres[2])&(pop_pct_change<Pop_pct_change_thres[3]):
        change_score = 1
    elif pop_pct_change>=Pop_pct_change_thres[3]:
        change_score = 2
    return change_score

def trend_check_table_fc(base_df,base_yr,y_train,y_base_pred,test_df,test_yr,y_test_pred,y_test):
    trend_check_table = pd.DataFrame()
    trend_check_table['Pred_base_yr'] = np.array(y_base_pred)[base_df['Year']==base_yr]
    trend_check_table['True_base_yr'] = np.array(y_train)[base_df['Year']==base_yr]
    trend_check_table['AADT_base_yr'] = base_df[base_df['Year']==base_yr].AADT.values
    trend_check_table['AADT_test_yr'] = test_df[test_df['Year']==test_yr].AADT.values
    trend_check_table['AADT_diff'] = base_df[base_df['Year']==base_yr].AADT.values - test_df[test_df['Year']==test_yr].AADT.values
    trend_check_table['AADT_%_diff'] = (base_df[base_df['Year']==base_yr].AADT.values - test_df[test_df['Year']==test_yr].AADT.values)/base_df[base_df['Year']==base_yr].AADT.values
    trend_check_table['Pop_den_base_yr'] = base_df[base_df['Year']==base_yr].Pop_den.values
    trend_check_table['Pop_den_test_yr'] = test_df[test_df['Year']==test_yr].Pop_den.values
    trend_check_table['Pop_den_diff'] = base_df[base_df['Year']==base_yr].Pop_den.values - test_df[test_df['Year']==test_yr].Pop_den.values
    trend_check_table['Pop_den_%_diff'] = (base_df[base_df['Year']==base_yr].Pop_den.values - test_df[test_df['Year']==test_yr].Pop_den.values)/base_df[base_df['Year']==base_yr].Pop_den.values
    trend_check_table['Pred_test_yr'] = np.array(y_test_pred)[test_df['Year']==test_yr]
    trend_check_table['True_test_yr'] = np.array(y_test)[test_df['Year']==test_yr]
    trend_check_table['Pred_diff'] = np.array(y_test_pred)[test_df['Year']==test_yr]-np.array(y_base_pred)[base_df['Year']==base_yr]

    return trend_check_table

def trend_check_score_table_fc(trend_check_table):
    trend_check_score_table = pd.DataFrame()
    trend_check_score_table['Pred_base_yr'] = trend_check_table['Pred_base_yr'].values
    trend_check_score_table['True_base_yr'] = trend_check_table['True_base_yr'].values
    trend_check_score_table['AADT_base_yr_score'] = 1*(trend_check_table['AADT_base_yr'].values>np.mean(trend_check_table['AADT_base_yr'].values))
    trend_check_score_table['AADT_test_yr_score'] = 1*(trend_check_table['AADT_test_yr'].values>np.mean(trend_check_table['AADT_test_yr'].values))
    trend_check_score_table['AADT_increase_level'] = [AADT_change_score(aadt_change,AADT_change_thres) for aadt_change in trend_check_table['AADT_diff'].values]
    trend_check_score_table['AADT_pct_increase_level'] = [AADT_pct_change_score(aadt_pct_change,AADT_pct_change_thres) for aadt_pct_change in trend_check_table['AADT_%_diff'].values]
    trend_check_score_table['Pop_den_base_yr_score'] = 1*(trend_check_table['Pop_den_base_yr'].values>np.mean(trend_check_table['Pop_den_base_yr'].values))
    trend_check_score_table['Pop_den_test_yr_score'] = 1*(trend_check_table['Pop_den_test_yr'].values>np.mean(trend_check_table['Pop_den_test_yr'].values))
    trend_check_score_table['Pop_den_increase_level'] = [Pop_change_score(pop_change,Pop_change_thres) for pop_change in trend_check_table['Pop_den_diff'].values]
    trend_check_score_table['Pop_den_pct_increase_level'] = [Pop_pct_change_score(pop_pct_change,Pop_pct_change_thres) for pop_pct_change in trend_check_table['Pop_den_%_diff'].values]
    trend_check_score_table['sum_score'] =  trend_check_score_table[[ 'AADT_base_yr_score','AADT_test_yr_score','AADT_increase_level','AADT_pct_increase_level','Pop_den_base_yr_score','Pop_den_test_yr_score','Pop_den_increase_level','Pop_den_pct_increase_level']].sum(axis=1)
    trend_check_score_table['Pred_test_yr'] = trend_check_table['Pred_test_yr'].values
    trend_check_score_table['True_test_yr'] = trend_check_table['True_test_yr'].values
    trend_check_score_table['Pred_diff'] = trend_check_table['Pred_diff'].values

    return trend_check_score_table
